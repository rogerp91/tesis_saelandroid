package info.udone.sael.notification;

import android.provider.BaseColumns;

import java.util.List;

import info.udone.sael.domain.entity.Notification;
import info.udone.sael.util.BasePresenter;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public interface NotificationContract {

    // TODO: View
    interface View {

        void updateView();// Progreso

        void showProgress(boolean active);// Progreso

        void setLoadingIndicator(boolean active);// Indicador

        void showModels(List<Notification> objects);// Listado de proyect

        void showNoModels(final boolean active);// No hay

        void showMessage(String message);

        boolean isActive();

    }

    // TODO: Presentador
    interface Presenter extends BasePresenter<View> {

    }

    abstract class NotificationEntry implements BaseColumns {
        public static final String TABLE_NAME = "Notification";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_FROM = "froms";
        public static final String COLUMN_NAME_BODY = "body";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_TIME = "time";
    }

}