package info.udone.sael.notification;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.util.Validations;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class CreateNotificationPresenter implements CreateNotificationContract.Presenter {

    private CreateNotificationContract.View view;

    private Context context;

    private CreateNotificationContract.SendNotificationInteractor interactor;

    @Inject
    public CreateNotificationPresenter(CreateNotificationContract.SendNotificationInteractor interactor) {
        this.interactor = interactor;
        context = Sael.getContext();
    }

    @Override
    public void setView(@NonNull CreateNotificationContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
        this.view.setTitle();
        this.view.setArrayPrioridad();
        this.view.setArrayCarrera();
    }

    @Override
    public void cleanEdit() {
        view.onCleanEdit();
    }


    @Override
    public void sendNotification(Notification notification) {
        //TODO: TITULO
        if (Validations.checkEmptry(notification.getTitle())) {
            if (!view.isActive()) {
                return;
            }
            view.showMessage("El titulo no puede ir vació");
            return;
        }
        if (Validations.checkLength(notification.getTitle().length(), 6)) {
            if (!view.isActive()) {
                return;
            }
            view.showMessage("El titulo debe de tener mínimo " + 6 + " caracteres");
            return;
        }

        //TODO: DE PARTE
        if (Validations.checkEmptry(notification.getFroms())) {
            if (!view.isActive()) {
                return;
            }
            view.showMessage("De no puede ir vació");
            return;
        }
        if (Validations.checkLength(notification.getFroms().length(), 5)) {
            if (!view.isActive()) {
                return;
            }
            view.showMessage("De debe de tener mínimo " + 6 + " caracteres");
            return;
        }

        //TODO: Mensaje
        if (Validations.checkEmptry(notification.getBody())) {
            if (!view.isActive()) {
                return;
            }
            view.showMessage("El mensaje no puede ir vació");
            return;
        }

        if (Validations.checkLength(notification.getBody().length(), 5)) {
            if (!view.isActive()) {
                return;
            }
            view.showMessage("El mensaje debe de tener mínimo " + 6 + " caracteres");
            return;
        }

        //TODO: Prioridad
        if (Validations.checkEmptry(notification.getBody())) {
            if (!view.isActive()) {
                return;
            }
            view.showMessage("El mensaje no puede ir vació");
            return;
        }

        //TODO: Carrera y cedula
        if (notification.getCarrera().equals("")) {
            notification.setCarrera("*");
        } else {
            if (notification.getCarrera().contains("All")) {
                notification.setCarrera("*");
            }
        }

        if (notification.getCedula().equals("")) {
            notification.setCedula("*");
        }

        view.showNotificationProgress(true);
        interactor.execute(notification, new CreateNotificationContract.SendNotificationInteractor.Callback() {
            @Override
            public void onSuccess() {
                if (!view.isActive()) return;
                view.onCleanEdit();
                view.showNotificationProgress(false);
                view.showMessageSuccess("Se envió la notificación exitosamente");
            }

            @Override
            public void onUnauthorized() {
                if (!view.isActive()) return;
                view.showNotificationProgress(false);
//                view.showMessage("Se envió la notificación exitosamente");
            }

            @Override
            public void onNotFound() {
                if (!view.isActive()) return;
                view.showNotificationProgress(false);
                view.showMessage("Ocurrió un error, verifique los números de cedula o los códigos de carrera");
            }

            @Override
            public void onUnprocessableEntity() {
                if (!view.isActive()) return;
                view.showNotificationProgress(false);
                view.showMessage("Ocurrió un error con los valores de las notificación son invadidos");
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                view.showNotificationProgress(false);
                view.showMessage("Ocurrió un error inesperado, intente de nuevo");
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                view.showNotificationProgress(false);
                view.showMessage("Ocurrió un error que usted no puede controlar, comuníquese con soporte urgentemente");
            }

            @Override
            public void onNetworkError() {
                if (!view.isActive()) return;
                view.showNotificationProgress(false);
                view.showMessage(context.getResources().getString(R.string.no_connection));
            }
        });

    }

}