package info.udone.sael.notification;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.domain.source.NotificationDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class NotificationPresenter implements NotificationContract.Presenter {

    private NotificationDataSource repository;
    private NotificationContract.View view;

    private Context context;

    @Inject
    NotificationPresenter(@Named("re_notification") NotificationDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }

    @Override
    public void setView(@NonNull NotificationContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
    }

    @Override
    public void start() {
        loadModels(true);
    }

    @Override
    public void loadModels(boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onProgress();
        } else {
            onIndicator();
        }
    }

    @Override
    public void deleteAllModels() {
        repository.deleteAllNotification();
    }

    private void onProgress() {
        view.showProgress(true);
        view.showNoModels(false);
        repository.getNotification(new NotificationDataSource.GetNotificationCallback() {
            @Override
            public void onLoaded(List<Notification> notifications) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (notifications.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(notifications);
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.updateView();
                view.showProgress(false);
                view.showNoModels(true);
            }
        });
    }

    private void onIndicator() {
        repository.getNotification(new NotificationDataSource.GetNotificationCallback() {
            @Override
            public void onLoaded(List<Notification> notifications) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (notifications.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(notifications);
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.no_data_available_appeals));
            }
        });
    }

    private void showMessage(String msg) {
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }

}