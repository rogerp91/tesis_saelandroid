package info.udone.sael.notification;

import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import info.udone.sael.domain.entity.Notification;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public interface CreateNotificationContract {

    // TODO: View
    interface View {

        void setTitle();

        void showMessage(String message);

        void showMessageSuccess(String message);

        boolean isActive();

        void setArrayCarrera();

        void setArrayPrioridad();

        void onCleanEdit();

        void onFinish();

        void showNotificationProgress(boolean active);

    }

    // TODO: Presentador
    interface Presenter {

        void setView(@NonNull CreateNotificationContract.View view);

        void cleanEdit();

        void sendNotification(Notification notification);

    }

    interface SendNotificationInteractor {

        interface Callback {

            void onSuccess();

            void onUnauthorized(); // 401

            void onNotFound(); // 403

            void onUnprocessableEntity(); // 422

            void onErrorOcurred(); // Error

            void onErrorNotSolve();// 500

            void onNetworkError();

        }

        void execute(@NonNull Notification notification, @NonNull SendNotificationInteractor.Callback callBackAnt);

    }

    abstract class NotificationCreateEntry implements BaseColumns {
        public static final String TABLE_NAME = "NotificationSend";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_FROM = "froms";
        public static final String COLUMN_NAME_BODY = "body";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_CARRERA = "carrera";
        public static final String COLUMN_NAME_CEDULA = "cedula";
    }

}