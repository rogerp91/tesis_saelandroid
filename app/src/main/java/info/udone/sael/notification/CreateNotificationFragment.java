package info.udone.sael.notification;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.ui.fragment.BaseFragment;


public class CreateNotificationFragment extends BaseFragment implements CreateNotificationContract.View {

    @BindView(R.id.titulo)
    EditText titulo;
    @BindView(R.id.de)
    EditText de;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.carrera)
    MultiAutoCompleteTextView carrera;
    @BindView(R.id.cedula)
    EditText cedula;

    @BindView(R.id.prioridad)
    SwitchCompat prioridad;

    @Inject
    CreateNotificationContract.Presenter presenter;

    InputMethodManager imm;

    private MaterialDialog.Builder builder;
    private MaterialDialog dialog;

    public static CreateNotificationFragment newInstance() {
        return new CreateNotificationFragment();
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_create_notification;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        presenter.setView(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        builder = new MaterialDialog.Builder(getActivity()).title("Espere por favor").content("Enviando notificación...").progress(true, 0).progressIndeterminateStyle(true);
        dialog = builder.build();
    }

    @Override
    public void setTitle() {
        ab.setTitle("Crear notificación");
    }

    @Override
    public void showMessage(String message) {
        shoeMessageError(message);
    }

    @Override
    public void showMessageSuccess(String message) {
        shoeMessage(message, R.color.success);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setArrayCarrera() {
        ArrayAdapter<String> topicName = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getActivity().getResources().getStringArray(R.array.array_specialtys));
        carrera.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        carrera.setThreshold(1);
        carrera.setAdapter(topicName);
    }

    @Override
    public void setArrayPrioridad() {
//        ArrayAdapter<String> topicName = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, getActivity().getResources().getStringArray(R.array.array_priority));
//        prioridad.setAdapter(topicName);
    }

    @Override
    public void onCleanEdit() {
        titulo.setText("");
        de.setText("");
        message.setText("");
        carrera.setText("");
        cedula.setText("");
        prioridad.setChecked(false);
    }

    @Override
    public void onFinish() {
        getActivity().finish();
    }

    @Override
    public void showNotificationProgress(boolean active) {
        if (active) {
            dialog.show();
        } else {
            dialog.dismiss();
        }
    }

    @OnClick({R.id.clean, R.id.send_button})
    void onClickEven(View view) {
        switch (view.getId()) {
            case R.id.clean:
                presenter.cleanEdit();
                break;
            case R.id.send_button:
                Notification notification = new Notification();
                notification.setTitle(titulo.getText().toString());
                notification.setBody(message.getText().toString());
                notification.setFroms(de.getText().toString());
                notification.setProridad(prioridad.isChecked() ? "high" : "normal");
                notification.setTime((new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())).format(new Date()));
                notification.setCedula(cedula.getText().toString());
                notification.setCarrera(carrera.getText().toString());
                presenter.sendNotification(notification);
                break;
        }
    }

}