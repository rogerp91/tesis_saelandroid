package info.udone.sael.elective;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Electives;
import info.udone.sael.domain.source.ElectiveDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 09/08/2016.
 */

public class ElectivePresenter implements ElectivesContract.Presenter {

    private ElectiveDataSource repository;
    private ElectivesContract.View view;

    private Context context;

    @Inject
    ElectivePresenter(@Named("re_elective") ElectiveDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }

    @Override
    public void setView(@NonNull ElectivesContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
    }

    @Override
    public void start(@NonNull String pensum, @NonNull String code) {
        loadModels(pensum, code, true);
    }

    @Override
    public void loadModels(@NonNull String pensum, @NonNull String code, boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onProgress(pensum, code);
        } else {
            onIndicator(pensum, code);
        }
    }

    private void onProgress(@NonNull String pensum, @NonNull String code) {
        view.showProgress(true);
        view.showErrorNotSolve(false);
        view.showNoModels(false);
        view.showErrorOcurred(false);
        repository.getElectives(pensum, code, new ElectiveDataSource.LoadElectiveCallback() {

            @Override
            public void onLoaded(List<Electives> electives) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (electives.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(electives);
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNoModels(true);
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorOcurred(true);
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorNotSolve(true);
            }

            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNetworkError(true);
            }
        });
    }

    private void onIndicator(@NonNull String pensum, @NonNull String code) {
        repository.refleshElectives(pensum, code, new ElectiveDataSource.LoadElectiveCallback() {

            @Override
            public void onLoaded(List<Electives> electives) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (electives.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(electives);
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.no_data_available_note));
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.error_occurred));
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.error_many));
            }

            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.no_connection));
            }
        });
    }

    private void showMessage(String msg) {
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }

}