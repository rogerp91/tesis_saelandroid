package info.udone.sael.elective;

import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import info.udone.sael.domain.entity.Electives;
import info.udone.sael.util.BaseView;

/**
 * Created by Roger Patiño on 09/08/2016.
 */

public interface ElectivesContract {

    // TODO: View
    interface View extends BaseView<Electives> {

    }

    // TODO: Presentador
    interface Presenter {

        void setView(@NonNull View view);

        void start(@NonNull String pensum, @NonNull String code);

        void loadModels(@NonNull String pensum, @NonNull String code, boolean showLoadingUI);

    }

    abstract class ElectiveEntry implements BaseColumns {
        public static final String TABLE_NAME = "Elective";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_CODIGO = "CODIGO";
        public static final String COLUMN_NAME_CARRERA = "CARRERA";
        public static final String COLUMN_NAME_PENSUM = "PENSUM";
        public static final String COLUMN_NAME_NOMBRE = "NOMBRE";
        public static final String COLUMN_NAME_TIPO_MATERIA = "TIPO_MATERIA";
        public static final String COLUMN_NAME_TIPO_MATERIA_NOMBRE = "TIPO_MATERIA_NOMBRE";
        public static final String COLUMN_NAME_SEMESTRE = "SEMESTRE";
        public static final String COLUMN_NAME_CREDITOS = "CREDITOS";
        public static final String COLUMN_NAME_ESPECIAL = "ESPECIAL";

    }

    abstract class RequisitoEntry implements BaseColumns {
        public static final String TABLE_NAME = "Requisito";
        public static final String COLUMN_NAME_MATERIA_PADRE = "MATERIA_PADRE";
        public static final String COLUMN_NAME_REQUISITO = "REQUISITO";
        public static final String COLUMN_NAME_CODIGO = "CODIGO";
        public static final String COLUMN_NAME_NOMBRE = "NOMBRE";

    }
}