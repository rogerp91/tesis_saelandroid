package info.udone.sael.interactor;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import javax.inject.Inject;

import info.udone.sael.domain.entity.Device;
import info.udone.sael.domain.entity.Host;
import info.udone.sael.domain.entity.Login;
import info.udone.sael.domain.entity.LoginWithProfile;
import info.udone.sael.domain.entity.ProvideClient;
import info.udone.sael.domain.source.remote.HttpRestClient;
import info.udone.sael.domain.source.remote.HttpRestClientFirebase;
import info.udone.sael.executor.Executor;
import info.udone.sael.executor.Interactor;
import info.udone.sael.executor.MainThread;
import info.udone.sael.login.LoginContract;
import info.udone.sael.util.ClientUtils;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Convertion;
import info.udone.sael.util.Devices;
import info.udone.sael.util.ReportLog;
import retrofit2.Call;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class LoginInteractor implements Interactor, LoginContract.LoginInteractor {

    private static String TAG = LoginInteractor.class.getSimpleName();

    private final MainThread mainThread;
    private final Executor executor;
    private String cedula;
    private String password;
    private Callback callback;

    @Inject
    HttpRestClient client;

//    @Inject
//    ProvideClient provideClient;
//
//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    LoginInteractor(MainThread mainThread, Executor executor) {
        this.mainThread = mainThread;
        this.executor = executor;
    }

    @Override
    public void execute(@NonNull String cedula, @NonNull String password, @NonNull Callback callBackAnt) {
        checkNotNull(cedula, "Cedula not null o empty!");
        checkNotNull(password, "Password not null o empty!");
        checkNotNull(callBackAnt, "Callback must not be null or response would not be able to be notified!");
        this.cedula = cedula;
        this.password = password;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(() -> signInUser(cedula, password));
//        mainThread.post(() -> getHost(cedula, password));
    }

//    private void getHost(String cedula, String password) {
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    response.body();
//                    client = ClientUtils.getRestClient(provideClient, response.body().getHost());
//                    signInUser(cedula, password);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }

    /**
     * Enviar la data al servidor
     *
     * @param cedula   Cedula
     * @param password Contraseña
     */
    private void signInUser(String cedula, String password) {
        Call<LoginWithProfile> call = client.postLogin2(HttpRestClient.USER_AGENT + Devices.getVersion(), new Login(cedula, Convertion.getBase64(password)));
        call.enqueue(new retrofit2.Callback<LoginWithProfile>() {
            @Override
            public void onResponse(Call<LoginWithProfile> call, Response<LoginWithProfile> response) {
                //Log.d(TAG, response.raw().toString());
                if (!response.isSuccessful()) {
                    if (response.code() == 401) {
                        mainThread.post(() -> callback.onUnauthorized());
                    } else {
                        if (response.code() == 403) {
                            ReportLog.send(TAG + ": Code " + response.code() + " in " + cedula);
                            mainThread.post(() -> callback.onForbidden());
                        } else {
                            if (response.code() == 400) {
                                ReportLog.send(TAG + ": Code " + response.code() + " in " + cedula);
                                mainThread.post(() -> callback.onErrorNotSolve());
                            } else {
                                ReportLog.send(TAG + ": Code " + response.code() + " in " + cedula);
                                mainThread.post(() -> callback.onErrorNotSolve());
                            }
                        }
                    }
                } else {
                    mainThread.post(() -> postDevices(response.body()));
                }
            }

            @Override
            public void onFailure(Call<LoginWithProfile> call, Throwable t) {
                mainThread.post(() -> callback.onErrorOcurred());
            }
        });
    }

    private void postDevices(LoginWithProfile loginWithProfile) {
        String fcm = FirebaseInstanceId.getInstance().getToken();
        try {
            assert fcm != null;
            if (fcm.isEmpty()) {
                fcm = FirebaseInstanceId.getInstance().getToken();
            }
        } catch (NullPointerException e) {
            ReportLog.send(TAG + " " + e.getMessage() + " " + loginWithProfile.getProfile().getCEDULA());
//            ReportError.send(e);
        }


        Device device = Devices.getDevices(fcm);
        System.out.println(new Gson().toJson(device));
        Call<Void> call = client.postDevice(HttpRestClient.PREFIX + loginWithProfile.getToken(), device);
        call.enqueue(new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                //Log.d(TAG, response.raw().toString());
                if (!response.isSuccessful()) {
                    if (response.code() == 401) {
                        mainThread.post(() -> callback.onUnauthorized());
                    } else {
                        if (response.code() == 403) {
                            ReportLog.send(TAG + ": Code " + response.code() + " in " + cedula);
                            mainThread.post(() -> callback.onForbidden());
                        } else {
                            if (response.code() == 409) {
                                ReportLog.send(TAG + ": Code " + response.code() + " in " + cedula);
                                mainThread.post(() -> callback.onErrorOcurred());
                            } else {
                                if (response.code() == 422) {
                                    ReportLog.send(TAG + ": Code " + response.code() + " in " + cedula);
                                    mainThread.post(() -> callback.onErrorOcurred());
                                } else {
                                    ReportLog.send(TAG + ": Code " + response.code() + " in " + cedula);
                                    mainThread.post(() -> callback.onErrorNotSolve());
                                }
                            }
                        }
                    }
                } else {
                    validateMe(loginWithProfile, device);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
//                FirebaseCrash.report(t);
                mainThread.post(() -> callback.onErrorOcurred());
            }
        });
    }


    private void validateMe(LoginWithProfile loginWithProfile, Device device) {
        try {
            if (loginWithProfile != null) {
                mainThread.post(() -> callback.onSuccess(loginWithProfile, device));
            } else {
                mainThread.post(() -> callback.onErrorOcurred());
            }
        } catch (NullPointerException e) {
//            ReportError.send(e);
            mainThread.post(() -> callback.onErrorOcurred());
        }
    }

}