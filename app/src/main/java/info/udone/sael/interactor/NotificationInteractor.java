package info.udone.sael.interactor;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;

import javax.inject.Inject;

import info.udone.sael.domain.entity.Notification;
import info.udone.sael.domain.source.remote.HttpRestClient;
import info.udone.sael.executor.Executor;
import info.udone.sael.executor.Interactor;
import info.udone.sael.executor.MainThread;
import info.udone.sael.notification.CreateNotificationContract;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Mapper;
import info.udone.sael.util.Networks;
import retrofit2.Call;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by AF on 04/01/2017.
 */

public class NotificationInteractor implements Interactor, CreateNotificationContract.SendNotificationInteractor {

    private static String TAG = NotificationInteractor.class.getSimpleName();

    private final MainThread mainThread;
    private final Executor executor;
    private CreateNotificationContract.SendNotificationInteractor.Callback callback;

    private Notification notification;

    @Inject
    HttpRestClient client;

    @Inject
    NotificationInteractor(MainThread mainThread, Executor executor) {
        this.mainThread = mainThread;
        this.executor = executor;
    }

    @Override
    public void execute(@NonNull Notification notification, @NonNull Callback callBackAnt) {
        checkNotNull(notification, "Notification not null o empty!");
        checkNotNull(callBackAnt, "Callback must not be null or response would not be able to be notified!");
        this.notification = notification;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(() -> {
            if (!Networks.isOnline(null)) {
                callback.onNetworkError();
                return;
            }

            sendNotification(notification);
        });
    }

//    private void getHostFirebase(Notification notification) {
//        ClientModule clientModule = new ClientModule();
//        Retrofit retrofit = clientModule.provideRetrofitFirebase(clientModule.provideGson(), clientModule.provideOkHttpClient(clientModule.provideInterceptor()));
//        HttpRestClientFirebase firebase = retrofit.create(HttpRestClientFirebase.class);
//        Call<Host> call = firebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    init(notification, response.body().getHost());
//                } else {
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                ReportLog.send(t.getMessage());
//                callback.onErrorOcurred();
//            }
//        });
//    }

    private void sendNotification(Notification notification) {
        Call<Void> call = client.pushNotification((HttpRestClient.PREFIX + SP.getString(Constants.TOKEN, "")), Mapper.getNotification(notification));
        call.enqueue(new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess();
                } else {
                    if (response.code() == 422) {
                        callback.onUnprocessableEntity();
                    } else {
                        if (response.code() == 404) {
                            callback.onNotFound();
                        } else {
                            callback.onErrorNotSolve();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onErrorOcurred();
            }
        });
    }
}