package info.udone.sael.interactor;

import android.Manifest;
import android.support.annotation.NonNull;

import java.io.File;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.executor.Executor;
import info.udone.sael.executor.Interactor;
import info.udone.sael.executor.MainThread;
import info.udone.sael.files.FileContract;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Devices;

import static info.udone.sael.Sael.getInstanceStorage;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class FilesInteractor implements Interactor, FileContract.FileInteractor {

    private static String TAG = FilesInteractor.class.getSimpleName();

    private final MainThread mainThread;
    private final Executor executor;
    private Callback callback;

    @Inject
    FilesInteractor(MainThread mainThread, Executor executor) {
        this.mainThread = mainThread;
        this.executor = executor;
    }

    @Override
    public void execute(@NonNull Callback callBackAnt) {
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(() -> {
            if (Devices.isMarshmallow()) {
                if (!Devices.hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    callback.onNotPermission();
                    return;
                }
            }
            boolean dirExists = getInstanceStorage().isDirectoryExists("Sael");
            if (!dirExists) {
                getInstanceStorage().createDirectory(Constants.PATH_BASE);
            }
            List<File> files = getInstanceStorage().getNestedFiles("Sael");
            if (files.isEmpty()) {
                callback.onDataNotAvailable();
            } else {
                Collections.sort(files, ((file, t1) -> t1.getName().compareTo(file.getName())));
                callback.onLoaded(files);
            }
        });
    }

}