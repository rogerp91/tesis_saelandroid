package info.udone.sael.interactor;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import info.udone.sael.document.DocumentContract;
import info.udone.sael.domain.entity.Doc;
import info.udone.sael.domain.entity.DocumentGenerate;
import info.udone.sael.domain.entity.FileDoc;
import info.udone.sael.domain.entity.Host;
import info.udone.sael.domain.entity.ProvideClient;
import info.udone.sael.domain.source.remote.HttpRestClientExternal;
import info.udone.sael.domain.source.remote.HttpRestClientFirebase;
import info.udone.sael.executor.Executor;
import info.udone.sael.executor.Interactor;
import info.udone.sael.executor.MainThread;
import info.udone.sael.util.ClientUtils;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;
import retrofit2.Call;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by AndrewX on 22/11/2016.
 */

public class DocumentInteractor implements Interactor, DocumentContract.CheckDocumentInteractor, DocumentContract.GenerateDocumentInteractor {

    private static String TAG = DocumentInteractor.class.getSimpleName();

    private final MainThread mainThread;
    private final Executor executor;
    private String ced;

//    @Inject
//    ProvideClient provideClient;
//
//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    HttpRestClientExternal clientExternal;


    private DocumentContract.CheckDocumentInteractor.Callback checkCallback;
    private DocumentContract.GenerateDocumentInteractor.Callback generateCallback;

    private DocumentGenerate docGen;

    private boolean check = false;
    private boolean generat = false;

    @Inject
    DocumentInteractor(MainThread mainThread, Executor executor) {
        this.mainThread = mainThread;
        this.executor = executor;
    }

    @Override
    public void execute(@NonNull String cedula, @NonNull DocumentContract.CheckDocumentInteractor.Callback callBackAnt) {
        checkNotNull(cedula, "Cedula not null o empty!");
        checkNotNull(callBackAnt, "Callback must not be null or response would not be able to be notified!");
        this.ced = cedula;
        this.checkCallback = callBackAnt;
        executor.run(this);
        check = true;
    }

    @Override
    public void execute(@NonNull DocumentGenerate generate, @NonNull DocumentContract.GenerateDocumentInteractor.Callback callBackAnt) {
        checkNotNull(generate, "DocumentGenerate not null o empty!");
        checkNotNull(callBackAnt, "Callback must not be null or response would not be able to be notified!");
        this.docGen = generate;
        this.generateCallback = callBackAnt;
        executor.run(this);
        generat = true;
    }

    @Override
    public void run() {
        mainThread.post(() -> {
            //Net
            if (!Networks.isOnline(null)) {
                if (check && checkCallback != null) {
                    checkCallback.onNetworkError();
                    return;
                } else {
                    if (generat && generateCallback != null) {
                        generateCallback.onNetworkError();
                        return;
                    }
                }
            }
            if (check && checkCallback != null) {
                getCheck();
            } else {
                if (generat && generateCallback != null) {
                    postGenerate();
                }
            }
        });
    }

    //============================================ Check =======================================
//    private void getHost() {
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    getCheck(ClientUtils.getRestClientExtenal(provideClient, response.body().getHost()));
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    mainThread.post(() -> checkCallback.onErrorNotSolve());
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                mainThread.post(() -> checkCallback.onErrorOcurred());
//            }
//        });
//    }

    private void getCheck() {
        Call<Doc> call = clientExternal.getDocCheck(ced);
        call.enqueue(new retrofit2.Callback<Doc>() {
            @Override
            public void onResponse(Call<Doc> call, Response<Doc> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 401) {
                        mainThread.post(() -> checkCallback.onUnauthorized());
                    } else {
                        if (response.code() == 403) {
                            ReportLog.send(TAG + ": Code " + response.code() + " in " + ced);
                            mainThread.post(() -> checkCallback.onForbidden());
                        } else {
                            if (response.code() == 404) {
                                ReportLog.send(TAG + ": Code " + response.code() + " in " + ced);
//                                mainThread.post(() -> checkCallback.onDataNotAvailable());
                            } else {
                                if (response.code() == 422) {
                                    ReportLog.send(TAG + ": Code " + response.code() + " in " + ced);
                                    mainThread.post(() -> checkCallback.onUnprocessableEntity());
                                } else {
                                    ReportLog.send(TAG + ": Code " + response.code() + " in " + ced);
                                    mainThread.post(() -> checkCallback.onErrorNotSolve());
                                }

                            }
                        }
                    }
                } else {
                    mainThread.post(() -> checkCallback.onSuccess(response.body()));
                }
            }

            @Override
            public void onFailure(Call<Doc> call, Throwable t) {
//                ReportError.send(t);
                checkCallback.onErrorOcurred();
            }
        });
    }

    //============================================ Check =======================================


    //============================================ Generate =======================================
//    private void getHost2() {
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    postGenerate(ClientUtils.getRestClientExtenal(provideClient, response.body().getHost()));
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    mainThread.post(() -> generateCallback.onErrorNotSolve());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                mainThread.post(() -> generateCallback.onErrorOcurred());
//            }
//        });
//    }

    private void postGenerate() {
        Call<FileDoc> call = clientExternal.getDoc(docGen.getCedula(), docGen.getDoc_modo(), docGen.getCi(), docGen.getCe(), docGen.getRe());
        call.enqueue(new retrofit2.Callback<FileDoc>() {
            @Override
            public void onResponse(Call<FileDoc> call, Response<FileDoc> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 401) {
                        mainThread.post(() -> generateCallback.onUnauthorized());
                    } else {
                        if (response.code() == 403) {
                            ReportLog.send(TAG + ": Code " + response.code() + " in " + ced);
                            mainThread.post(() -> generateCallback.onForbidden());
                        } else {
                            if (response.code() == 404) {
                                ReportLog.send(TAG + ": Code " + response.code() + " in " + ced);
//                                mainThread.post(() -> checkCallback.onDataNotAvailable());
                            } else {
                                if (response.code() == 422) {
                                    ReportLog.send(TAG + ": Code " + response.code() + " in " + ced);
                                    mainThread.post(() -> generateCallback.onUnprocessableEntity());
                                } else {
                                    ReportLog.send(TAG + ": Code " + response.code() + " in " + ced);
                                    mainThread.post(() -> generateCallback.onErrorNotSolve());
                                }

                            }
                        }
                    }
                } else {
                    mainThread.post(() -> generateCallback.onSuccess(response.body()));
                }
            }

            @Override
            public void onFailure(Call<FileDoc> call, Throwable t) {
//                ReportError.send(t);
                generateCallback.onErrorOcurred();
            }
        });
    }
    //============================================ Generate =======================================

}