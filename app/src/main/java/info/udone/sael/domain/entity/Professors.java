package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class Professors {

    private int id;
    @SerializedName("CEDULA")
    private String CEDULA;
    @SerializedName("TIT")
    private String TIT;
    @SerializedName("NOMBRES")
    private String NOMBRES;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCEDULA() {
        return CEDULA;
    }

    public void setCEDULA(String CEDULA) {
        this.CEDULA = CEDULA;
    }

    public String getTIT() {
        return TIT;
    }

    public void setTIT(String TIT) {
        this.TIT = TIT;
    }

    public String getNOMBRES() {
        return NOMBRES;
    }

    public void setNOMBRES(String NOMBRES) {
        this.NOMBRES = NOMBRES;
    }
}