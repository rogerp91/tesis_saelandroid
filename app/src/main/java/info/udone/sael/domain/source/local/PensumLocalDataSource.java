package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Materias;
import info.udone.sael.domain.entity.Requisito;
import info.udone.sael.domain.source.PensumDataSource;
import info.udone.sael.pensum.PensumContract.PensumEntry;
import info.udone.sael.pensum.PensumContract.RequisitoEntry;
import info.udone.sael.util.FormatDBString;

import static com.google.common.base.Preconditions.checkNotNull;
import static info.udone.sael.pensum.PensumContract.PensumEntry.COLUMN_NAME_CODIGO;

/**
 * Created by Roger Patiño on 03/08/2016.
 */
public class PensumLocalDataSource implements PensumDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    PensumLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void getPensums(String pensum, String code, @NonNull LoadPensumCallback callback) {
        open();
        List<Materias> materiases = new ArrayList<>();
        String query = "SELECT * FROM " + PensumEntry.TABLE_NAME + " WHERE CARRERA=" + FormatDBString.getFormatDBString(code) + " AND PENSUM=" + FormatDBString.getFormatDBString(pensum);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Materias materias = new Materias();
                materias.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_ID))));
                materias.setCARRERA(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_CARRERA)));
                materias.setPENSUM(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_PENSUM)));
                materias.setNOMBRE(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_NOMBRE)));
                materias.setTIPO_MATERIA(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_TIPO_MATERIA)));
                materias.setTIPO_MATERIA_NOMBRE(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_TIPO_MATERIA_NOMBRE)));
                materias.setSEMESTRE(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_SEMESTRE)));
                materias.setCREDITOS(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_CREDITOS)));
                materias.setESPECIAL(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_ESPECIAL)));
                String codigo = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_CODIGO));
                materias.setCODIGO(codigo);
                materias.setREQUISITOS(getRequisito(codigo));
                materiases.add(materias);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (materiases.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(materiases);
        }
    }

    @Override
    public void refleshPensum(String pensum, String code, @NonNull LoadPensumCallback callback) {
        getPensums(pensum, code, callback);
    }

    @Override
    public void getPensums(String pensum, String code, @NonNull GetPensumCallback callback) {
        open();
        List<Materias> materiases = new ArrayList<>();
        String query = "SELECT * FROM " + PensumEntry.TABLE_NAME + " WHERE CARRERA=" + FormatDBString.getFormatDBString(code) + " AND PENSUM=" + FormatDBString.getFormatDBString(pensum);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Materias materias = new Materias();
                materias.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_ID))));
                materias.setCARRERA(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_CARRERA)));
                materias.setPENSUM(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_PENSUM)));
                materias.setNOMBRE(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_NOMBRE)));
                materias.setTIPO_MATERIA(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_TIPO_MATERIA)));
                materias.setTIPO_MATERIA_NOMBRE(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_TIPO_MATERIA_NOMBRE)));
                materias.setSEMESTRE(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_SEMESTRE)));
                materias.setCREDITOS(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_CREDITOS)));
                materias.setESPECIAL(cursor.getString(cursor.getColumnIndex(PensumEntry.COLUMN_NAME_ESPECIAL)));
                String codigo = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_CODIGO));
                materias.setCODIGO(codigo);
                materias.setREQUISITOS(getRequisito(codigo));
                materiases.add(materias);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (materiases.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onPensumLoaded(materiases);
        }
    }

    @Override
    public void savePensum(@NonNull Materias materias) {
        checkNotNull(materias);
        open();
        ContentValues values = new ContentValues();
        //String replaced = materias.getCODIGO().replaceAll("x", "1");
        values.put(COLUMN_NAME_CODIGO, materias.getCODIGO());
        values.put(PensumEntry.COLUMN_NAME_CARRERA, materias.getCARRERA());
        values.put(PensumEntry.COLUMN_NAME_PENSUM, materias.getPENSUM());
        values.put(PensumEntry.COLUMN_NAME_NOMBRE, materias.getNOMBRE());
        values.put(PensumEntry.COLUMN_NAME_TIPO_MATERIA, materias.getTIPO_MATERIA());
        values.put(PensumEntry.COLUMN_NAME_TIPO_MATERIA_NOMBRE, materias.getTIPO_MATERIA_NOMBRE());
        values.put(PensumEntry.COLUMN_NAME_SEMESTRE, materias.getSEMESTRE());
        values.put(PensumEntry.COLUMN_NAME_CREDITOS, materias.getCREDITOS());
        values.put(PensumEntry.COLUMN_NAME_ESPECIAL, materias.getESPECIAL());
        db.insert(PensumEntry.TABLE_NAME, null, values);
        if (materias.getREQUISITOS() != null) {
            for (Requisito requisito : materias.getREQUISITOS())
                saveRequisito(materias.getCODIGO(), requisito);
        }
        close();
    }

    @Override
    public void deleteAllPensum() {
        open();
        db.delete(PensumEntry.TABLE_NAME, null, null);
        close();
    }

    private void saveRequisito(String father, Requisito requisito) {
        ContentValues values = new ContentValues();
        values.put(RequisitoEntry.COLUMN_NAME_MATERIA_PADRE, father);
        values.put(RequisitoEntry.COLUMN_NAME_CODIGO, requisito.getCODIGO());
        values.put(RequisitoEntry.COLUMN_NAME_NOMBRE, requisito.getNOMBRE());
        db.insert(RequisitoEntry.TABLE_NAME, null, values);
    }

    private List<Requisito> getRequisito(String codigo) {
        List<Requisito> requisitos = new ArrayList<>();
        String query = "SELECT * FROM " + RequisitoEntry.TABLE_NAME + " WHERE " + RequisitoEntry.COLUMN_NAME_MATERIA_PADRE + "=" + FormatDBString.getFormatDBString(codigo);
        try {
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    Requisito requisito = new Requisito();
                    requisito.setMATERIA_PADRE(cursor.getString(cursor.getColumnIndex(RequisitoEntry.COLUMN_NAME_MATERIA_PADRE)));
                    requisito.setCODIGO(cursor.getString(cursor.getColumnIndex(RequisitoEntry.COLUMN_NAME_CODIGO)));
                    requisito.setNOMBRE(cursor.getString(cursor.getColumnIndex(RequisitoEntry.COLUMN_NAME_NOMBRE)));
                    requisitos.add(requisito);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return requisitos;
    }

    private void open() {
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        db.close();
        sqliteDBHelper.close();
    }
}