package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Electives;
import info.udone.sael.domain.source.ElectiveDataSource;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class ElectiveRepository implements ElectiveDataSource {

    private ElectiveDataSource local;
    private ElectiveDataSource remote;

    @Inject
    ElectiveRepository(@Named("re_elective_l") ElectiveDataSource local, @Named("re_elective_r") ElectiveDataSource remote) {
        this.local = local;
        this.remote = remote;
    }

    @Override
    public void getElectives(String pensum, String code, @NonNull LoadElectiveCallback callback) {
        local.getElectives(pensum, code, new GetElectivesCallback() {
            @Override
            public void onElectivesLoaded(List<Electives> electives) {
                callback.onLoaded(electives);
            }

            @Override
            public void onDataNotAvailable() {
                remote.getElectives(pensum, code, new LoadElectiveCallback() {
                    @Override
                    public void onLoaded(List<Electives> electives) {
                        refreshLocalDataSource(electives);
                        callback.onLoaded(electives);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });
    }

    @Override
    public void refleshElectives(String pensum, String code, @NonNull LoadElectiveCallback callback) {
        remote.getElectives(pensum, code, new LoadElectiveCallback() {
            @Override
            public void onLoaded(List<Electives> electives) {
                refreshLocalDataSource(electives);
                callback.onLoaded(electives);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getElectives(String pensum, String code, @NonNull GetElectivesCallback callback) {
        local.getElectives(pensum, code, new GetElectivesCallback() {
            @Override
            public void onElectivesLoaded(List<Electives> electives) {
                callback.onElectivesLoaded(electives);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveElective(@NonNull Electives electives) {

    }

    @Override
    public void deleteAllElectives() {
        local.deleteAllElectives();
    }

    private void refreshLocalDataSource(List<Electives> electivesList) {
        local.deleteAllElectives();
        for (int i = 0; i < electivesList.size(); i++) {
            Electives electives = electivesList.get(i);
            electives.setId(i);
            local.saveElective(electives);
        }
    }
}