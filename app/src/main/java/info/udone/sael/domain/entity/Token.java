package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class Token {

    @SerializedName("token")
    private String token;
    @SerializedName("expires")
    private String expires;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }
}