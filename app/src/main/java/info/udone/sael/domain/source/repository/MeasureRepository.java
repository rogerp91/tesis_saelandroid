package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Measures;
import info.udone.sael.domain.source.MeasureDataSource;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class MeasureRepository implements MeasureDataSource {


    private MeasureDataSource remoteDataSource;
    private MeasureDataSource localDataSource;

    @Inject
    MeasureRepository(@Named("re_measures_r") MeasureDataSource remoteDataSource, @Named("re_measures_l") MeasureDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
    }

    @Override
    public void getMeasure(@NonNull LoadMeasureCallback callback) {
        localDataSource.getMeasure(new GetMeasureCallback() {
            @Override
            public void onLoaded(List<Measures> measures) {
                callback.onLoaded(measures);
            }

            @Override
            public void onDataNotAvailable() {
                remoteDataSource.getMeasure(new LoadMeasureCallback() {
                    @Override
                    public void onLoaded(List<Measures> measures) {
                        refreshLocalDataSource(measures);
                        callback.onLoaded(measures);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });

    }

    @Override
    public void refleshMeasure(@NonNull LoadMeasureCallback callback) {
        remoteDataSource.getMeasure(new LoadMeasureCallback() {
            @Override
            public void onLoaded(List<Measures> measures) {
                refreshLocalDataSource(measures);
                callback.onLoaded(measures);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getMeasure(@NonNull GetMeasureCallback callback) {
        localDataSource.getMeasure(new GetMeasureCallback() {
            @Override
            public void onLoaded(List<Measures> measures) {
                callback.onLoaded(measures);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveMeasure(@NonNull Measures measures) {

    }

    @Override
    public void deleteAllMeasure() {

    }

    private void refreshLocalDataSource(List<Measures> measures) {
        localDataSource.deleteAllMeasure();
        for (Measures measures1 : measures) {
            localDataSource.saveMeasure(measures1);
        }
    }

}