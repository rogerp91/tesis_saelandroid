package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Appeals;
import info.udone.sael.domain.source.AppealsDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class AppealsRepository implements AppealsDataSource {

    private AppealsDataSource mAppealsRemote;
    private AppealsDataSource mAppealsLocal;

    @Inject
    AppealsRepository(@Named("re_appeals_r") AppealsDataSource mAppealsRemoteDataSource, @Named("re_appeals_l") AppealsDataSource mAppealsLocalDataSource) {
        this.mAppealsRemote = mAppealsRemoteDataSource;
        this.mAppealsLocal = mAppealsLocalDataSource;
    }

    @Override
    public void getAppeals(@NonNull final LoadAppealCallback callback) {
        checkNotNull(callback);
        mAppealsLocal.getAppeals(new GetAppealCallback() {
            @Override
            public void onLoaded(List<Appeals> appealses) {
                callback.onLoaded(appealses);
            }

            @Override
            public void onDataNotAvailable() {
                mAppealsRemote.getAppeals(new LoadAppealCallback() {
                    @Override
                    public void onLoaded(List<Appeals> appealses) {
                        refreshLocalDataSource(appealses);
                        mAppealsLocal.getAppeals(new GetAppealCallback() {
                            @Override
                            public void onLoaded(List<Appeals> appealses) {
                                callback.onLoaded(appealses);
                            }

                            @Override
                            public void onDataNotAvailable() {

                            }
                        });
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });
    }

    @Override
    public void refleshAppeals(@NonNull LoadAppealCallback callback) {
        mAppealsRemote.refleshAppeals(new LoadAppealCallback() {
            @Override
            public void onLoaded(List<Appeals> appealses) {
                refreshLocalDataSource(appealses);
                mAppealsLocal.getAppeals(new GetAppealCallback() {
                    @Override
                    public void onLoaded(List<Appeals> appealses) {
                        callback.onLoaded(appealses);
                    }

                    @Override
                    public void onDataNotAvailable() {

                    }
                });
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getAppeals(@NonNull GetAppealCallback callback) {
        mAppealsLocal.getAppeals(new GetAppealCallback() {
            @Override
            public void onLoaded(List<Appeals> appealses) {
                callback.onLoaded(appealses);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveAppeal(@NonNull Appeals appeals) {

    }

    @Override
    public void deleteAllAppeals() {

    }

    private void refreshLocalDataSource(List<Appeals> appealses) {
        mAppealsLocal.deleteAllAppeals();
        for (Appeals appeals : appealses) {
            mAppealsLocal.saveAppeal(appeals);
        }
    }

}