package info.udone.sael.domain.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by AndrewX on 20/11/2016.
 */

public class ProfileItem {

    private int id;
    private String title;
    private String subname;
    private Drawable drawable;

    public ProfileItem(int id, String title, String subname, Drawable drawable) {
        this.id = id;
        this.title = title;
        this.subname = subname;
        this.drawable = drawable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}