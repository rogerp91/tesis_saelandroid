package info.udone.sael.domain.entity;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class Photo {

    private String status;
    private String link;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}