package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class Materias {

    @SerializedName("CODIGO")
    private String CODIGO;
    @SerializedName("CARRERA")
    private String CARRERA;
    @SerializedName("PENSUM")
    private String PENSUM;
    @SerializedName("NOMBRE")
    private String NOMBRE;
    @SerializedName("TIPO_MATERIA")
    private String TIPO_MATERIA;
    @SerializedName("TIPO_MATERIA_NOMBRE")
    private String TIPO_MATERIA_NOMBRE;
    @SerializedName("SEMESTRE")
    private String SEMESTRE;
    @SerializedName("CREDITOS")
    private String CREDITOS;
    @SerializedName("ESPECIAL")
    private String ESPECIAL;
    @SerializedName("REQUISITOS")
    private List<Requisito> REQUISITOS = null;

    private int id;

    public int getId() {
        return id;
    }

    public String getTIPO_MATERIA_NOMBRE() {
        return TIPO_MATERIA_NOMBRE;
    }

    public void setTIPO_MATERIA_NOMBRE(String TIPO_MATERIA_NOMBRE) {
        this.TIPO_MATERIA_NOMBRE = TIPO_MATERIA_NOMBRE;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCARRERA() {
        return CARRERA;
    }

    public void setCARRERA(String CARRERA) {
        this.CARRERA = CARRERA;
    }

    public String getPENSUM() {
        return PENSUM;
    }

    public void setPENSUM(String PENSUM) {
        this.PENSUM = PENSUM;
    }

    public String getCODIGO() {
        return CODIGO;
    }

    public void setCODIGO(String CODIGO) {
        this.CODIGO = CODIGO;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getTIPO_MATERIA() {
        return TIPO_MATERIA;
    }

    public void setTIPO_MATERIA(String TIPO_MATERIA) {
        this.TIPO_MATERIA = TIPO_MATERIA;
    }

    public String getSEMESTRE() {
        return SEMESTRE;
    }

    public void setSEMESTRE(String SEMESTRE) {
        this.SEMESTRE = SEMESTRE;
    }

    public String getCREDITOS() {
        return CREDITOS;
    }

    public void setCREDITOS(String CREDITOS) {
        this.CREDITOS = CREDITOS;
    }

    public String getESPECIAL() {
        return ESPECIAL;
    }

    public void setESPECIAL(String ESPECIAL) {
        this.ESPECIAL = ESPECIAL;
    }

    public List<Requisito> getREQUISITOS() {
        return REQUISITOS;
    }

    public void setREQUISITOS(List<Requisito> REQUISITOS) {
        this.REQUISITOS = REQUISITOS;
    }
}