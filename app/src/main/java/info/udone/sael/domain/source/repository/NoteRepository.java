package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Note;
import info.udone.sael.domain.source.NoteDataSource;


/**
 * Created by Roger Patiño on 03/08/2016.
 */
public class NoteRepository implements NoteDataSource {

    private String TAG = NoteRepository.class.getSimpleName();
    private NoteDataSource mNotesLocalDataSource;
    private NoteDataSource mNotesRemoteDataSource;

    @Inject
    NoteRepository(@Named("re_note_r") NoteDataSource mNotesRemoteDataSource, @Named("re_note_l") NoteDataSource mNotesLocalDataSource) {
        this.mNotesRemoteDataSource = mNotesRemoteDataSource;
        this.mNotesLocalDataSource = mNotesLocalDataSource;
    }

    @Override
    public void getNotes(@NonNull LoadNoteCallback callback) {
        mNotesLocalDataSource.getNotes(new GetNotesCallback() {
            @Override
            public void onNoteLoaded(List<Note> notes) {
                Log.d(TAG, "onNoteLoaded: 1");
                callback.onLoaded(notes);
            }

            @Override
            public void onDataNotAvailable() {
                mNotesRemoteDataSource.getNotes(new LoadNoteCallback() {
                    @Override
                    public void onLoaded(List<Note> notes) {
                        refreshLocalDataSource(notes);
                        Log.d(TAG, "onLoaded: 2");
                        mNotesLocalDataSource.getNotes(new GetNotesCallback() {
                            @Override
                            public void onNoteLoaded(List<Note> notess) {
                                Log.d(TAG, "onNoteLoaded: 3");
                                callback.onLoaded(notess);
                            }

                            @Override
                            public void onDataNotAvailable() {

                            }
                        });

                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });
    }

    @Override
    public void refleshNotes(@NonNull LoadNoteCallback callback) {
        mNotesRemoteDataSource.getNotes(new LoadNoteCallback() {
            @Override
            public void onLoaded(List<Note> notes) {
                refreshLocalDataSource(notes);
                mNotesLocalDataSource.getNotes(new GetNotesCallback() {
                    @Override
                    public void onNoteLoaded(List<Note> notes) {
                        callback.onLoaded(notes);
                    }

                    @Override
                    public void onDataNotAvailable() {

                    }
                });
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getNotes(@NonNull GetNotesCallback callback) {
        mNotesRemoteDataSource.getNotes(new GetNotesCallback() {
            @Override
            public void onNoteLoaded(List<Note> notes) {
                callback.onNoteLoaded(notes);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveNote(@NonNull Note note) {

    }

    @Override
    public void deleteAllNote() {

    }

    private void refreshLocalDataSource(List<Note> notes) {
        mNotesLocalDataSource.deleteAllNote();
        for (Note note : notes) {
            mNotesLocalDataSource.saveNote(note);
        }
    }


}