package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Note;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public interface NoteDataSource {

    /**
     * TODO: Cargar notas
     */
    interface LoadNoteCallback {

        void onLoaded(List<Note> notes);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetNotesCallback {

        void onNoteLoaded(List<Note> notes);

        void onDataNotAvailable();

    }


    void getNotes(@NonNull LoadNoteCallback callback);

    void refleshNotes(@NonNull LoadNoteCallback callback);

    void getNotes(@NonNull GetNotesCallback callback);

    void saveNote(@NonNull Note note);

    void deleteAllNote();

}