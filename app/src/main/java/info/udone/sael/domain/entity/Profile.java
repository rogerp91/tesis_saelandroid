package info.udone.sael.domain.entity;

/**
 * Created by Roger Patiño on 22/12/2015.
 */

import com.google.gson.annotations.SerializedName;

/**
 * TODO: Entidad perfil
 */
public class Profile {

    @SerializedName("ID")
    private String ID;
    @SerializedName("CEDULA")
    private String CEDULA;
    @SerializedName("NOMBRE1")
    private String NOMBRE1;
    @SerializedName("NOMBRE2")
    private String NOMBRE2;
    @SerializedName("APELLIDO1")
    private String APELLIDO1;
    @SerializedName("APELLIDO2")
    private String APELLIDO2;
    @SerializedName("CORREO")
    private String CORREO;
    @SerializedName("CARRERA2")
    private String CARRERA2;
    @SerializedName("ANO_ING")
    private String ANOING;
    @SerializedName("ESTATUS")
    private String ESTATUS;
    @SerializedName("TELF")
    private String TELF;
    @SerializedName("DIRECCION")
    private String DIRECCION;
    @SerializedName("TITULO")
    private String TITULO;
    @SerializedName("CODIGO")
    private String CODIGO;
    @SerializedName("PROMEDIO")
    private String PROMEDIO;

    @SerializedName("ADMIN_DESKTOP")
    private boolean ADMIN_DESKTOP;
    @SerializedName("ADMIN_MOBILE")
    private boolean ADMIN_MOBILE;

    public boolean getADMIN_DESKTOP() {
        return ADMIN_DESKTOP;
    }

    public void setADMIN_DESKTOP(boolean ADMIN_DESKTOP) {
        this.ADMIN_DESKTOP = ADMIN_DESKTOP;
    }

    public boolean getADMIN_MOBILE() {
        return ADMIN_MOBILE;
    }

    public void setADMIN_MOBILE(boolean ADMIN_MOBILE) {
        this.ADMIN_MOBILE = ADMIN_MOBILE;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCEDULA() {
        return CEDULA;
    }

    public void setCEDULA(String CEDULA) {
        this.CEDULA = CEDULA;
    }

    public String getNOMBRE1() {
        return NOMBRE1;
    }

    public void setNOMBRE1(String NOMBRE1) {
        this.NOMBRE1 = NOMBRE1;
    }

    public String getNOMBRE2() {
        return NOMBRE2;
    }

    public void setNOMBRE2(String NOMBRE2) {
        this.NOMBRE2 = NOMBRE2;
    }

    public String getAPELLIDO1() {
        return APELLIDO1;
    }

    public void setAPELLIDO1(String APELLIDO1) {
        this.APELLIDO1 = APELLIDO1;
    }

    public String getAPELLIDO2() {
        return APELLIDO2;
    }

    public void setAPELLIDO2(String APELLIDO2) {
        this.APELLIDO2 = APELLIDO2;
    }

    public String getCORREO() {
        return CORREO;
    }

    public void setCORREO(String CORREO) {
        this.CORREO = CORREO;
    }

    public String getCARRERA2() {
        return CARRERA2;
    }

    public void setCARRERA2(String CARRERA2) {
        this.CARRERA2 = CARRERA2;
    }

    public String getANOING() {
        return ANOING;
    }

    public void setANOING(String ANOING) {
        this.ANOING = ANOING;
    }

    public String getESTATUS() {
        return ESTATUS;
    }

    public void setESTATUS(String ESTATUS) {
        this.ESTATUS = ESTATUS;
    }

    public String getTELF() {
        return TELF;
    }

    public void setTELF(String TELF) {
        this.TELF = TELF;
    }

    public String getDIRECCION() {
        return DIRECCION;
    }

    public void setDIRECCION(String DIRECCION) {
        this.DIRECCION = DIRECCION;
    }

    public String getTITULO() {
        return TITULO;
    }

    public void setTITULO(String TITULO) {
        this.TITULO = TITULO;
    }

    public String getCODIGO() {
        return CODIGO;
    }

    public void setCODIGO(String CODIGO) {
        this.CODIGO = CODIGO;
    }

    public String getPROMEDIO() {
        return PROMEDIO;
    }

    public void setPROMEDIO(String PROMEDIO) {
        this.PROMEDIO = PROMEDIO;
    }
}