package info.udone.sael.domain.source.local;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;

import info.udone.sael.appeals.AppealsContract;
import info.udone.sael.elective.ElectivesContract;
import info.udone.sael.measure.MeasuresContract;
import info.udone.sael.note.NoteContract;
import info.udone.sael.notification.NotificationContract;
import info.udone.sael.pensum.PensumContract;
import info.udone.sael.readmissions.ReadmissionsContract;
import info.udone.sael.school.SchoolContract;
import info.udone.sael.specialty.SpecialtyContract;
import info.udone.sael.util.AssetUtils;
import info.udone.sael.util.Constants;
import info.udone.sael.util.SQLParser;

public class SQLiteDBHelper extends SQLiteOpenHelper {

    private Context context = null;

    SQLiteDBHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
        this.context = context;
    }

    public String getDatabaseName() {
        return Constants.DATABASE_NAME;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            // Pasar el nombre de la base de dato que va hacer parseada
            execSqlFile(Constants.CREATEFILE, db);
        } catch (Exception e) {
            throw new RuntimeException("Database creation failed", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            for (String sqlFile : AssetUtils.list(Constants.SQL_DIR, context.getAssets())) {
                if (sqlFile.startsWith(Constants.UPGRADEFILE_PREFIX)) {
                    int fileVersion = Integer.parseInt(sqlFile.substring(Constants.UPGRADEFILE_PREFIX.length(), sqlFile.length() - Constants.UPGRADEFILE_SUFFIX.length()));
                    if (fileVersion > oldVersion && fileVersion <= newVersion) {
                        execSqlFile(sqlFile, db);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Database upgrade failed", e);
        }
    }

    private void execSqlFile(String createfile, SQLiteDatabase db) throws SQLException, IOException {
        try {
            for (String sqlInstruction : SQLParser.parseSqlFile(Constants.SQL_DIR + "/" + createfile, context.getAssets())) {
                db.execSQL(sqlInstruction);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    public void onDelete(SQLiteDatabase db) {
        db.delete(AppealsContract.AppealsEntry.TABLE_NAME, null, null);
        db.delete(ElectivesContract.ElectiveEntry.TABLE_NAME, null, null);
        db.delete(MeasuresContract.MeasureEntry.TABLE_NAME, null, null);
        db.delete(NoteContract.NotesEntry.TABLE_NAME, null, null);
        db.delete(NotificationContract.NotificationEntry.TABLE_NAME, null, null);
        db.delete(PensumContract.PensumEntry.TABLE_NAME, null, null);
        db.delete(ReadmissionsContract.ReadmissionsEntry.TABLE_NAME, null, null);
        db.delete(SchoolContract.SchoolEntry.TABLE_NAME, null, null);
        db.delete(SpecialtyContract.SpecialtyEntry.TABLE_NAME, null, null);
        db.close();
    }

    public void onDeleteNotification(SQLiteDatabase db) {
        db.delete(NotificationContract.NotificationEntry.TABLE_NAME, null, null);
        db.close();
    }

}