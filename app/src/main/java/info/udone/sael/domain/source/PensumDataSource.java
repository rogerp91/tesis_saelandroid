package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Materias;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public interface PensumDataSource {

    /**
     * TODO: Cargar Especialidad
     */
    interface LoadPensumCallback {

        void onLoaded(List<Materias> materiases);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetPensumCallback {

        void onPensumLoaded(List<Materias> materiases);

        void onDataNotAvailable();

    }

    void getPensums(String pensum, String code, @NonNull LoadPensumCallback callback);

    void refleshPensum(String pensum, String code, @NonNull LoadPensumCallback callback);

    void getPensums(String pensum, String code, @NonNull GetPensumCallback callback);

    void savePensum(@NonNull Materias materias);

    void deleteAllPensum();

}