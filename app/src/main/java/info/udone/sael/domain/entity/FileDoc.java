package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AndrewX on 22/11/2016.
 */

public class FileDoc {

    @SerializedName("code")
    private String code;

    @SerializedName("doc_id")
    private Integer docId;

    @SerializedName("link")
    private String link;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}