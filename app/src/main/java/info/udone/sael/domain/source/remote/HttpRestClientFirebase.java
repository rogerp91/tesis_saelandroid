package info.udone.sael.domain.source.remote;

import info.udone.sael.BuildConfig;
import info.udone.sael.domain.entity.Host;
import info.udone.sael.domain.entity.version.Version;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public interface HttpRestClientFirebase {

    String BASE_URL = BuildConfig.FIREBAE_HOST;

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET("setting.json")
    Call<Host> getHost();

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET("google_play.json")
    Call<Version> getVersion();

}