package info.udone.sael.domain.entity.version;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AF on 30/12/2016.
 */

public class Version {

    @SerializedName("alpha")
    private Alpha alpha;

    @SerializedName("beta")
    private Beta beta;

    @SerializedName("release")
    private Release release;

    public Alpha getAlpha() {
        return alpha;
    }

    public void setAlpha(Alpha alpha) {
        this.alpha = alpha;
    }

    public Beta getBeta() {
        return beta;
    }

    public void setBeta(Beta beta) {
        this.beta = beta;
    }

    public Release getRelease() {
        return release;
    }

    public void setRelease(Release release) {
        this.release = release;
    }
}