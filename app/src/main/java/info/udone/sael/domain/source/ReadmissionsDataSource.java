package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Readmissions;


/**
 * Created by Roger Patiño on 11/08/2016.
 */

public interface ReadmissionsDataSource {

    /*
    * TODO: Cargar tareas
    */
    interface LoadReadmissionsCallback {

        void onLoaded(List<Readmissions> readmissionses);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetLoadReadmissionsCallback {

        void onLoaded(List<Readmissions> readmissionses);

        void onDataNotAvailable();

    }

    void getLoadReadmissions(@NonNull LoadReadmissionsCallback callback);

    void refleshLoadReadmissions(@NonNull LoadReadmissionsCallback callback);

    void getLoadReadmissions(@NonNull GetLoadReadmissionsCallback callback);

    void saveLoadPReadmissions(@NonNull Readmissions readmissions);

    void deleteAllLoadReadmissions();

}