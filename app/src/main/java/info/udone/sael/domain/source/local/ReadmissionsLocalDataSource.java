package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Readmissions;
import info.udone.sael.domain.source.ReadmissionsDataSource;
import info.udone.sael.readmissions.ReadmissionsContract.ReadmissionsEntry;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class ReadmissionsLocalDataSource implements ReadmissionsDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    ReadmissionsLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void getLoadReadmissions(@NonNull LoadReadmissionsCallback callback) {
        open();
        List<Readmissions> readmissionses = new ArrayList<>();
        String query = "SELECT * FROM " + ReadmissionsEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Readmissions readmissions = new Readmissions();
                readmissions.setId(cursor.getInt(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_ID)));
                readmissions.setANO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_ANO)));
                readmissions.setPER(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_PER)));
                readmissions.setCEDULA(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_CEDULA)));
                readmissions.setCREADO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_CREADO)));
                readmissions.setOFICIO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_OFICIO)));
                readmissions.setFECHA_OFICIO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_FECHA_OFICIO)));
                readmissions.setCOMENTARIO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_COMENTARIO)));
                readmissions.setEGRESADO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_EGRESADO)));
                readmissions.setAPROBADO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_APROBADO)));
                readmissionses.add(readmissions);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (readmissionses.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(readmissionses);
        }
    }

    @Override
    public void refleshLoadReadmissions(@NonNull LoadReadmissionsCallback callback) {
        getLoadReadmissions(callback);
    }

    @Override
    public void getLoadReadmissions(@NonNull GetLoadReadmissionsCallback callback) {
        open();
        List<Readmissions> readmissionses = new ArrayList<>();
        String query = "SELECT * FROM " + ReadmissionsEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Readmissions readmissions = new Readmissions();
                readmissions.setId(cursor.getInt(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_ID)));
                readmissions.setANO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_ANO)));
                readmissions.setPER(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_PER)));
                readmissions.setCEDULA(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_CEDULA)));
                readmissions.setCREADO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_CREADO)));
                readmissions.setOFICIO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_OFICIO)));
                readmissions.setFECHA_OFICIO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_FECHA_OFICIO)));
                readmissions.setCOMENTARIO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_COMENTARIO)));
                readmissions.setEGRESADO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_EGRESADO)));
                readmissions.setAPROBADO(cursor.getString(cursor.getColumnIndex(ReadmissionsEntry.COLUMN_NAME_APROBADO)));
                readmissionses.add(readmissions);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (readmissionses.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(readmissionses);
        }
    }

    @Override
    public void saveLoadPReadmissions(@NonNull Readmissions readmissions) {
        checkNotNull(readmissions);
        open();
        ContentValues values = new ContentValues();
        values.put(ReadmissionsEntry.COLUMN_NAME_ANO, readmissions.getANO());
        values.put(ReadmissionsEntry.COLUMN_NAME_PER, readmissions.getPER());
        values.put(ReadmissionsEntry.COLUMN_NAME_CEDULA, readmissions.getCEDULA());
        values.put(ReadmissionsEntry.COLUMN_NAME_CREADO, readmissions.getCREADO());
        values.put(ReadmissionsEntry.COLUMN_NAME_OFICIO, readmissions.getOFICIO());
        values.put(ReadmissionsEntry.COLUMN_NAME_FECHA_OFICIO, readmissions.getFECHA_OFICIO());
        values.put(ReadmissionsEntry.COLUMN_NAME_CARRERA_NUEVA, readmissions.getCARRERA_NUEVA());
        values.put(ReadmissionsEntry.COLUMN_NAME_COMENTARIO, readmissions.getCOMENTARIO());
        values.put(ReadmissionsEntry.COLUMN_NAME_EGRESADO, readmissions.getEGRESADO());
        values.put(ReadmissionsEntry.COLUMN_NAME_APROBADO, readmissions.getAPROBADO());
        db.insert(ReadmissionsEntry.TABLE_NAME, null, values);
        close();
    }

    @Override
    public void deleteAllLoadReadmissions() {

    }

    private void open() {
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        db.close();
        sqliteDBHelper.close();
    }


}