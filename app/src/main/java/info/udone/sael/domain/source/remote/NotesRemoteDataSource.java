package info.udone.sael.domain.source.remote;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;

import java.util.List;

import javax.inject.Inject;

import info.udone.sael.domain.entity.Note;
import info.udone.sael.domain.source.NoteDataSource;
import info.udone.sael.executor.ThreadExecutor;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class NotesRemoteDataSource implements NoteDataSource {
    private static String TAG = NotesRemoteDataSource.class.getSimpleName();

    private String token = "";
    private ThreadExecutor threadExecutor;
    private String id;

    @Inject
    HttpRestClient client;

//    @Inject
//    ProvideClient provideClient;
//
//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    NotesRemoteDataSource() {
        token = SP.getString(Constants.TOKEN, "");
        threadExecutor = new ThreadExecutor();
        id = SP.getString(Constants.CEDULA, "");
    }

    @Override
    public void getNotes(@NonNull LoadNoteCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
            getNoteRemote(callback);
        });

    }

    @Override
    public void refleshNotes(@NonNull LoadNoteCallback callback) {
        getNotes(callback);
    }

    @Override
    public void getNotes(@NonNull GetNotesCallback callback) {

    }

    @Override
    public void saveNote(@NonNull Note note) {

    }

    @Override
    public void deleteAllNote() {

    }

//    private void getHost(LoadNoteCallback callback) {//Host de firebase data base
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    response.body();
//                    getNote(ClientUtils.getRestClient(provideClient, response.body().getHost()), callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }

    private void getNoteRemote(LoadNoteCallback callback) {
        Call<List<Note>> call = client.getNotes(HttpRestClient.PREFIX + token);
        call.enqueue(new Callback<List<Note>>() {
            @Override
            public void onResponse(Call<List<Note>> call, Response<List<Note>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 404) {
                        callback.onDataNotAvailable();
                    } else {
                        ReportLog.send(TAG + ": Code " + response.code() + " in " + id);
                        callback.onErrorNotSolve();
                    }
                } else {
                    callback.onLoaded(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Note>> call, Throwable t) {
//                    ReportLog.send(t.getMessage());
                callback.onErrorOcurred();
            }
        });
    }

}
