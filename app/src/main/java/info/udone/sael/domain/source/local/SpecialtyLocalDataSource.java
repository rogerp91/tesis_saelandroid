package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Specialtys;
import info.udone.sael.domain.source.SpecialtyDataSource;
import info.udone.sael.specialty.SpecialtyContract.SpecialtyEntry;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class SpecialtyLocalDataSource implements SpecialtyDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    SpecialtyLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void getSpecialtys(@NonNull LoadSpecialtyCallback callback) {
        open();
        List<Specialtys> specialtyses = new ArrayList<>();
        String query = "SELECT * FROM " + SpecialtyEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Specialtys specialtys = new Specialtys();
                specialtys.setId(cursor.getInt(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_ID)));
                specialtys.setCODIGO(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_CODIGO)));
                specialtys.setTITULO(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_TITULO)));
                specialtys.setPENSUM(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_PENSUM)));
                specialtyses.add(specialtys);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (specialtyses.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(specialtyses);
        }
    }

    @Override
    public void refleshSpecialty(@NonNull LoadSpecialtyCallback callback) {
        getSpecialtys(callback);
    }

    @Override
    public void getSpecialtys(@NonNull GetSpecialtyCallback callback) {
        open();
        List<Specialtys> specialtyses = new ArrayList<>();
        String query = "SELECT * FROM " + SpecialtyEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Specialtys specialtys = new Specialtys();
                specialtys.setId(cursor.getInt(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_ID)));
                specialtys.setCODIGO(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_CODIGO)));
                specialtys.setTITULO(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_TITULO)));
                specialtys.setPENSUM(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_PENSUM)));
                specialtyses.add(specialtys);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (specialtyses.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onSpecialtyLoaded(specialtyses);
        }
    }

    @Override
    public void getSpecialty(@NonNull String code, @NonNull GetSpecialtyOnlyCallback callback) {
        open();
        Specialtys specialtys = null;
        String query = "SELECT * FROM " + SpecialtyEntry.TABLE_NAME + " WHERE " + SpecialtyEntry.COLUMN_NAME_CODIGO + " = " + code;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                specialtys = new Specialtys();
                specialtys.setId(cursor.getInt(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_ID)));
                specialtys.setCODIGO(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_CODIGO)));
                specialtys.setTITULO(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_TITULO)));
                specialtys.setPENSUM(cursor.getString(cursor.getColumnIndex(SpecialtyEntry.COLUMN_NAME_PENSUM)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (specialtys == null) {
            callback.onDataNotAvailable();
        } else {
            callback.onSpecialtyLoaded(specialtys);
        }
    }

    @Override
    public void saveSpecialty(@NonNull Specialtys specialtys) {
        open();
        checkNotNull(specialtys);
        ContentValues values = new ContentValues();
        values.put(SpecialtyEntry.COLUMN_NAME_CODIGO, specialtys.getCODIGO());
        values.put(SpecialtyEntry.COLUMN_NAME_TITULO, specialtys.getTITULO());
        values.put(SpecialtyEntry.COLUMN_NAME_PENSUM, specialtys.getPENSUM());
        db.insert(SpecialtyEntry.TABLE_NAME, null, values);
        close();
    }

    @Override
    public void deleteAllSpecialty() {
        open();
        db.delete(SpecialtyEntry.TABLE_NAME, null, null);
        close();
    }

    private void open() {
        //Log.d(TAG, "Se abre conexion a la base de datos " + sqliteDBHelper.getDatabaseName());
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        //Log.d(TAG, "Se cierra conexion a la base de datos " + sqliteDBHelper.getDatabaseName());
        db.close();
        sqliteDBHelper.close();
    }

}