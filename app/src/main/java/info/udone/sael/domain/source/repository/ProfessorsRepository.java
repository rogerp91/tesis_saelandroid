package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Professors;
import info.udone.sael.domain.source.ProfessorsDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class ProfessorsRepository implements ProfessorsDataSource {

    private ProfessorsDataSource localDataSource;
    private ProfessorsDataSource remoteDataSource;

    @Inject
    ProfessorsRepository(@Named("re_profesor_l") ProfessorsDataSource localDataSource, @Named("re_profesor_r") ProfessorsDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public void getProfessors(@NonNull LoadProfessorsCallback callback) {
        checkNotNull(callback);
        localDataSource.getProfessors(new ProfessorsDataSource.GetProfessorsCallback() {
            @Override
            public void onLoaded(List<Professors> professorses) {
                callback.onLoaded(professorses);
            }

            @Override
            public void onDataNotAvailable() {
                remoteDataSource.getProfessors(new ProfessorsDataSource.LoadProfessorsCallback() {
                    @Override
                    public void onLoaded(List<Professors> professorses) {
                        refreshLocalDataSource(professorses);
                        callback.onLoaded(professorses);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });
    }

    @Override
    public void refleshProfessors(@NonNull LoadProfessorsCallback callback) {
        remoteDataSource.getProfessors(new ProfessorsDataSource.LoadProfessorsCallback() {
            @Override
            public void onLoaded(List<Professors> professorses) {
                refreshLocalDataSource(professorses);
                callback.onLoaded(professorses);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getProfessors(@NonNull GetProfessorsCallback callback) {
        localDataSource.getProfessors(new ProfessorsDataSource.GetProfessorsCallback() {
            @Override
            public void onLoaded(List<Professors> professorses) {
                callback.onLoaded(professorses);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveProfessors(@NonNull Professors professor) {

    }

    @Override
    public void deleteAllProfessors() {
        localDataSource.deleteAllProfessors();
    }

    private void refreshLocalDataSource(List<Professors> professorses) {
        localDataSource.deleteAllProfessors();
        for (Professors professors : professorses) {
            localDataSource.saveProfessors(professors);
        }
    }
}