package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AndrewX on 23/11/2016.
 */

public class CreateNotification {

    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;
    @SerializedName("time")
    private String time;
    @SerializedName("from_to")
    private String fromTo;
    @SerializedName("carrera")
    private String carrera;
    @SerializedName("cedula")
    private String cedula;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFromTo() {
        return fromTo;
    }

    public void setFromTo(String fromTo) {
        this.fromTo = fromTo;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
}