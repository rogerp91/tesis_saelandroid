package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 03/08/2016.
 */
public class Specialtys {

    @SerializedName("CODIGO")
    private String CODIGO;
    @SerializedName("TITULO")
    private String TITULO;
    @SerializedName("PENSUM")
    private String PENSUM;

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCODIGO() {
        return CODIGO;
    }

    public void setCODIGO(String CODIGO) {
        this.CODIGO = CODIGO;
    }

    public String getTITULO() {
        return TITULO;
    }

    public void setTITULO(String TITULO) {
        this.TITULO = TITULO;
    }

    public String getPENSUM() {
        return PENSUM;
    }

    public void setPENSUM(String PENSUM) {
        this.PENSUM = PENSUM;
    }
}