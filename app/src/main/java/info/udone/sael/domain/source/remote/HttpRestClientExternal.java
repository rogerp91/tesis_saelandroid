package info.udone.sael.domain.source.remote;

import info.udone.sael.domain.entity.Doc;
import info.udone.sael.domain.entity.FileDoc;
import info.udone.sael.domain.entity.Photo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public interface HttpRestClientExternal {

    //21219756 y 4 22998372 y 5 20903635 y1
    String URL = "https://mypcplanet.com/sael-api-restful/";

    @GET("api/v1/photo.php?cedula={cd}")
    Call<Photo> getPhoto(@Header("Authorization") String token, @Query("cd") String cd);

    @Headers({
            "Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9",
            "Accept:application/json",
            "User-Agent:Sael-Mobile/4"
    })
    @GET("doc_check.php")
    Call<Doc> getDocCheck(@Query("cedula") String cedula);

    @Headers({
            "Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9",
            "Accept:application/json",
            "User-Agent:Sael-Mobile/3"
    })
    @GET("doc_generate.php")
    Call<FileDoc> getDoc(@Query("cedula") String cedula, @Query("doc_modo") String doc_modo, @Query("ci") int ci, @Query("ce") int ce, @Query("re") int re);

    @GET("{url}")
    @Streaming
    Call<ResponseBody> downloadFile(@Path("url") String url);

}