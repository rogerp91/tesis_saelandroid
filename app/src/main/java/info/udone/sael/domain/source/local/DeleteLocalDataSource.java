package info.udone.sael.domain.source.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.source.DeleteDataSource;


/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class DeleteLocalDataSource implements DeleteDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    public DeleteLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
        db = sqliteDBHelper.getWritableDatabase();
    }

    @Override
    public void deleteAll() {
        sqliteDBHelper.onDelete(db);
        db.close();
        sqliteDBHelper.close();
    }

    @Override
    public void deleteNotification() {
        sqliteDBHelper.onDeleteNotification(db);
        db.close();
        sqliteDBHelper.close();
    }

}