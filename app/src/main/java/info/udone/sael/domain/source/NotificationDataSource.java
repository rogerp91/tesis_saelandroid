package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Notification;


/**
 * Created by Roger Patiño on 15/08/2016.
 */

public interface NotificationDataSource {

    interface GetNotificationCallback {

        void onLoaded(List<Notification> notifications);

        void onDataNotAvailable();

    }

    void refleshNotification(@NonNull GetNotificationCallback callback);

    void getNotification(@NonNull GetNotificationCallback callback);

    void saveNotification(@NonNull Notification notification);

    void deleteAllNotification();

}