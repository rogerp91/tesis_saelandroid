package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 29/08/2016.
 */

/**
 * TODO: "i": "Interno",
 * TODO:"e": "Externo (requiere Pago y Firma del Jefe)",
 * TODO:"b": "Beca Funjemasu (solicitud o renovacion)",
 * TODO:"f": "Fontur-NE (solicitud de tarjeta o recarga)"
 */
public class DocModo {

    @SerializedName("i")
    private String i;
    @SerializedName("e")
    private String e;
    @SerializedName("b")
    private String b;
    @SerializedName("f")
    private String f;

    public String getI() {
        return i;
    }

    public void setI(String i) {
        this.i = i;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }
}