package info.udone.sael.domain.source;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public interface DeleteDataSource {

    void deleteAll();

    void deleteNotification();
}