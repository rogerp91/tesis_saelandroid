package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.domain.source.NotificationDataSource;
import info.udone.sael.notification.NotificationContract.NotificationEntry;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class NotificationLocalDataSource implements NotificationDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    public NotificationLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void refleshNotification(@NonNull GetNotificationCallback callback) {
        open();
        List<Notification> notifications = new ArrayList<>();
        String query = "SELECT * FROM " + NotificationEntry.TABLE_NAME + " ORDER BY " + NotificationEntry.COLUMN_NAME_TIME + " DESC";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Notification notification = new Notification();
                notification.setId(cursor.getInt(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_ID)));
                notification.setBody(cursor.getString(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_BODY)));
                notification.setFroms(cursor.getString(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_FROM)));
                notification.setTime(cursor.getString(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_TIME)));
                notification.setTitle(cursor.getString(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_TITLE)));
                notifications.add(notification);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        //Log.d(TAG, "getNotification: " + Integer.toString(notifications.size()));
        if (notifications.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(notifications);
        }
    }

    @Override
    public void getNotification(@NonNull GetNotificationCallback callback) {
        open();
        List<Notification> notifications = new ArrayList<>();
        String query = "SELECT * FROM " + NotificationEntry.TABLE_NAME + " ORDER BY " + NotificationEntry.COLUMN_NAME_TIME + " DESC";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Notification notification = new Notification();
                notification.setId(cursor.getInt(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_ID)));
                notification.setBody(cursor.getString(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_BODY)));
                notification.setFroms(cursor.getString(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_FROM)));
                notification.setTime(cursor.getString(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_TIME)));
                notification.setTitle(cursor.getString(cursor.getColumnIndex(NotificationEntry.COLUMN_NAME_TITLE)));
                notifications.add(notification);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        //Log.d(TAG, "getNotification: " + Integer.toString(notifications.size()));
        if (notifications.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(notifications);
        }
    }

    @Override
    public void saveNotification(@NonNull Notification notification) {
        checkNotNull(notification);
        open();
        ContentValues values = new ContentValues();
        values.put(NotificationEntry.COLUMN_NAME_BODY, notification.getBody());
        values.put(NotificationEntry.COLUMN_NAME_FROM, notification.getFroms());
        values.put(NotificationEntry.COLUMN_NAME_TITLE, notification.getTitle());
        db.insert(NotificationEntry.TABLE_NAME, null, values);
        close();
    }

    @Override
    public void deleteAllNotification() {
        open();
        db.delete(NotificationEntry.TABLE_NAME, null, null);
        close();
    }

    private void open() {
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        db.close();
        sqliteDBHelper.close();
    }

}