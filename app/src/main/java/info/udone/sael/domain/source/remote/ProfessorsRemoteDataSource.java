package info.udone.sael.domain.source.remote;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;

import java.util.List;

import javax.inject.Inject;

import info.udone.sael.domain.entity.Professors;
import info.udone.sael.domain.source.ProfessorsDataSource;
import info.udone.sael.executor.ThreadExecutor;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class ProfessorsRemoteDataSource implements ProfessorsDataSource {

    private static String TAG = ProfessorsRemoteDataSource.class.getSimpleName();

    private String token = "";
    private ThreadExecutor threadExecutor;
    private String id;

//    @Inject
//    ProvideClient provideClient;
//
//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    HttpRestClient client;

    @Inject
    ProfessorsRemoteDataSource() {
        token = SP.getString(Constants.TOKEN, "");
        threadExecutor = new ThreadExecutor();
        id = SP.getString(Constants.CEDULA, "");
    }

    @Override
    public void getProfessors(@NonNull LoadProfessorsCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
            getProfessorsRemote(callback);
        });
    }

    @Override
    public void refleshProfessors(@NonNull LoadProfessorsCallback callback) {

    }

    @Override
    public void getProfessors(@NonNull GetProfessorsCallback callback) {

    }

    @Override
    public void saveProfessors(@NonNull Professors professor) {

    }

    @Override
    public void deleteAllProfessors() {

    }

//    private void getHost(LoadProfessorsCallback callback) {//Host de firebase data base
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    response.body();
//                    getProfessors(ClientUtils.getRestClient(provideClient, response.body().getHost()), callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }

    private void getProfessorsRemote(LoadProfessorsCallback callback) {
        Call<List<Professors>> call = client.getProfessors(HttpRestClient.PREFIX + token);
        call.enqueue(new Callback<List<Professors>>() {
            @Override
            public void onResponse(Call<List<Professors>> call, Response<List<Professors>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 404) {
                        callback.onDataNotAvailable();
                    } else {
                        ReportLog.send(TAG + ": Code " + response.code() + " in " + id);
                        callback.onErrorNotSolve();
                    }
                } else {
                    callback.onLoaded(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Professors>> call, Throwable t) {
//                    ReportLog.send(t.getMessage());
                callback.onErrorOcurred();
            }
        });
    }


}
