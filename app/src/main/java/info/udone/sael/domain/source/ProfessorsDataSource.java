package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Professors;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public interface ProfessorsDataSource {

    /*
    * TODO: Cargar tareas
    */
    interface LoadProfessorsCallback {

        void onLoaded(List<Professors> professorses);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetProfessorsCallback {

        void onLoaded(List<Professors> professorses);

        void onDataNotAvailable();

    }

    void getProfessors(@NonNull LoadProfessorsCallback callback);

    void refleshProfessors(@NonNull LoadProfessorsCallback callback);

    void getProfessors(@NonNull GetProfessorsCallback callback);

    void saveProfessors(@NonNull Professors professor);

    void deleteAllProfessors();

}