package info.udone.sael.domain.entity.version;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AF on 30/12/2016.
 */

public class Release {

    @SerializedName("active")
    private Boolean active;
    @SerializedName("version_code")
    private Integer versionCode;
    @SerializedName("version_name")
    private String versionName;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

}