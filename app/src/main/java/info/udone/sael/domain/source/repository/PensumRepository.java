package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Materias;
import info.udone.sael.domain.source.PensumDataSource;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class PensumRepository implements PensumDataSource {

    private PensumDataSource pensumRemoteDataSource;
    private PensumDataSource pensumLocalDataSource;

    @Inject
    PensumRepository(@Named("re_pensum_r") PensumDataSource pensumRemoteDataSource, @Named("re_pensum_l") PensumDataSource pensumLocalDataSource) {
        this.pensumRemoteDataSource = pensumRemoteDataSource;
        this.pensumLocalDataSource = pensumLocalDataSource;
    }

    @Override
    public void getPensums(String pensum, String code, @NonNull LoadPensumCallback callback) {
        Log.d("PensumRepository", "getPensums");

        pensumLocalDataSource.getPensums(pensum, code, new GetPensumCallback() {
            @Override
            public void onPensumLoaded(List<Materias> materiases) {
                Log.d("PensumRepository", "onPensumLoaded");
                callback.onLoaded(materiases);
            }

            @Override
            public void onDataNotAvailable() {
                Log.d("PensumRepository", "onDataNotAvailable");
                pensumRemoteDataSource.getPensums(pensum, code, new LoadPensumCallback() {
                    @Override
                    public void onLoaded(List<Materias> materiases) {
                        refreshLocalDataSource(materiases);
                        callback.onLoaded(materiases);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });
    }

    @Override
    public void refleshPensum(String pensum, String code, @NonNull LoadPensumCallback callback) {
        pensumRemoteDataSource.getPensums(pensum, code, new LoadPensumCallback() {
            @Override
            public void onLoaded(List<Materias> materiases) {
                refreshLocalDataSource(materiases);
                callback.onLoaded(materiases);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getPensums(String pensum, String code, @NonNull GetPensumCallback callback) {
        pensumLocalDataSource.getPensums(pensum, code, new GetPensumCallback() {
            @Override
            public void onPensumLoaded(List<Materias> materiases) {
                callback.onPensumLoaded(materiases);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void savePensum(@NonNull Materias materias) {

    }

    @Override
    public void deleteAllPensum() {
        pensumLocalDataSource.deleteAllPensum();
    }

    private void refreshLocalDataSource(List<Materias> materiases) {
        pensumLocalDataSource.deleteAllPensum();
        for (int i = 0; i < materiases.size(); i++) {
            Materias materias = materiases.get(i);
            materias.setId(i);
            pensumLocalDataSource.savePensum(materias);
        }
    }

}