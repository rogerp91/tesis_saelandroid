package info.udone.sael.domain.entity;

/**
 * Created by AndrewX on 22/11/2016.
 */

public class DocumentGenerate {

    private String cedula;
    private String doc_modo;
    private int ci;
    private int ce;
    private int re;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDoc_modo() {
        return doc_modo;
    }

    public void setDoc_modo(String doc_modo) {
        this.doc_modo = doc_modo;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci) {
        this.ci = ci;
    }

    public int getCe() {
        return ce;
    }

    public void setCe(int ce) {
        this.ce = ce;
    }

    public int getRe() {
        return re;
    }

    public void setRe(int re) {
        this.re = re;
    }
}