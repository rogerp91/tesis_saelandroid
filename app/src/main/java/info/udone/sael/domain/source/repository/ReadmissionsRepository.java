package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Readmissions;
import info.udone.sael.domain.source.ReadmissionsDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class ReadmissionsRepository implements ReadmissionsDataSource {

    private ReadmissionsDataSource localDataSource;
    private ReadmissionsDataSource remoteDataSource;

    @Inject
    ReadmissionsRepository(@Named("re_readmission_l") ReadmissionsDataSource localDataSource, @Named("re_readmission_r") ReadmissionsDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public void getLoadReadmissions(@NonNull LoadReadmissionsCallback callback) {
        checkNotNull(callback);
        localDataSource.getLoadReadmissions(new GetLoadReadmissionsCallback() {
            @Override
            public void onLoaded(List<Readmissions> readmissionses) {
                callback.onLoaded(readmissionses);
            }

            @Override
            public void onDataNotAvailable() {
                remoteDataSource.getLoadReadmissions(new LoadReadmissionsCallback() {
                    @Override
                    public void onLoaded(List<Readmissions> readmissionses) {
                        refreshLocalDataSource(readmissionses);
                        localDataSource.getLoadReadmissions(new GetLoadReadmissionsCallback() {
                            @Override
                            public void onLoaded(List<Readmissions> readmissionses) {
                                callback.onLoaded(readmissionses);
                            }

                            @Override
                            public void onDataNotAvailable() {

                            }
                        });
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });
    }

    @Override
    public void refleshLoadReadmissions(@NonNull LoadReadmissionsCallback callback) {
        remoteDataSource.getLoadReadmissions(new LoadReadmissionsCallback() {
            @Override
            public void onLoaded(List<Readmissions> readmissionses) {
                refreshLocalDataSource(readmissionses);
                callback.onLoaded(readmissionses);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getLoadReadmissions(@NonNull GetLoadReadmissionsCallback callback) {
        localDataSource.getLoadReadmissions(new GetLoadReadmissionsCallback() {
            @Override
            public void onLoaded(List<Readmissions> readmissionses) {
                callback.onLoaded(readmissionses);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveLoadPReadmissions(@NonNull Readmissions readmissions) {

    }

    @Override
    public void deleteAllLoadReadmissions() {
        localDataSource.deleteAllLoadReadmissions();
    }

    private void refreshLocalDataSource(List<Readmissions> readmissionses) {
        localDataSource.deleteAllLoadReadmissions();
        for (Readmissions readmissions: readmissionses) {
            localDataSource.saveLoadPReadmissions(readmissions);
        }
    }

}