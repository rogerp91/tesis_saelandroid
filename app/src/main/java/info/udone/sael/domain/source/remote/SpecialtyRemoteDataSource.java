package info.udone.sael.domain.source.remote;

import android.support.annotation.NonNull;
import android.util.Log;

import com.github.rogerp91.pref.SP;

import java.util.List;

import javax.inject.Inject;

import info.udone.sael.domain.entity.Specialtys;
import info.udone.sael.domain.source.SpecialtyDataSource;
import info.udone.sael.executor.ThreadExecutor;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class SpecialtyRemoteDataSource implements SpecialtyDataSource {

    private static String TAG = SpecialtyRemoteDataSource.class.getSimpleName();

    private String token = "";
    private ThreadExecutor threadExecutor;
    private String id;

//    @Inject
//    ProvideClient provideClient;
//
//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    HttpRestClient client;

    @Inject
    SpecialtyRemoteDataSource() {
        token = SP.getString(Constants.TOKEN, "");
        threadExecutor = new ThreadExecutor();
        id = SP.getString(Constants.CEDULA, "");
    }

    @Override
    public void getSpecialtys(@NonNull LoadSpecialtyCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
            getSpecialtysRemote(callback);
        });
    }

    @Override
    public void refleshSpecialty(@NonNull LoadSpecialtyCallback callback) {
        getSpecialtys(callback);
    }

    @Override
    public void getSpecialtys(@NonNull GetSpecialtyCallback callback) {

    }

    @Override
    public void getSpecialty(@NonNull String code, @NonNull GetSpecialtyOnlyCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
            getSpecialtyRemote(code, callback);
        });
    }

    @Override
    public void saveSpecialty(@NonNull Specialtys specialtys) {

    }

    @Override
    public void deleteAllSpecialty() {

    }
    //========================== 1 ==========================

    /**
     * @param callback //     * @see SpecialtyRemoteDataSource#getSpecialtys(HttpRestClient, LoadSpecialtyCallback)
     */
//    private void getHost1(LoadSpecialtyCallback callback) {//Host de firebase data base
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    getSpecialtys(ClientUtils.getRestClient(provideClient, response.body().getHost()), callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }
    private void getSpecialtysRemote(LoadSpecialtyCallback callback) {
        Call<List<Specialtys>> call = client.getSpecialtys(HttpRestClient.PREFIX + token);
        call.enqueue(new Callback<List<Specialtys>>() {
            @Override
            public void onResponse(Call<List<Specialtys>> call, Response<List<Specialtys>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 404) {
                        callback.onDataNotAvailable();
                    } else {
                        callback.onErrorNotSolve();
                    }
                } else {
                    callback.onLoaded(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Specialtys>> call, Throwable t) {
                callback.onErrorOcurred();
            }
        });
    }

    //========================== 1 ==========================

    //========================== 2 ==========================

    /**
     * @param code
     * @param callback //     * @see SpecialtyRemoteDataSource#getSpecialty(HttpRestClient, String, GetSpecialtyOnlyCallback)
     */
//    private void getHost2(String code, GetSpecialtyOnlyCallback callback) {//Host de firebase data base
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    getSpecialty(ClientUtils.getRestClient(provideClient, response.body().getHost()), code, callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }
    private void getSpecialtyRemote(@NonNull String code, @NonNull GetSpecialtyOnlyCallback callback) {
        Call<Specialtys> call = client.getSpecialty(HttpRestClient.PREFIX + token, code);
        call.enqueue(new Callback<Specialtys>() {
            @Override
            public void onResponse(Call<Specialtys> call, Response<Specialtys> response) {
                Log.d(TAG, response.raw().toString());
                if (!response.isSuccessful()) {
                    if (response.code() == 404) {
                        callback.onDataNotAvailable();
                    } else {
                        ReportLog.send(TAG + ": Code " + response.code() + " in " + id);
                        callback.onErrorNotSolve();
                    }
                } else {
                    callback.onSpecialtyLoaded(response.body());
                }
            }

            @Override
            public void onFailure(Call<Specialtys> call, Throwable t) {
                callback.onErrorOcurred();
            }
        });

    }
    //========================== 2 ==========================
}
