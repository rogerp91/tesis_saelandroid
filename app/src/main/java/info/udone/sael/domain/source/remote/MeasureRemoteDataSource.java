package info.udone.sael.domain.source.remote;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;

import java.util.List;

import javax.inject.Inject;

import info.udone.sael.domain.entity.Measures;
import info.udone.sael.domain.source.MeasureDataSource;
import info.udone.sael.executor.ThreadExecutor;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class MeasureRemoteDataSource implements MeasureDataSource {

    private static String TAG = MeasureRemoteDataSource.class.getSimpleName();

    private String token = "";
    private ThreadExecutor threadExecutor;
    private String id;

    @Inject
    HttpRestClient client;

//    @Inject
//    ProvideClient provideClient;
//
//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    MeasureRemoteDataSource() {
        token = SP.getString(Constants.TOKEN, "");
        threadExecutor = new ThreadExecutor();
        id = SP.getString(Constants.CEDULA, "");
    }


    @Override
    public void getMeasure(@NonNull LoadMeasureCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
            getMeasureRemote(callback);//Host de firebase data base
        });
    }

    @Override
    public void refleshMeasure(@NonNull LoadMeasureCallback callback) {

    }

    @Override
    public void getMeasure(@NonNull GetMeasureCallback callback) {

    }

    @Override
    public void saveMeasure(@NonNull Measures measures) {

    }

    @Override
    public void deleteAllMeasure() {

    }

//    private void getHost(LoadMeasureCallback callback) {//Host de firebase data base
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    getMeasure(ClientUtils.getRestClient(provideClient, response.body().getHost()), callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }

    private void getMeasureRemote(LoadMeasureCallback callback) {
        Call<List<Measures>> call = client.getMeasures(HttpRestClient.PREFIX + token);
        call.enqueue(new Callback<List<Measures>>() {
            @Override
            public void onResponse(Call<List<Measures>> call, Response<List<Measures>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 404) {
                        callback.onDataNotAvailable();
                    } else {
                        ReportLog.send(TAG + ": Code " + response.code() + " in " + id);
                        callback.onErrorNotSolve();
                    }
                } else {
                    callback.onLoaded(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Measures>> call, Throwable t) {
//                    ReportLog.send(t.getMessage());
                callback.onErrorOcurred();
            }
        });
    }


}