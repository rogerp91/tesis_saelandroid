package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 20/08/2016.
 */

public class Doc {

    @SerializedName("code")
    private String code;
    @SerializedName("status")
    private String status;
    @SerializedName("doc_modo")
    private DocModo docModo;
    @SerializedName("doc_tipo")
    private DocTipo docTipo;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DocModo getDocModo() {
        return docModo;
    }

    public void setDocModo(DocModo docModo) {
        this.docModo = docModo;
    }

    public DocTipo getDocTipo() {
        return docTipo;
    }

    public void setDocTipo(DocTipo docTipo) {
        this.docTipo = docTipo;
    }
}