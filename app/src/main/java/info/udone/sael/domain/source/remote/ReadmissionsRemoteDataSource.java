package info.udone.sael.domain.source.remote;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;

import java.util.List;

import javax.inject.Inject;

import info.udone.sael.domain.entity.Readmissions;
import info.udone.sael.domain.source.ReadmissionsDataSource;
import info.udone.sael.executor.ThreadExecutor;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class ReadmissionsRemoteDataSource implements ReadmissionsDataSource {

    private static String TAG = ReadmissionsRemoteDataSource.class.getSimpleName();

    private String token = "";
    private ThreadExecutor threadExecutor;
    private String id;

    @Inject
    HttpRestClient client;

//    @Inject
//    ProvideClient provideClient;
//
//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    ReadmissionsRemoteDataSource() {
        token = SP.getString(Constants.TOKEN, "");
        threadExecutor = new ThreadExecutor();
        id = SP.getString(Constants.CEDULA, "");
    }

    @Override
    public void getLoadReadmissions(@NonNull LoadReadmissionsCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
            getReadmissionsRemote(callback);
        });
    }

    @Override
    public void refleshLoadReadmissions(@NonNull LoadReadmissionsCallback callback) {

    }

    @Override
    public void getLoadReadmissions(@NonNull GetLoadReadmissionsCallback callback) {

    }

    @Override
    public void saveLoadPReadmissions(@NonNull Readmissions readmissions) {

    }

    @Override
    public void deleteAllLoadReadmissions() {

    }

//    private void getHost(LoadReadmissionsCallback callback) {//Host de firebase data base
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    response.body();
//                    getReadmissions(ClientUtils.getRestClient(provideClient, response.body().getHost()), callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }

    private void getReadmissionsRemote(LoadReadmissionsCallback callback) {
        Call<List<Readmissions>> call = client.getReadmissions(HttpRestClient.PREFIX + token);
        call.enqueue(new Callback<List<Readmissions>>() {
            @Override
            public void onResponse(Call<List<Readmissions>> call, Response<List<Readmissions>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 404) {
                        callback.onDataNotAvailable();
                    } else {
                        ReportLog.send(TAG + ": Code " + response.code() + " in " + id);
                        callback.onErrorNotSolve();
                    }
                } else {
                    callback.onLoaded(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Readmissions>> call, Throwable t) {
//                    ReportLog.send(t.getMessage());
                callback.onErrorOcurred();
            }
        });
    }

}
