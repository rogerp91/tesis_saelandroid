package info.udone.sael.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class PushNotification {

    private String carrera;
    private String cedula;
    private String proridad;
    private String from_to;
    private String body;
    private String title;
    private String time;

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getProridad() {
        return proridad;
    }

    public void setProridad(String proridad) {
        this.proridad = proridad;
    }

    public String getFrom_to() {
        return from_to;
    }

    public void setFrom_to(String from_to) {
        this.from_to = from_to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}