package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Electives;
import info.udone.sael.domain.entity.Requisito;
import info.udone.sael.domain.source.ElectiveDataSource;
import info.udone.sael.elective.ElectivesContract.ElectiveEntry;
import info.udone.sael.elective.ElectivesContract.RequisitoEntry;
import info.udone.sael.util.FormatDBString;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class ElectiveLocalDataSource implements ElectiveDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    ElectiveLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void getElectives(String pensum, String code, @NonNull LoadElectiveCallback callback) {
        open();
        List<Electives> electivesList = new ArrayList<>();
        String query = "SELECT * FROM " + ElectiveEntry.TABLE_NAME + " WHERE CARRERA=" + FormatDBString.getFormatDBString(code) + " AND PENSUM=" + FormatDBString.getFormatDBString(pensum);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Electives electives = new Electives();
                electives.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_ID))));
                electives.setCARRERA(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CARRERA)));
                electives.setPENSUM(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_PENSUM)));
                electives.setNOMBRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_NOMBRE)));
                electives.setTIPO_MATERIA(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_TIPO_MATERIA)));
                electives.setTIPO_MATERIA_NOMBRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_TIPO_MATERIA_NOMBRE)));
                electives.setSEMESTRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_SEMESTRE)));
                electives.setCREDITOS(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CREDITOS)));
                electives.setESPECIAL(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_ESPECIAL)));
                String codigo = cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CODIGO));
                electives.setCODIGO(codigo);
                electives.setREQUISITOS(getRequisito(codigo));
                electivesList.add(electives);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (electivesList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(electivesList);
        }
    }

    @Override
    public void refleshElectives(String pensum, String code, @NonNull LoadElectiveCallback callback) {
        open();
        List<Electives> electivesList = new ArrayList<>();
        String query = "SELECT * FROM " + ElectiveEntry.TABLE_NAME + " WHERE CARRERA=" + FormatDBString.getFormatDBString(code) + " AND PENSUM=" + FormatDBString.getFormatDBString(pensum);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Electives electives = new Electives();
                electives.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_ID))));
                electives.setCARRERA(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CARRERA)));
                electives.setPENSUM(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_PENSUM)));
                electives.setNOMBRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_NOMBRE)));
                electives.setTIPO_MATERIA(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_TIPO_MATERIA)));
                electives.setTIPO_MATERIA_NOMBRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_TIPO_MATERIA_NOMBRE)));
                electives.setSEMESTRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_SEMESTRE)));
                electives.setCREDITOS(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CREDITOS)));
                electives.setESPECIAL(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_ESPECIAL)));
                String codigo = cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CODIGO));
                electives.setCODIGO(codigo);
                electives.setREQUISITOS(getRequisito(codigo));
                electivesList.add(electives);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (electivesList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(electivesList);
        }
    }

    @Override
    public void getElectives(String pensum, String code, @NonNull GetElectivesCallback callback) {
        open();
        List<Electives> electivesList = new ArrayList<>();
        String query = "SELECT * FROM " + ElectiveEntry.TABLE_NAME + " WHERE CARRERA=" + FormatDBString.getFormatDBString(code) + " AND PENSUM=" + FormatDBString.getFormatDBString(pensum);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Electives electives = new Electives();
                electives.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_ID))));
                electives.setCARRERA(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CARRERA)));
                electives.setPENSUM(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_PENSUM)));
                electives.setNOMBRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_NOMBRE)));
                electives.setTIPO_MATERIA(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_TIPO_MATERIA)));
                electives.setTIPO_MATERIA_NOMBRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_TIPO_MATERIA_NOMBRE)));
                electives.setSEMESTRE(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_SEMESTRE)));
                electives.setCREDITOS(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CREDITOS)));
                electives.setESPECIAL(cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_ESPECIAL)));
                String codigo = cursor.getString(cursor.getColumnIndex(ElectiveEntry.COLUMN_NAME_CODIGO));
                electives.setCODIGO(codigo);
                electives.setREQUISITOS(getRequisito(codigo));
                electivesList.add(electives);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (electivesList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onElectivesLoaded(electivesList);
        }
    }

    @Override
    public void saveElective(@NonNull Electives electives) {
        checkNotNull(electives);
        open();
        ContentValues values = new ContentValues();
        String replaced = electives.getCODIGO().replaceAll("x", "1");
        values.put(ElectiveEntry.COLUMN_NAME_CODIGO, replaced);
        values.put(ElectiveEntry.COLUMN_NAME_CARRERA, electives.getCARRERA());
        values.put(ElectiveEntry.COLUMN_NAME_PENSUM, electives.getPENSUM());
        values.put(ElectiveEntry.COLUMN_NAME_NOMBRE, electives.getNOMBRE());
        values.put(ElectiveEntry.COLUMN_NAME_TIPO_MATERIA, electives.getTIPO_MATERIA());
        values.put(ElectiveEntry.COLUMN_NAME_TIPO_MATERIA_NOMBRE, electives.getTIPO_MATERIA_NOMBRE());
        values.put(ElectiveEntry.COLUMN_NAME_SEMESTRE, electives.getSEMESTRE());
        values.put(ElectiveEntry.COLUMN_NAME_CREDITOS, electives.getCREDITOS());
        values.put(ElectiveEntry.COLUMN_NAME_ESPECIAL, electives.getESPECIAL());
        db.insert(ElectiveEntry.TABLE_NAME, null, values);
        if (electives.getREQUISITOS() != null) {
            for (Requisito requisito : electives.getREQUISITOS())
                saveRequisito(replaced, requisito);
        }
        close();
    }

    @Override
    public void deleteAllElectives() {
        open();
        db.delete(ElectiveEntry.TABLE_NAME, null, null);
        close();
    }

    private void saveRequisito(String father, Requisito requisito) {
        ContentValues values = new ContentValues();
        values.put(RequisitoEntry.COLUMN_NAME_MATERIA_PADRE, father);
        String replaced = requisito.getCODIGO().replaceAll("x", "1");
        values.put(RequisitoEntry.COLUMN_NAME_CODIGO, replaced);
        values.put(RequisitoEntry.COLUMN_NAME_NOMBRE, requisito.getNOMBRE());
        db.insert(RequisitoEntry.TABLE_NAME, null, values);
    }

    private List<Requisito> getRequisito(String codigo) {
        List<Requisito> requisitos = new ArrayList<>();
        String query = "SELECT * FROM " + RequisitoEntry.TABLE_NAME + " WHERE " + RequisitoEntry.COLUMN_NAME_MATERIA_PADRE + "=" + FormatDBString.getFormatDBString(codigo);
        try {
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    Requisito requisito = new Requisito();
                    requisito.setMATERIA_PADRE(cursor.getString(cursor.getColumnIndex(RequisitoEntry.COLUMN_NAME_MATERIA_PADRE)));
                    requisito.setCODIGO(cursor.getString(cursor.getColumnIndex(RequisitoEntry.COLUMN_NAME_CODIGO)));
                    requisito.setNOMBRE(cursor.getString(cursor.getColumnIndex(RequisitoEntry.COLUMN_NAME_NOMBRE)));
                    requisitos.add(requisito);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return requisitos;
    }


    private void open() {
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        db.close();
        sqliteDBHelper.close();
    }
}