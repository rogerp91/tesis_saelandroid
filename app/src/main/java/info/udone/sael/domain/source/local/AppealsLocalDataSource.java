package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.appeals.AppealsContract.AppealsEntry;
import info.udone.sael.domain.entity.Appeals;
import info.udone.sael.domain.source.AppealsDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 02/08/2016.
 */
public class AppealsLocalDataSource implements AppealsDataSource {

    private String TAG = AppealsLocalDataSource.class.getSimpleName();

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    AppealsLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void getAppeals(@NonNull LoadAppealCallback callback) {
        open();
        List<Appeals> appealses = new ArrayList<>();
        String query = "SELECT * FROM " + AppealsEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Appeals appeals = new Appeals();
                appeals.setId(cursor.getInt(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_ID)));
                appeals.setANO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_ANO)));
                appeals.setCREADO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_CREADO)));
                appeals.setPER(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_PER)));
                appeals.setSUBTIPO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_SUBTIPO)));
                appeals.setTIPO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_TIPO)));
                appeals.setMOTIVO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_MOTIVO)));
                appeals.setCOMENTARIO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_COMENTARIO)));
                appealses.add(appeals);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (appealses.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(appealses);
        }

    }

    @Override
    public void refleshAppeals(@NonNull LoadAppealCallback callback) {
        getAppeals(callback);
    }

    @Override
    public void getAppeals(@NonNull GetAppealCallback callback) {
        open();
        List<Appeals> appealses = new ArrayList<>();
        String query = "SELECT * FROM " + AppealsEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Appeals appeals = new Appeals();
                appeals.setANO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_ANO)));
                appeals.setCREADO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_CREADO)));
                appeals.setPER(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_PER)));
                appeals.setSUBTIPO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_SUBTIPO)));
                appeals.setTIPO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_TIPO)));
                appeals.setMOTIVO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_MOTIVO)));
                appeals.setCOMENTARIO(cursor.getString(cursor.getColumnIndex(AppealsEntry.COLUMN_NAME_COMENTARIO)));
                appealses.add(appeals);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (appealses.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(appealses);
        }
    }

    @Override
    public void saveAppeal(@NonNull Appeals appeals) {
        checkNotNull(appeals);
        open();
        ContentValues values = new ContentValues();
        values.put(AppealsEntry.COLUMN_NAME_ANO, appeals.getANO());
        values.put(AppealsEntry.COLUMN_NAME_CREADO, appeals.getCREADO());
        values.put(AppealsEntry.COLUMN_NAME_PER, appeals.getPER());
        values.put(AppealsEntry.COLUMN_NAME_SUBTIPO, appeals.getSUBTIPO());
        values.put(AppealsEntry.COLUMN_NAME_TIPO, appeals.getTIPO());
        values.put(AppealsEntry.COLUMN_NAME_MOTIVO, appeals.getMOTIVO());
        values.put(AppealsEntry.COLUMN_NAME_COMENTARIO, appeals.getCOMENTARIO());
        db.insert(AppealsEntry.TABLE_NAME, null, values);
        close();
    }

    @Override
    public void deleteAllAppeals() {
        open();
        db.delete(AppealsEntry.TABLE_NAME, null, null);
        close();
    }

    private void open() {
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        db.close();
        sqliteDBHelper.close();
    }

}