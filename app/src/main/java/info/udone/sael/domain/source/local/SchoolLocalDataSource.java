package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.School;
import info.udone.sael.domain.source.SchoolDataSource;
import info.udone.sael.school.SchoolContract.SchoolEntry;
import info.udone.sael.util.Constants;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 09/08/2016.
 */

public class SchoolLocalDataSource implements SchoolDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    SchoolLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    private void open() {
        //Log.d(TAG, "Se abre conexion a la base de datos " + sqliteDBHelper.getDatabaseName());
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        //Log.d(TAG, "Se cierra conexion a la base de datos " + sqliteDBHelper.getDatabaseName());
        db.close();
        sqliteDBHelper.close();
    }

    @Override
    public void getSchool(@NonNull LoadSchoolCallback callback) {
        open();
        List<School> schoolList = new ArrayList<>();
        String query = "SELECT * FROM " + Constants.TABLE_SCHOOL;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                School school = new School();
                school.setCODIGO(cursor.getString(cursor.getColumnIndex(SchoolEntry.COLUMN_NAME_CODIGO)));
                school.setDESCRIPCION(cursor.getString(cursor.getColumnIndex(SchoolEntry.COLUMN_NAME_DESCRIPTION)));
                school.setESTATUS(cursor.getString(cursor.getColumnIndex(SchoolEntry.COLUMN_NAME_STATUS)));
                schoolList.add(school);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (schoolList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(schoolList);
        }
    }

    @Override
    public void refleshSchool(@NonNull LoadSchoolCallback callback) {
        getSchool(callback);
    }

    @Override
    public void getSchool(@NonNull GetSchoolCallback callback) {
        open();
        List<School> schoolList = new ArrayList<>();
        String query = "SELECT * FROM " + SchoolEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                School school = new School();
                school.setCODIGO(cursor.getString(cursor.getColumnIndex(SchoolEntry.COLUMN_NAME_CODIGO)));
                school.setDESCRIPCION(cursor.getString(cursor.getColumnIndex(SchoolEntry.COLUMN_NAME_DESCRIPTION)));
                school.setESTATUS(cursor.getString(cursor.getColumnIndex(SchoolEntry.COLUMN_NAME_STATUS)));
                schoolList.add(school);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (schoolList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(schoolList);
        }
    }

    @Override
    public void saveSchool(@NonNull School school) {
        open();
        checkNotNull(school);
        ContentValues values = new ContentValues();
        values.put(SchoolEntry.COLUMN_NAME_CODIGO, school.getCODIGO());
        values.put(SchoolEntry.COLUMN_NAME_DESCRIPTION, school.getDESCRIPCION());
        values.put(SchoolEntry.COLUMN_NAME_STATUS, school.getESTATUS());
        db.insert(SchoolEntry.TABLE_NAME, null, values);
        close();
    }

    @Override
    public void deleteAllSchool() {
        open();
        db.delete(SchoolEntry.TABLE_NAME, null, null);
        db.close();
    }
}