package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 03/08/2016.
 */
public class Note{

    private int id;
    @SerializedName("CODIGO")
    private String CODIGO;
    @SerializedName("NOMBRE")
    private String NOMBRE;
    @SerializedName("ULT_ANO")
    private String ULT_ANO;
    @SerializedName("ULT_PER")
    private String ULT_PER;
    @SerializedName("ULT_NOTA")
    private String ULT_NOTA;
    @SerializedName("INSCRITA")
    private String INSCRITA;
    @SerializedName("RETIRADA")
    private String RETIRADA;
    @SerializedName("REPROBADA")
    private String REPROBADA;
    @SerializedName("NOREPORTADA")
    private String NOREPORTADA;
    @SerializedName("APROBADA")
    private String APROBADA;
    @SerializedName("EJECUCION")
    private String EJECUCION;
    @SerializedName("OTRA")
    private String OTRA;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getCODIGO() {
        return CODIGO;
    }

    public void setCODIGO(String CODIGO) {
        this.CODIGO = CODIGO;
    }

    public String getULT_ANO() {
        return ULT_ANO;
    }

    public void setULT_ANO(String ULT_ANO) {
        this.ULT_ANO = ULT_ANO;
    }

    public String getULT_PER() {
        return ULT_PER;
    }

    public void setULT_PER(String ULT_PER) {
        this.ULT_PER = ULT_PER;
    }

    public String getULT_NOTA() {
        return ULT_NOTA;
    }

    public void setULT_NOTA(String ULT_NOTA) {
        this.ULT_NOTA = ULT_NOTA;
    }

    public String getINSCRITA() {
        return INSCRITA;
    }

    public void setINSCRITA(String INSCRITA) {
        this.INSCRITA = INSCRITA;
    }

    public String getRETIRADA() {
        return RETIRADA;
    }

    public void setRETIRADA(String RETIRADA) {
        this.RETIRADA = RETIRADA;
    }

    public String getREPROBADA() {
        return REPROBADA;
    }

    public void setREPROBADA(String REPROBADA) {
        this.REPROBADA = REPROBADA;
    }

    public String getNOREPORTADA() {
        return NOREPORTADA;
    }

    public void setNOREPORTADA(String NOREPORTADA) {
        this.NOREPORTADA = NOREPORTADA;
    }

    public String getAPROBADA() {
        return APROBADA;
    }

    public void setAPROBADA(String APROBADA) {
        this.APROBADA = APROBADA;
    }

    public String getEJECUCION() {
        return EJECUCION;
    }

    public void setEJECUCION(String EJECUCION) {
        this.EJECUCION = EJECUCION;
    }

    public String getOTRA() {
        return OTRA;
    }

    public void setOTRA(String OTRA) {
        this.OTRA = OTRA;
    }
}