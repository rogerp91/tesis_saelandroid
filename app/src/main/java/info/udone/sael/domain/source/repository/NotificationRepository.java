package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Notification;
import info.udone.sael.domain.source.NotificationDataSource;


/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class NotificationRepository implements NotificationDataSource {

    private NotificationDataSource local;

    @Inject
    NotificationRepository(@Named("re_notification_l") NotificationDataSource local) {
        this.local = local;
    }

    @Override
    public void refleshNotification(@NonNull GetNotificationCallback callback) {
        local.getNotification(new GetNotificationCallback() {
            @Override
            public void onLoaded(List<Notification> notifications) {
                callback.onLoaded(notifications);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void getNotification(@NonNull GetNotificationCallback callback) {
        local.getNotification(new GetNotificationCallback() {
            @Override
            public void onLoaded(List<Notification> notifications) {
                callback.onLoaded(notifications);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveNotification(@NonNull Notification notification) {
        local.saveNotification(notification);
    }

    @Override
    public void deleteAllNotification() {
        local.deleteAllNotification();
    }
}