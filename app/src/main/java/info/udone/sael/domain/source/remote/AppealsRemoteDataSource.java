package info.udone.sael.domain.source.remote;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;

import java.util.List;

import javax.inject.Inject;

import info.udone.sael.domain.entity.Appeals;
import info.udone.sael.domain.entity.Host;
import info.udone.sael.domain.entity.ProvideClient;
import info.udone.sael.domain.source.AppealsDataSource;
import info.udone.sael.executor.ThreadExecutor;
import info.udone.sael.util.ClientUtils;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class AppealsRemoteDataSource implements AppealsDataSource {

    private static String TAG = AppealsRemoteDataSource.class.getSimpleName();

    private String token = "";
    private ThreadExecutor threadExecutor;
    private String id;

    @Inject
    HttpRestClient client;

//    @Inject
//    ProvideClient provideClient;
//
//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    AppealsRemoteDataSource() {
        token = SP.getString(Constants.TOKEN, "");
        id = SP.getString(Constants.CEDULA, "");
        threadExecutor = new ThreadExecutor();
    }

    @Override
    public void getAppeals(@NonNull final LoadAppealCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
            getAppealsRemote(callback);
        });
    }

    @Override
    public void refleshAppeals(@NonNull LoadAppealCallback callback) {
        getAppeals(callback);
    }

    @Override
    public void getAppeals(@NonNull GetAppealCallback callback) {

    }

    @Override
    public void saveAppeal(@NonNull Appeals appeals) {

    }

    @Override
    public void deleteAllAppeals() {

    }

//    private void getHost(LoadAppealCallback callback) {
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    getAppeals(ClientUtils.getRestClient(provideClient, response.body().getHost()), callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }

    //    private void getAppeals(HttpRestClient client, LoadAppealCallback callback) {
    private void getAppealsRemote(LoadAppealCallback callback) {
        Call<List<Appeals>> call = client.getAppeals(HttpRestClient.PREFIX + token);
        call.enqueue(new Callback<List<Appeals>>() {
            @Override
            public void onResponse(Call<List<Appeals>> call, Response<List<Appeals>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 404) {
                        callback.onDataNotAvailable();
                    } else {
                        ReportLog.send(TAG + ": Code " + response.code() + " in " + id);
                        callback.onErrorNotSolve();
                    }
                } else {
                    callback.onLoaded(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Appeals>> call, Throwable t) {
                callback.onErrorOcurred();
            }
        });
    }

}