package info.udone.sael.domain.entity;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class LoginWithProfile {

    private String token;
    private String expires;
    private Profile profile;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}