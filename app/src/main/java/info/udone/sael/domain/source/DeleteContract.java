package info.udone.sael.domain.source;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public interface DeleteContract {

    // TODO: Presentador
    interface Presenter {

        void deleteAll();

        void deleteNotification();

    }
}