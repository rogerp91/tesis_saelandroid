package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Professors;
import info.udone.sael.domain.source.ProfessorsDataSource;
import info.udone.sael.measure.MeasuresContract;
import info.udone.sael.professors.ProfessorsContract.ProfessorsEntry;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class ProfessorsLocalDataSource implements ProfessorsDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    ProfessorsLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void getProfessors(@NonNull LoadProfessorsCallback callback) {
        open();
        List<Professors> professorses = new ArrayList<>();
        String query = "SELECT * FROM " + MeasuresContract.MeasureEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Professors professors = new Professors();
                professors.setId(cursor.getInt(cursor.getColumnIndex(ProfessorsEntry.COLUMN_NAME_ID)));
                professors.setTIT(cursor.getString(cursor.getColumnIndex(ProfessorsEntry.COLUMN_NAME_TIT)));
                professors.setCEDULA(cursor.getString(cursor.getColumnIndex(ProfessorsEntry.COLUMN_NAME_CEDULA)));
                professors.setNOMBRES(cursor.getString(cursor.getColumnIndex(ProfessorsEntry.COLUMN_NAME_NOMBRES)));
                professorses.add(professors);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (professorses.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(professorses);
        }
    }

    @Override
    public void refleshProfessors(@NonNull LoadProfessorsCallback callback) {
        getProfessors(callback);
    }

    @Override
    public void getProfessors(@NonNull GetProfessorsCallback callback) {
        open();
        List<Professors> professorses = new ArrayList<>();
        String query = "SELECT * FROM " + MeasuresContract.MeasureEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Professors professors = new Professors();
                professors.setId(cursor.getInt(cursor.getColumnIndex(ProfessorsEntry.COLUMN_NAME_ID)));
                professors.setTIT(cursor.getString(cursor.getColumnIndex(ProfessorsEntry.COLUMN_NAME_TIT)));
                professors.setCEDULA(cursor.getString(cursor.getColumnIndex(ProfessorsEntry.COLUMN_NAME_CEDULA)));
                professors.setNOMBRES(cursor.getString(cursor.getColumnIndex(ProfessorsEntry.COLUMN_NAME_NOMBRES)));
                professorses.add(professors);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (professorses.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(professorses);
        }
    }

    @Override
    public void saveProfessors(@NonNull Professors professor) {
        checkNotNull(professor);
        open();
        ContentValues values = new ContentValues();
        values.put(ProfessorsEntry.COLUMN_NAME_TIT, professor.getTIT());
        values.put(ProfessorsEntry.COLUMN_NAME_CEDULA, professor.getCEDULA());
        values.put(ProfessorsEntry.COLUMN_NAME_NOMBRES, professor.getNOMBRES());
        db.insert(ProfessorsEntry.TABLE_NAME, null, values);
        close();
    }

    @Override
    public void deleteAllProfessors() {
        open();
        db.delete(ProfessorsEntry.TABLE_NAME, null, null);
        close();
    }

    private void open() {
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        db.close();
        sqliteDBHelper.close();
    }
}