package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class Appeals {

    public int id;
    @SerializedName("ANO")
    private String ANO;
    @SerializedName("PER")
    private String PER;
    @SerializedName("CREADO")
    private String CREADO;
    @SerializedName("TIPO")
    private String TIPO;
    @SerializedName("SUBTIPO")
    private String SUBTIPO;
    @SerializedName("MOTIVO")
    private String MOTIVO;
    @SerializedName("COMENTARIO")
    private String COMENTARIO;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getANO() {
        return ANO;
    }

    public void setANO(String ANO) {
        this.ANO = ANO;
    }

    public String getPER() {
        return PER;
    }

    public void setPER(String PER) {
        this.PER = PER;
    }

    public String getCREADO() {
        return CREADO;
    }

    public void setCREADO(String CREADO) {
        this.CREADO = CREADO;
    }

    public String getTIPO() {
        return TIPO;
    }

    public void setTIPO(String TIPO) {
        this.TIPO = TIPO;
    }

    public String getSUBTIPO() {
        return SUBTIPO;
    }

    public void setSUBTIPO(String SUBTIPO) {
        this.SUBTIPO = SUBTIPO;
    }

    public String getMOTIVO() {
        return MOTIVO;
    }

    public void setMOTIVO(String MOTIVO) {
        this.MOTIVO = MOTIVO;
    }

    public String getCOMENTARIO() {
        return COMENTARIO;
    }

    public void setCOMENTARIO(String COMENTARIO) {
        this.COMENTARIO = COMENTARIO;
    }
}