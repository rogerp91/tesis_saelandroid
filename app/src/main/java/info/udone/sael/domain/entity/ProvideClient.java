package info.udone.sael.domain.entity;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;

/**
 * Created by AF on 29/12/2016.
 */

public class ProvideClient {

    private OkHttpClient okHttpClient;
    private Gson gson;

    public ProvideClient(OkHttpClient okHttpClient, Gson gson) {
        this.okHttpClient = okHttpClient;
        this.gson = gson;
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public void setOkHttpClient(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    public Gson getGson() {
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
