package info.udone.sael.domain.source.remote;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;
import com.koushikdutta.ion.Ion;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Materias;
import info.udone.sael.domain.source.PensumDataSource;
import info.udone.sael.executor.ThreadExecutor;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Convertion;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;

import static info.udone.sael.domain.source.remote.HttpRestClient.VERSION;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class PensumRemoteDataSource implements PensumDataSource {


    private static String TAG = PensumRemoteDataSource.class.getSimpleName();

    private String token = "";
    private ThreadExecutor threadExecutor;
    private String id;

    @Inject
    HttpRestClient client;

//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    PensumRemoteDataSource() {
        token = SP.getString(Constants.TOKEN, "");
        threadExecutor = new ThreadExecutor();
        id = SP.getString(Constants.CEDULA, "");
    }

    @Override
    public void getPensums(String pensum, String code, @NonNull LoadPensumCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
            getPensumRemote(pensum, code, callback);
        });
    }

    @Override
    public void refleshPensum(String pensum, String code, @NonNull LoadPensumCallback callback) {
        getPensums(pensum, code, callback);
    }

    @Override
    public void getPensums(String pensum, String code, @NonNull GetPensumCallback callback) {

    }

    @Override
    public void savePensum(@NonNull Materias materias) {

    }

    @Override
    public void deleteAllPensum() {

    }

//    private void getHost(String pensum, String code, @NonNull LoadPensumCallback callback) {
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    getPensum(response.body().getHost(), pensum, code, callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }

    private void getPensumRemote(String pensum, String code, @NonNull LoadPensumCallback callback) {
        Ion.with(Sael.getContext())
                .load("GET", HttpRestClient.BASE_URL + VERSION + "specialty/" + code + "/pensum/" + pensum)
                .addHeader("Authorization", HttpRestClient.PREFIX + token)
                .setTimeout(45000)
                .asJsonArray()
                .withResponse()
                .setCallback((e, result) -> {
                    if (e != null) {
                        callback.onErrorOcurred();
                    } else {
                        if (result.getHeaders().code() == 404) {
                            callback.onDataNotAvailable();
                        } else {
                            if (result.getHeaders().code() == 500) {
                                ReportLog.send(TAG + ": Code " + result.getHeaders().code() + " in " + id);
                                callback.onErrorNotSolve();
                            } else {
                                if (result.getHeaders().code() > 199 && result.getHeaders().code() < 300) {
                                    callback.onLoaded(Convertion.getPensumArray(result.getResult()));
                                }
                            }
                        }
                    }
                });
    }

}
