package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class Requisito {

    private String MATERIA_PADRE;

    @SerializedName("CODIGO")
    private String CODIGO;
    @SerializedName("NOMBRE")
    private String NOMBRE;

    public String getMATERIA_PADRE() {
        return MATERIA_PADRE;
    }

    public void setMATERIA_PADRE(String MATERIA_PADRE) {
        this.MATERIA_PADRE = MATERIA_PADRE;
    }

    public String getCODIGO() {
        return CODIGO;
    }

    public void setCODIGO(String CODIGO) {
        this.CODIGO = CODIGO;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

}