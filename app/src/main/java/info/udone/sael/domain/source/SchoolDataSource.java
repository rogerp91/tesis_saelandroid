package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.School;


/**
 * Created by Roger Patiño on 02/08/2016.
 */

public interface SchoolDataSource {

    /**
     * TODO: Cargar tareas
     */
    interface LoadSchoolCallback {

        void onLoaded(List<School> schools);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetSchoolCallback {

        void onLoaded(List<School> schools);

        void onDataNotAvailable();

    }

    void getSchool(@NonNull LoadSchoolCallback callback);

    void refleshSchool(@NonNull LoadSchoolCallback callback);

    void getSchool(@NonNull GetSchoolCallback callback);

    void saveSchool(@NonNull School school);

    void deleteAllSchool();

}