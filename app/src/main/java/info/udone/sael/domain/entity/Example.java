package info.udone.sael.domain.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roger Patiño on 13/08/2016.
 */

public class Example {

    private String semestre;
    private List<Materias> materiases;


    public Example() {
        this.materiases = new ArrayList<>();
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public List<Materias> getMateriases() {
        return materiases;
    }

    public void setMateriases(List<Materias> materiases) {
        this.materiases = materiases;
    }
}
