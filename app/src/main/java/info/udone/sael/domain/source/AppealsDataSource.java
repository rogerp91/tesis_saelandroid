package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Appeals;


/**
 * Created by Roger Patiño on 02/08/2016.
 */

public interface AppealsDataSource {

    /**
     * TODO: Cargar tareas
     */
    interface LoadAppealCallback {

        void onLoaded(List<Appeals> appealses);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetAppealCallback {

        void onLoaded(List<Appeals> appealses);

        void onDataNotAvailable();

    }

    void getAppeals(@NonNull LoadAppealCallback callback);

    void refleshAppeals(@NonNull LoadAppealCallback callback);

    void getAppeals(@NonNull GetAppealCallback callback);

    void saveAppeal(@NonNull Appeals appeals);

    void deleteAllAppeals();

}