package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class Readmissions {

    private int id;
    @SerializedName("ANO")
    private String ANO;
    @SerializedName("PER")
    private String PER;
    @SerializedName("CEDULA")
    private String CEDULA;
    @SerializedName("CREADO")
    private String CREADO;
    @SerializedName("OFICIO")
    private String OFICIO;
    @SerializedName("FECHA_OFICIO")
    private String FECHA_OFICIO;
    @SerializedName("CARRERA_NUEVA")
    private String CARRERA_NUEVA;
    @SerializedName("COMENTARIO")
    private String COMENTARIO;
    @SerializedName("EGRESADO")
    private String EGRESADO;
    @SerializedName("APROBADO")
    private String APROBADO;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getANO() {
        return ANO;
    }

    public void setANO(String ANO) {
        this.ANO = ANO;
    }

    public String getPER() {
        return PER;
    }

    public void setPER(String PER) {
        this.PER = PER;
    }

    public String getCEDULA() {
        return CEDULA;
    }

    public void setCEDULA(String CEDULA) {
        this.CEDULA = CEDULA;
    }

    public String getCREADO() {
        return CREADO;
    }

    public void setCREADO(String CREADO) {
        this.CREADO = CREADO;
    }

    public String getOFICIO() {
        return OFICIO;
    }

    public void setOFICIO(String OFICIO) {
        this.OFICIO = OFICIO;
    }

    public String getFECHA_OFICIO() {
        return FECHA_OFICIO;
    }

    public void setFECHA_OFICIO(String FECHA_OFICIO) {
        this.FECHA_OFICIO = FECHA_OFICIO;
    }

    public String getCARRERA_NUEVA() {
        return CARRERA_NUEVA;
    }

    public void setCARRERA_NUEVA(String CARRERA_NUEVA) {
        this.CARRERA_NUEVA = CARRERA_NUEVA;
    }

    public String getCOMENTARIO() {
        return COMENTARIO;
    }

    public void setCOMENTARIO(String COMENTARIO) {
        this.COMENTARIO = COMENTARIO;
    }

    public String getEGRESADO() {
        return EGRESADO;
    }

    public void setEGRESADO(String EGRESADO) {
        this.EGRESADO = EGRESADO;
    }

    public String getAPROBADO() {
        return APROBADO;
    }

    public void setAPROBADO(String APROBADO) {
        this.APROBADO = APROBADO;
    }
}