package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Note;
import info.udone.sael.domain.source.NoteDataSource;
import info.udone.sael.note.NoteContract.NotesEntry;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class NotesLocalDataSource implements NoteDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    NotesLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void getNotes(@NonNull LoadNoteCallback callback) {
        open();
        List<Note> noteList = new ArrayList<>();
        String query = "SELECT * FROM " + NotesEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(cursor.getInt(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_ID)));
                note.setNOMBRE(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_NOMBRE)));
                note.setCODIGO(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_CODIGO)));
                note.setULT_ANO(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_ULT_ANO)));
                note.setULT_PER(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_ULT_PER)));
                note.setULT_NOTA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_ULT_NOTA)));
                note.setINSCRITA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_INSCRITA)));
                note.setRETIRADA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_RETIRADA)));
                note.setREPROBADA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_REPROBADA)));
                note.setNOREPORTADA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_NOREPORTADA)));
                note.setAPROBADA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_APROBADA)));
                note.setEJECUCION(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_EJECUCION)));
                note.setOTRA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_OTRA)));
                noteList.add(note);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (noteList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(noteList);
        }
    }

    @Override
    public void refleshNotes(@NonNull LoadNoteCallback callback) {
        getNotes(callback);
    }

    @Override
    public void getNotes(@NonNull GetNotesCallback callback) {
        open();
        List<Note> noteList = new ArrayList<>();
        String query = "SELECT * FROM " + NotesEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(cursor.getInt(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_ID)));
                note.setNOMBRE(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_NOMBRE)));
                note.setCODIGO(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_CODIGO)));
                note.setULT_ANO(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_ULT_ANO)));
                note.setULT_PER(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_ULT_PER)));
                note.setULT_NOTA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_ULT_NOTA)));
                note.setINSCRITA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_INSCRITA)));
                note.setRETIRADA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_RETIRADA)));
                note.setREPROBADA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_REPROBADA)));
                note.setNOREPORTADA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_NOREPORTADA)));
                note.setAPROBADA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_APROBADA)));
                note.setEJECUCION(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_EJECUCION)));
                note.setOTRA(cursor.getString(cursor.getColumnIndex(NotesEntry.COLUMN_NAME_OTRA)));
                noteList.add(note);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (noteList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onNoteLoaded(noteList);
        }
    }

    @Override
    public void saveNote(@NonNull Note note) {
        checkNotNull(note);
        open();
        ContentValues values = new ContentValues();
        values.put(NotesEntry.COLUMN_NAME_NOMBRE, note.getNOMBRE());
        values.put(NotesEntry.COLUMN_NAME_CODIGO, note.getCODIGO());
        values.put(NotesEntry.COLUMN_NAME_ULT_ANO, note.getULT_ANO());
        values.put(NotesEntry.COLUMN_NAME_ULT_PER, note.getULT_PER());
        values.put(NotesEntry.COLUMN_NAME_ULT_NOTA, note.getULT_NOTA());
        values.put(NotesEntry.COLUMN_NAME_INSCRITA, note.getINSCRITA());
        values.put(NotesEntry.COLUMN_NAME_RETIRADA, note.getRETIRADA());
        values.put(NotesEntry.COLUMN_NAME_REPROBADA, note.getREPROBADA());
        values.put(NotesEntry.COLUMN_NAME_NOREPORTADA, note.getNOREPORTADA());
        values.put(NotesEntry.COLUMN_NAME_APROBADA, note.getAPROBADA());
        values.put(NotesEntry.COLUMN_NAME_EJECUCION, note.getEJECUCION());
        values.put(NotesEntry.COLUMN_NAME_OTRA, note.getOTRA());
        db.insert(NotesEntry.TABLE_NAME, null, values);
        close();
    }

    @Override
    public void deleteAllNote() {
        open();
        db.delete(NotesEntry.TABLE_NAME, null, null);
        close();
    }

    private void open() {
        //Log.d(TAG, "Se abre conexion a la base de datos " + sqliteDBHelper.getDatabaseName());
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        //Log.d(TAG, "Se cierra conexion a la base de datos " + sqliteDBHelper.getDatabaseName());
        db.close();
        sqliteDBHelper.close();
    }


}