package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class Measures {

    private int id;

    @SerializedName("ANO")
    private String ANO;
    @SerializedName("PER")
    private String PER;
    @SerializedName("CEDULA")
    private String CEDULA;
    @SerializedName("MEDIDA")
    private String MEDIDA;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getANO() {
        return ANO;
    }

    public void setANO(String ANO) {
        this.ANO = ANO;
    }

    public String getPER() {
        return PER;
    }

    public void setPER(String PER) {
        this.PER = PER;
    }

    public String getCEDULA() {
        return CEDULA;
    }

    public void setCEDULA(String CEDULA) {
        this.CEDULA = CEDULA;
    }

    public String getMEDIDA() {
        return MEDIDA;
    }

    public void setMEDIDA(String MEDIDA) {
        this.MEDIDA = MEDIDA;
    }
}