package info.udone.sael.domain.source.repository;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.source.DeleteDataSource;


/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class DeleteRepository implements DeleteDataSource {

    private DeleteDataSource source;

    @Inject
    DeleteRepository(@Named("re_delete_l") DeleteDataSource source) {
        this.source = source;
    }

    @Override
    public void deleteAll() {
        source.deleteAll();
    }

    @Override
    public void deleteNotification() {
        source.deleteNotification();
    }
}
