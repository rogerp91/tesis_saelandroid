package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 29/08/2016.
 */

public class DocTipo {

    @SerializedName("ci")
    private String ci; //inscripción
    @SerializedName("ce")
    private String ce; //estudio
    @SerializedName("re")
    private String re;//record

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getCe() {
        return ce;
    }

    public void setCe(String ce) {
        this.ce = ce;
    }

    public String getRe() {
        return re;
    }

    public void setRe(String re) {
        this.re = re;
    }
}