package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Specialtys;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public interface SpecialtyDataSource {

    /**
     * TODO: Cargar Especialidad
     */
    interface LoadSpecialtyCallback {

        void onLoaded(List<Specialtys> specialtyses);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetSpecialtyCallback {

        void onSpecialtyLoaded(List<Specialtys> specialtyses);

        void onDataNotAvailable();

    }

    interface GetSpecialtyOnlyCallback {

        void onSpecialtyLoaded(Specialtys specialtys);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    void getSpecialtys(@NonNull LoadSpecialtyCallback callback);

    void refleshSpecialty(@NonNull LoadSpecialtyCallback callback);

    void getSpecialtys(@NonNull GetSpecialtyCallback callback);

    void getSpecialty(@NonNull String code, @NonNull GetSpecialtyOnlyCallback callback);

    void saveSpecialty(@NonNull Specialtys specialtys);

    void deleteAllSpecialty();
}