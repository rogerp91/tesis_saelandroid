package info.udone.sael.domain.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by AndrewX on 21/11/2016.
 */

public class University {

    private int id;
    private String name;
    private Drawable avatar;
    private Drawable color;

    public University(int id, String name, Drawable avatar, Drawable color) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getAvatar() {
        return avatar;
    }

    public void setAvatar(Drawable avatar) {
        this.avatar = avatar;
    }

    public Drawable getColor() {
        return color;
    }

    public void setColor(Drawable color) {
        this.color = color;
    }
}