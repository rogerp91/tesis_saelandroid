package info.udone.sael.domain.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AF on 29/12/2016.
 */

public class Host {

    @SerializedName("host")
    private String host;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
