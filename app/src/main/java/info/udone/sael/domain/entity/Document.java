package info.udone.sael.domain.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by AF on 26/12/2016.
 */

public class Document {

    private int id;
    private String title;
    private Drawable icon;
    private String sub_title;


    public Document(int id, String title, Drawable icon, String sub_title) {
        this.id = id;
        this.title = title;
        this.icon = icon;
        this.sub_title = sub_title;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public Document(int id, String title, Drawable icon) {
        this.id = id;
        this.title = title;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }
}
