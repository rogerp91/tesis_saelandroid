package info.udone.sael.domain.source.remote;

import android.support.annotation.NonNull;

import com.github.rogerp91.pref.SP;
import com.koushikdutta.ion.Ion;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Electives;
import info.udone.sael.domain.source.ElectiveDataSource;
import info.udone.sael.executor.ThreadExecutor;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Convertion;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;

import static info.udone.sael.domain.source.remote.HttpRestClient.VERSION;


/**
 * Created by Roger Patiño on 09/08/2016.
 */

public class ElectiveRemoteDataSource implements ElectiveDataSource {

    private static String TAG = ElectiveRemoteDataSource.class.getSimpleName();

    private String token = "";
    private ThreadExecutor threadExecutor;
    private String id;

    @Inject
    HttpRestClient client;

//    @Inject
//    HttpRestClientFirebase hostFirebase;

    @Inject
    ElectiveRemoteDataSource() {
        token = SP.getString(Constants.TOKEN, "");
        id = SP.getString(Constants.CEDULA, "");
        threadExecutor = new ThreadExecutor();
    }

    @Override
    public void getElectives(String pensum, String code, @NonNull LoadElectiveCallback callback) {
        threadExecutor.run(() -> {
            if (!Networks.isOnline(null)) {
                callback.onErrorNetwork();
                return;
            }
//            getHost(pensum, code, callback);

            getElectiveRemote(pensum, code, callback);

        });
    }

    @Override
    public void refleshElectives(String pensum, String code, @NonNull LoadElectiveCallback callback) {

    }

    @Override
    public void getElectives(String pensum, String code, @NonNull GetElectivesCallback callback) {

    }

    @Override
    public void saveElective(@NonNull Electives electives) {

    }

    @Override
    public void deleteAllElectives() {

    }

//    private void getHost(String pensum, String code, @NonNull LoadElectiveCallback callback) {
//        Call<Host> call = hostFirebase.getHost();
//        call.enqueue(new retrofit2.Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (response.isSuccessful()) {
//                    response.body();
//                    getElective(response.body().getHost(), pensum, code, callback);
//                } else {
//                    ReportLog.send("Error ocurred Firebase rest api: " + response.code());
//                    callback.onErrorNotSolve();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                callback.onErrorOcurred();
//            }
//        });
//    }

    private void getElectiveRemote(String pensum, String code, @NonNull LoadElectiveCallback callback) {
        Ion.with(Sael.getContext())
                .load("GET", HttpRestClient.BASE_URL + VERSION + "specialty/" + code + "/pensum/" + pensum + "/electives")
                .addHeader("Authorization", HttpRestClient.PREFIX + token)
                .setTimeout(45000)
                .asJsonArray()
                .withResponse()
                .setCallback((e, result) -> {
                    if (e != null) {
                        callback.onErrorOcurred();
                    } else {
                        if (result.getHeaders().code() == 404) {
                            ReportLog.send(TAG + ": Code " + result.getHeaders().code() + " in " + id);
                            callback.onDataNotAvailable();
                        } else {
                            if (result.getHeaders().code() == 500) {
                                ReportLog.send(TAG + ": Code " + result.getHeaders().code() + " in " + id);
                                callback.onErrorNotSolve();
                            } else {
                                if (result.getHeaders().code() > 199 && result.getHeaders().code() < 300) {
                                    callback.onLoaded(Convertion.getElectiveArray(result.getResult()));
                                }
                            }
                        }
                    }
                });
    }


}
