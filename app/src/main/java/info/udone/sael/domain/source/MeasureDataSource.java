package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Measures;


/**
 * Created by Roger Patiño on 11/08/2016.
 */

public interface MeasureDataSource {
    /**
     * TODO: Cargar tareas
     */
    interface LoadMeasureCallback {

        void onLoaded(List<Measures> measures);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetMeasureCallback {

        void onLoaded(List<Measures> measures);

        void onDataNotAvailable();

    }

    void getMeasure(@NonNull LoadMeasureCallback callback);

    void refleshMeasure(@NonNull LoadMeasureCallback callback);

    void getMeasure(@NonNull GetMeasureCallback callback);

    void saveMeasure(@NonNull Measures measures);

    void deleteAllMeasure();

}