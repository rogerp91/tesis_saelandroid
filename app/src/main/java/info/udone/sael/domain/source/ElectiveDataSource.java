package info.udone.sael.domain.source;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Electives;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public interface ElectiveDataSource {

    /**
     * TODO: Cargar Especialidad
     */
    interface LoadElectiveCallback {

        void onLoaded(List<Electives> electives);

        void onDataNotAvailable();

        void onErrorOcurred();

        void onErrorNotSolve();

        void onErrorNetwork();
    }

    interface GetElectivesCallback {

        void onElectivesLoaded(List<Electives> electives);

        void onDataNotAvailable();

    }

    void getElectives(String pensum, String code, @NonNull LoadElectiveCallback callback);

    void refleshElectives(String pensum, String code, @NonNull LoadElectiveCallback callback);

    void getElectives(String pensum, String code, @NonNull GetElectivesCallback callback);

    void saveElective(@NonNull Electives electives);

    void deleteAllElectives();

}