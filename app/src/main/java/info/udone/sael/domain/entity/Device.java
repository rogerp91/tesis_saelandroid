package info.udone.sael.domain.entity;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class Device {

    private String key_fcm;
    private String key_device;
    private String board;
    private String brand;
    private String cpu_abi;
    private String cpu_abi2;
    private String device;
    private String display_devices;
    private String hardware;
    private String host;
    private String manufacturer;
    private String model;
    private String product;
    private String serial;
    private String supported_abis;
    private String version_device;
    private String version_codes;


    public String getKey_fcm() {
        return key_fcm;
    }

    public void setKey_fcm(String key_fcm) {
        this.key_fcm = key_fcm;
    }

    public String getKey_device() {
        return key_device;
    }

    public void setKey_device(String key_device) {
        this.key_device = key_device;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCpu_abi() {
        return cpu_abi;
    }

    public void setCpu_abi(String cpu_abi) {
        this.cpu_abi = cpu_abi;
    }

    public String getCpu_abi2() {
        return cpu_abi2;
    }

    public void setCpu_abi2(String cpu_abi2) {
        this.cpu_abi2 = cpu_abi2;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDisplay_devices() {
        return display_devices;
    }

    public void setDisplay_devices(String display_devices) {
        this.display_devices = display_devices;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getSupported_abis() {
        return supported_abis;
    }

    public void setSupported_abis(String supported_abis) {
        this.supported_abis = supported_abis;
    }

    public String getVersion_cevice() {
        return version_device;
    }

    public void setVersion_cevice(String version_device) {
        this.version_device = version_device;
    }

    public String getVersion_codes() {
        return version_codes;
    }

    public void setVersion_codes(String version_codes) {
        this.version_codes = version_codes;
    }
}