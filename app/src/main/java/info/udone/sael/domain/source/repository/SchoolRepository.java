package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.School;
import info.udone.sael.domain.source.SchoolDataSource;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class SchoolRepository implements SchoolDataSource {

    private SchoolDataSource local;
    private SchoolDataSource remote;

    @Inject
    SchoolRepository(@Named("re_school_l") SchoolDataSource local, @Named("re_school_r") SchoolDataSource remote) {
        this.local = local;
        this.remote = remote;
    }

    @Override
    public void getSchool(@NonNull LoadSchoolCallback callback) {
        local.getSchool(new GetSchoolCallback() {
            @Override
            public void onLoaded(List<School> schools) {
                callback.onLoaded(schools);
            }

            @Override
            public void onDataNotAvailable() {
                remote.getSchool(new LoadSchoolCallback() {
                    @Override
                    public void onLoaded(List<School> schools) {
                        refreshLocalDataSource(schools);
                        callback.onLoaded(schools);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });
    }

    @Override
    public void refleshSchool(@NonNull LoadSchoolCallback callback) {
        remote.getSchool(new LoadSchoolCallback() {
            @Override
            public void onLoaded(List<School> schools) {
                refreshLocalDataSource(schools);
                callback.onLoaded(schools);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getSchool(@NonNull GetSchoolCallback callback) {
        local.getSchool(new GetSchoolCallback() {
            @Override
            public void onLoaded(List<School> schools) {
                callback.onLoaded(schools);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveSchool(@NonNull School school) {

    }

    @Override
    public void deleteAllSchool() {
        local.deleteAllSchool();
    }

    private void refreshLocalDataSource(List<School> schoolList) {
        local.deleteAllSchool();
        for (School school : schoolList) {
            local.saveSchool(school);
        }
    }
}