package info.udone.sael.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class Notification implements Parcelable {

    private int id;
    private String carrera;
    private String cedula;
    private String proridad;
    private String from_to;
    private String body;
    private String title;
    private String time;

    public String getProridad() {
        return proridad;
    }

    public void setProridad(String proridad) {
        this.proridad = proridad;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getFrom_to() {
        return from_to;
    }

    public void setFrom_to(String from_to) {
        this.from_to = from_to;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFroms() {
        return from_to;
    }

    public void setFroms(String froms) {
        this.from_to = froms;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.carrera);
        dest.writeString(this.cedula);
        dest.writeString(this.proridad);
        dest.writeString(this.from_to);
        dest.writeString(this.body);
        dest.writeString(this.title);
        dest.writeString(this.time);
    }

    public Notification() {
    }

    protected Notification(Parcel in) {
        this.id = in.readInt();
        this.carrera = in.readString();
        this.cedula = in.readString();
        this.proridad = in.readString();
        this.from_to = in.readString();
        this.body = in.readString();
        this.title = in.readString();
        this.time = in.readString();
    }

    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel source) {
            return new Notification(source);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}