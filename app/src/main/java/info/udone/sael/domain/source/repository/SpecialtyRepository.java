package info.udone.sael.domain.source.repository;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.entity.Specialtys;
import info.udone.sael.domain.source.SpecialtyDataSource;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class SpecialtyRepository implements SpecialtyDataSource {

    private SpecialtyDataSource localDataSource;
    private SpecialtyDataSource remoteDataSource;

    @Inject
    SpecialtyRepository(@Named("re_specialty_l") SpecialtyDataSource localDataSource, @Named("re_specialty_r") SpecialtyDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public void getSpecialtys(@NonNull LoadSpecialtyCallback callback) {
        localDataSource.getSpecialtys(new GetSpecialtyCallback() {
            @Override
            public void onSpecialtyLoaded(List<Specialtys> specialtyses) {
                callback.onLoaded(specialtyses);
            }

            @Override
            public void onDataNotAvailable() {
                remoteDataSource.getSpecialtys(new LoadSpecialtyCallback() {
                    @Override
                    public void onLoaded(List<Specialtys> specialtyses) {
                        refreshLocalDataSource(specialtyses);
                        localDataSource.getSpecialtys(new GetSpecialtyCallback() {
                            @Override
                            public void onSpecialtyLoaded(List<Specialtys> specialtyses) {
                                callback.onLoaded(specialtyses);
                            }

                            @Override
                            public void onDataNotAvailable() {

                            }
                        });
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }
        });
    }

    @Override
    public void refleshSpecialty(@NonNull LoadSpecialtyCallback callback) {
        remoteDataSource.getSpecialtys(new LoadSpecialtyCallback() {
            @Override
            public void onLoaded(List<Specialtys> specialtyses) {
                refreshLocalDataSource(specialtyses);
                localDataSource.getSpecialtys(new GetSpecialtyCallback() {
                    @Override
                    public void onSpecialtyLoaded(List<Specialtys> specialtyses) {
                        callback.onLoaded(specialtyses);
                    }

                    @Override
                    public void onDataNotAvailable() {

                    }
                });
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void getSpecialtys(@NonNull GetSpecialtyCallback callback) {
        localDataSource.getSpecialtys(new GetSpecialtyCallback() {
            @Override
            public void onSpecialtyLoaded(List<Specialtys> specialtyses) {
                callback.onSpecialtyLoaded(specialtyses);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void getSpecialty(@NonNull String code, @NonNull GetSpecialtyOnlyCallback callback) {
        localDataSource.getSpecialty(code, new GetSpecialtyOnlyCallback() {
            @Override
            public void onSpecialtyLoaded(Specialtys specialtys) {
                callback.onSpecialtyLoaded(specialtys);
            }

            @Override
            public void onDataNotAvailable() {
                remoteDataSource.getSpecialty(code, new GetSpecialtyOnlyCallback() {
                    @Override
                    public void onSpecialtyLoaded(Specialtys specialtys) {
                        localDataSource.saveSpecialty(specialtys);
                        callback.onSpecialtyLoaded(specialtys);
                    }

                    @Override
                    public void onDataNotAvailable() {
                        callback.onDataNotAvailable();
                    }

                    @Override
                    public void onErrorOcurred() {
                        callback.onErrorOcurred();
                    }

                    @Override
                    public void onErrorNotSolve() {
                        callback.onErrorNotSolve();
                    }

                    @Override
                    public void onErrorNetwork() {
                        callback.onErrorNetwork();
                    }
                });
            }

            @Override
            public void onErrorOcurred() {
                callback.onErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                callback.onErrorNotSolve();
            }

            @Override
            public void onErrorNetwork() {
                callback.onErrorNetwork();
            }
        });
    }

    @Override
    public void saveSpecialty(Specialtys specialtys) {

    }

    @Override
    public void deleteAllSpecialty() {
        localDataSource.deleteAllSpecialty();
    }

    private void refreshLocalDataSource(List<Specialtys> specialtyses) {
        localDataSource.deleteAllSpecialty();
        for (Specialtys specialtys : specialtyses) {
            localDataSource.saveSpecialty(specialtys);
        }
    }

}
