package info.udone.sael.domain.source.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Measures;
import info.udone.sael.domain.source.MeasureDataSource;
import info.udone.sael.measure.MeasuresContract.MeasureEntry;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class MeasureLocalDataSource implements MeasureDataSource {

    private SQLiteDBHelper sqliteDBHelper = null;
    private SQLiteDatabase db = null;

    Context context = Sael.getContext();

    @Inject
    MeasureLocalDataSource() {
        sqliteDBHelper = new SQLiteDBHelper(context);
    }

    @Override
    public void getMeasure(@NonNull LoadMeasureCallback callback) {
        open();
        List<Measures> measuresList = new ArrayList<>();
        String query = "SELECT * FROM " + MeasureEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Measures measures = new Measures();
                measures.setId(cursor.getInt(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_ID)));
                measures.setANO(cursor.getString(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_ANO)));
                measures.setCEDULA(cursor.getString(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_CEDULA)));
                measures.setPER(cursor.getString(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_PER)));
                measures.setMEDIDA(cursor.getString(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_MEDIDA)));
                measuresList.add(measures);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (measuresList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(measuresList);
        }
    }

    @Override
    public void refleshMeasure(@NonNull LoadMeasureCallback callback) {
        getMeasure(callback);
    }

    @Override
    public void getMeasure(@NonNull GetMeasureCallback callback) {
        open();
        List<Measures> measuresList = new ArrayList<>();
        String query = "SELECT * FROM " + MeasureEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                Measures measures = new Measures();
                measures.setId(cursor.getInt(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_ID)));
                measures.setANO(cursor.getString(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_ANO)));
                measures.setCEDULA(cursor.getString(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_CEDULA)));
                measures.setPER(cursor.getString(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_PER)));
                measures.setMEDIDA(cursor.getString(cursor.getColumnIndex(MeasureEntry.COLUMN_NAME_MEDIDA)));
                measuresList.add(measures);
            } while (cursor.moveToNext());
        }
        cursor.close();
        close();
        if (measuresList.isEmpty()) {
            callback.onDataNotAvailable();
        } else {
            callback.onLoaded(measuresList);
        }
    }

    @Override
    public void saveMeasure(@NonNull Measures measures) {
        checkNotNull(measures);
        open();
        ContentValues values = new ContentValues();
        values.put(MeasureEntry.COLUMN_NAME_ANO, measures.getANO());
        values.put(MeasureEntry.COLUMN_NAME_PER, measures.getPER());
        values.put(MeasureEntry.COLUMN_NAME_CEDULA, measures.getCEDULA());
        values.put(MeasureEntry.COLUMN_NAME_MEDIDA, measures.getMEDIDA());
        db.insert(MeasureEntry.TABLE_NAME, null, values);
        close();
    }

    @Override
    public void deleteAllMeasure() {
        open();
        db.delete(MeasureEntry.TABLE_NAME, null, null);
        close();
    }

    private void open() {
        db = sqliteDBHelper.getWritableDatabase();
    }

    private void close() {
        db.close();
        sqliteDBHelper.close();
    }

}