package info.udone.sael.domain.source.remote;

import java.util.List;

import info.udone.sael.BuildConfig;
import info.udone.sael.domain.entity.Appeals;
import info.udone.sael.domain.entity.Device;
import info.udone.sael.domain.entity.Doc;
import info.udone.sael.domain.entity.Electives;
import info.udone.sael.domain.entity.Login;
import info.udone.sael.domain.entity.LoginWithProfile;
import info.udone.sael.domain.entity.Materias;
import info.udone.sael.domain.entity.Measures;
import info.udone.sael.domain.entity.Note;
import info.udone.sael.domain.entity.Professors;
import info.udone.sael.domain.entity.Profile;
import info.udone.sael.domain.entity.PushNotification;
import info.udone.sael.domain.entity.Readmissions;
import info.udone.sael.domain.entity.School;
import info.udone.sael.domain.entity.Specialtys;
import info.udone.sael.domain.entity.Token;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public interface HttpRestClient {

    //21219756 y 4 22998372 y 5 20903635 y1
    String BASE_URL = BuildConfig.HOST + "/sael-api-restful/";
    String VERSION = "api/v1/";
    String PREFIX = "Bearer ";
    String USER_AGENT = "Sael-Mobile/";

    @Headers("Content-Type: application/json; charset=UTF-8")
    @POST(VERSION + "auth/login")
    Call<Token> postLogin(@Header("User-Agent") String agent, @Body Login login);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @POST(VERSION + "auth/login_with_profile")
    Call<LoginWithProfile> postLogin2(@Header("User-Agent") String agent, @Body Login login);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "auth/me")
    Call<Profile> getMe(@Header("Authorization") String token);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "appeals")
    Call<List<Appeals>> getAppeals(@Header("Authorization") String token);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "notes")
    Call<List<Note>> getNotes(@Header("Authorization") String token);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "specialtys")
    Call<List<Specialtys>> getSpecialtys(@Header("Authorization") String token);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "specialtys/{code}")
    Call<Specialtys> getSpecialty(@Header("Authorization") String token, @Path("code") String code);

    @Headers({
            "Accept: application/json; charset=utf-8",
            "Content-Type: application/json; charset=UTF-8"
    })
    @GET(VERSION + "specialty/{code}/pensum/{pensum}")
    Call<List<Materias>> getPensum(@Header("Authorization") String token, @Path("code") String code, @Path("pensum") String pensum);

    @Headers({
            "Accept: application/json; charset=utf-8",
            "Content-Type: application/json; charset=UTF-8"
    })
    @GET(VERSION + "specialty/{code}/pensum/{pensum}/electives")
    Call<List<Electives>> getElectives(@Header("Authorization") String token, @Path("code") String code, @Path("pensum") String pensum);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "schools")
    Call<List<School>> getSchools(@Header("Authorization") String token);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "measures")
    Call<List<Measures>> getMeasures(@Header("Authorization") String token);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "professors")
    Call<List<Professors>> getProfessors(@Header("Authorization") String token);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "readmissions")
    Call<List<Readmissions>> getReadmissions(@Header("Authorization") String token);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @POST(VERSION + "device")
    Call<Void> postDevice(@Header("Authorization") String token, @Body Device device);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @POST(VERSION + "doc")
    Call<ResponseBody> postConstancy(@Header("Authorization") String token, @Body Doc doc);

    @Headers("Content-Type: application/json; charset=UTF-8")
    @GET(VERSION + "example")
    Call<Doc> getDocCheck2(@Query("cedula") String cedula);

    @POST(VERSION + "notification")
    Call<Void> pushNotification(@Header("Authorization") String token, @Body PushNotification notification);

}