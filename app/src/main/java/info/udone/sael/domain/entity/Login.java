package info.udone.sael.domain.entity;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class Login {

    private String cedula;
    private String password;

    public Login(String cedula, String password) {
        this.cedula = cedula;
        this.password = password;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}