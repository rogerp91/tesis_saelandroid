package info.udone.sael.executor;

public interface MainThread {
	
	void post(final Runnable runnable);
	
}
