package info.udone.sael.executor;

public interface Executor {
	
    void run(final Interactor interactor);
    
}
