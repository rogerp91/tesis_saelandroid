package info.udone.sael.ui.adapte;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Materias;
import info.udone.sael.domain.entity.Requisito;
import info.udone.sael.util.ColorGenerator;
import info.udone.sael.util.SemesterFilter;
import info.udone.sael.util.TextDrawable;
import info.udone.sael.util.UseView;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class PensumAdapte extends AbstractExpandableItemAdapter<PensumAdapte.BaseViewHolder, PensumAdapte.ChildViewHolder> {

    private String TAG = PensumAdapte.class.getSimpleName();

    private List<Materias> materiases;
    private List<Materias> aux;
    private SemesterFilter filter;

    public PensumAdapte(List<Materias> materiases) {
        this.materiases = materiases;
        filter = new SemesterFilter(materiases, this);
        setHasStableIds(true); // this is required for expandable feature.
    }

    @Override
    public int getGroupCount() {
        return materiases.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return materiases.get(groupPosition).getREQUISITOS().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return materiases.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public BaseViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_group_materia, parent, false);
        return new MyGroupViewHolder(v);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_child_requisito, parent, false);
        return new MyChildViewHolder(v);
    }

    @Override
    public void onBindGroupViewHolder(BaseViewHolder holder, int groupPosition, int viewType) {
        Materias materias = materiases.get(groupPosition);
        holder.mCode.setText(materias.getCODIGO());
        holder.mCredito.setText(materias.getCREDITOS());
        holder.mName.setText(materias.getNOMBRE());
        holder.mSemestre.setText(materias.getSEMESTRE());
        holder.mTipo.setText(materias.getTIPO_MATERIA_NOMBRE());
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
        Requisito requisito = materiases.get(groupPosition).getREQUISITOS().get(childPosition);
        holder.mCodeRequisito.setText(String.format(Sael.getContext().getString(R.string.codigo2), requisito.getCODIGO()));
        holder.mNameRequisito.setText(requisito.getNOMBRE());
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(BaseViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return true;
    }

    static abstract class BaseViewHolder extends AbstractExpandableItemViewHolder {

        @BindView(R.id.name)
        TextView mName;

        @BindView(R.id.code)
        TextView mCode;

        @BindView(R.id.tipo)
        TextView mTipo;

        @BindView(R.id.semestre)
        TextView mSemestre;

        @BindView(R.id.credito)
        TextView mCredito;

        BaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static abstract class ChildViewHolder extends AbstractExpandableItemViewHolder {

        @BindView(R.id.name_requisito)
        TextView mNameRequisito;

        @BindView(R.id.code_requisito)
        TextView mCodeRequisito;

        ChildViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private static class MyGroupViewHolder extends BaseViewHolder {
        MyGroupViewHolder(View itemView) {
            super(itemView);
        }
    }

    private static class MyChildViewHolder extends ChildViewHolder {
        MyChildViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void animateTo(List<Materias> newMateriases) {
        applyAndAnimateRemovals(newMateriases);
        applyAndAnimateAdditions(newMateriases);
        applyAndAnimateMovedItems(newMateriases);
    }

    private void applyAndAnimateRemovals(List<Materias> newMateriases) {
        for (int i = newMateriases.size() - 1; i >= 0; i--) {
            final Materias model = newMateriases.get(i);
            if (!newMateriases.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Materias> newMateriases) {
        for (int i = 0, count = newMateriases.size(); i < count; i++) {
            final Materias model = newMateriases.get(i);
            if (!materiases.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Materias> newMateriases) {
        for (int toPosition = newMateriases.size() - 1; toPosition >= 0; toPosition--) {
            final Materias model = newMateriases.get(toPosition);
            final int fromPosition = materiases.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    private Materias removeItem(int position) {
        final Materias model = materiases.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    private void addItem(int position, Materias model) {
        materiases.add(position, model);
        notifyItemInserted(position);
    }

    private void moveItem(int fromPosition, int toPosition) {
        final Materias model = materiases.remove(fromPosition);
        materiases.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void setList(List<Materias> list) {
        this.materiases = list;
    }

    //call when you want to filter
    public void filterList(String text) {
        filter.filter(text);
    }

}