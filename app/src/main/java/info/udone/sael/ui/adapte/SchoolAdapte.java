package info.udone.sael.ui.adapte;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.School;
import info.udone.sael.util.ColorGenerator;
import info.udone.sael.util.Functions;
import info.udone.sael.util.TextDrawable;
import info.udone.sael.util.UseView;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class SchoolAdapte extends RecyclerView.Adapter<SchoolAdapte.ViewHolder> {
    private static String TAG = SchoolAdapte.class.getSimpleName();

    private List<School> schoolList;

    public SchoolAdapte(List<School> schoolList) {
        this.schoolList = schoolList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.school_itam_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        School school = schoolList.get(position);
        ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
        String desp = school.getDESCRIPCION();
        TextDrawable.IBuilder mDrawableBuilder = TextDrawable.builder().round();
        TextDrawable drawable = mDrawableBuilder.build(String.valueOf(desp.charAt(0)), mColorGenerator.getColor(desp));
        holder.mImgColor.setImageDrawable(drawable);
        String tx = desp.toLowerCase();
        holder.mTitle.setText(Functions.toTitleCase2(desp));
        holder.mCode.setText(String.format(Sael.getContext().getString(R.string.codigo3), school.getCODIGO()));
        holder.mStatus.setText(String.format("Estado: %s", school.getESTATUS()));
        UseView.setFadeAnimation(holder.mImgColor);
    }

    @Override
    public int getItemCount() {
        return schoolList.size();
    }

    @Override
    public long getItemId(int position) {
        return Integer.parseInt(schoolList.get(position).getCODIGO());
    }

    /**
     * TypedArray colors = getResources().obtainTypedArray(R.array.loading_colors);
     * int index = (int) (Math.random() * colors.length());
     * int color = colors.getColor(index, Color.BLACK);
     * textView.setTextColor(color);
     */

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView mTitle;
        @BindView(R.id.status)
        TextView mStatus;
        @BindView(R.id.code)
        TextView mCode;
        @BindView(R.id.imgColor)
        ImageView mImgColor;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}