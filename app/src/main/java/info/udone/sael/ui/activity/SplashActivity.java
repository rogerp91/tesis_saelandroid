package info.udone.sael.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.rogerp91.pref.SP;

import info.udone.sael.intro.IntroActivity;
import info.udone.sael.login.LoginActivity;

import static info.udone.sael.util.Constants.INTRODUTION;
import static info.udone.sael.util.Constants.SESSION;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (!SP.getBoolean(SESSION, false)) {
            if (!SP.getBoolean(INTRODUTION, false)) {
                startActivity(new Intent(this, IntroActivity.class));
            } else {
                SP.putBoolean(INTRODUTION, true);
                startActivity(new Intent(this, LoginActivity.class));
            }
        } else {
            startActivity(new Intent(this, MainActivity.class));
        }
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash);

    }
}
