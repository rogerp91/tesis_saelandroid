package info.udone.sael.ui.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import info.udone.sael.R;

/**
 * Created by AF on 29/12/2016.
 */

public class SecretSettingsFragment extends PreferenceFragment {

    protected static String TAG = SecretSettingsFragment.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.secret_setting);

    }

}
