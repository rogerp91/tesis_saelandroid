package info.udone.sael.ui.adapte;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.files.FilesFragment;
import info.udone.sael.util.Functions;


/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class FilesAdapte extends RecyclerView.Adapter<FilesAdapte.ViewHolder> {

    private static String TAG = FilesAdapte.class.getSimpleName();

    private List<File> files;
    private FilesFragment.SelectFileListener selectFile;
    private Context context;

    public FilesAdapte(Context context, List<File> files, FilesFragment.SelectFileListener selectFile) {
        this.files = files;
        this.selectFile = selectFile;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        File file = files.get(position);
        String name = file.getName().replace("CONS_", "");
        holder.mFileName.setText(name);
        holder.mFileZise.setText(Functions.bytesToHuman(file.length()));
        file.toURI();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
        Date lastModDate = new Date(file.lastModified());
        holder.mFileDate.setText(dateFormat.format(lastModDate));

        holder.mOverflow.setOnClickListener(view -> showPopupMenu(holder.mOverflow, file));

    }


    private void showPopupMenu(View view, File file) {
        Context wrapper = new ContextThemeWrapper(context, R.style.PopupMenu);
        PopupMenu popup = new PopupMenu(wrapper, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_file, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuItemClickListener(file));
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    private class MenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        File file;

        public MenuItemClickListener(File file) {
            this.file = file;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_view:
                    selectFile.selectFile(file);
                    return true;
                case R.id.action_shared:
                    selectFile.shareFile(Uri.fromFile(file));
                    return true;
                default:
            }
            return false;
        }
    }


    @Override
    public int getItemCount() {
        return files.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.file_name)
        TextView mFileName;
        @BindView(R.id.file_size)
        TextView mFileZise;
        @BindView(R.id.file_date)
        TextView mFileDate;

        @BindView(R.id.overflow)
        ImageView mOverflow;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}