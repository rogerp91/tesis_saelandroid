package info.udone.sael.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import dagger.ObjectGraph;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.di.FragmentModule;
import info.udone.sael.ui.activity.BaseActivity;
import info.udone.sael.util.UseView;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public abstract class BaseFragment extends Fragment {


    private ObjectGraph activityGraph;

    public ActionBar ab;
    protected BaseActivity activity;

    abstract public int getLayoutView();

    abstract public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) getActivity();
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        ab = appCompatActivity.getSupportActionBar();
        injectDependencies();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getLayoutView(), container, false);
        ButterKnife.bind(this, view);
        onViewReady(inflater, container, savedInstanceState, view);
        return view;
    }


    private void injectDependencies() {
        Sael app = (Sael) getActivity().getApplication();
        List<Object> activityScopeModules = new ArrayList<>();
        activityScopeModules.add(new FragmentModule(getActivity()));
        activityGraph = app.buildGraphWithAditionalModules(activityScopeModules);
        inject(this);
    }

    private void inject(Object entityToGetInjected) {
        activityGraph.inject(entityToGetInjected);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activityGraph = null;
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (SP.getBoolean(Constants.ACTION_CHANGE_PASSWORD, false)) {
//            new MaterialDialog.Builder(getActivity())
//                    .title("Cambio de contraseña!")
//                    .content("Se ha detectado un cambio de contraseña desde la web")
//                    .positiveText("Aceptar")
//                    .onPositive((dialog, which) -> {
//                        SP.clear();
//                        SP.putBoolean(Constants.INTRODUTION, true);
//                        getActivity().finish();
//                        startActivity(new Intent(getActivity(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                    })
//                    .show();
//        }
    }

    protected void showOrNotView(boolean active, View view) {
        UseView.showOrHideView(active, view, getActivity().getApplicationContext(), isAdded());
    }

    protected void showOrNotViewMultiple(boolean active, View view1, View view2) {
        UseView.showOrHideViewMultiple(active, view1, view2, getActivity().getApplicationContext(), isAdded());
    }

    protected void shoeMessageError(String s) {
        if (!isAdded()) {
            return;
        }
        Snackbar snackbar;
        snackbar = Snackbar.make(getView(), s, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.alert));
        snackbar.show();
    }

    protected void shoeMessage(String s, int color) {
        if (!isAdded()) {
            return;
        }
        Snackbar snackbar;
        snackbar = Snackbar.make(getView(), s, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(getContext(), color));
        snackbar.show();
    }


}
