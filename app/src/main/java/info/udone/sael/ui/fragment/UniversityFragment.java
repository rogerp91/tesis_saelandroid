package info.udone.sael.ui.fragment;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ScrollView;

import butterknife.BindView;
import butterknife.OnClick;
import info.udone.sael.R;
import info.udone.sael.ui.activity.ContainerActivity;
import info.udone.sael.util.ActivityWithOptions;
import info.udone.sael.util.Devices;
import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;

import static info.udone.sael.util.Constants.EXTRA_TRANSITION;
import static info.udone.sael.util.Constants.ID_FRAGMENT;
import static info.udone.sael.util.Constants.TRANSITION_FADE_SLOW;


public class UniversityFragment extends BaseFragment {

    public static UniversityFragment newInstance() {
        return new UniversityFragment();
    }

    @BindView(R.id.container)
    ScrollView nestedScrollView;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_university;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        PermissionGen.with(getActivity()).addRequestCode(99).permissions(Manifest.permission.WRITE_EXTERNAL_STORAGE).request();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
        animation.setDuration(300);
        nestedScrollView.setAnimation(animation);
        nestedScrollView.animate();
        animation.start();
    }

    @OnClick(R.id.btn_appeals)
    void clickAppeals() {
        goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 1));
    }

    @OnClick(R.id.btn_school)
    void clickSchool() {
        goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 2));
    }

    @OnClick(R.id.btn_speciality)
    void clickSpeciality() {
        goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 3));
    }

    @OnClick(R.id.btn_measure)
    void clickMeasure() {
        goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 4));
    }

    @OnClick(R.id.btn_note)
    void clickNote() {
        goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 5));
    }

    @OnClick(R.id.btn_record)
    void clickRecord() {
        goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 6));
//        if (Devices.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//
//        } else {
//            Snackbar snackbar;
//            snackbar = Snackbar.make(getView(), "Se require permiso de almacenamiento", Snackbar.LENGTH_SHORT);
//            View snackBarView = snackbar.getView();
//            snackBarView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.info));
//            snackbar.show();
//        }
    }

    @OnClick(R.id.btn_readmissions)
    void clickReadmissions() {
        goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 7));
    }

    @OnClick(R.id.btn_download)
    void clickDownload() {
        if (Devices.isMarshmallow()) {
            if (Devices.hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 9));
            } else {
                Snackbar snackbar;
                snackbar = Snackbar.make(getView(), "Se require permiso de almacenamiento", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.info));
                snackbar.show();
            }
        } else {
            goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 9));
        }
    }

    private void goToContainer(Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(EXTRA_TRANSITION, TRANSITION_FADE_SLOW);
            ActivityWithOptions.startActivityWithOptions(intent, getActivity());
        } else {
            startActivity(intent);
            getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @PermissionFail(requestCode = 99)
    public void failPermission() {
        Snackbar snack = Snackbar.make(getView(), "Se necesitan permisos especiales!", Snackbar.LENGTH_INDEFINITE);
        snack.setAction("Configuración", v -> {
            final Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + getActivity().getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(i);
        });
        snack.show();
    }

}