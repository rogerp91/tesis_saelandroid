package info.udone.sael.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.ui.fragment.SecretSettingsFragment;
import info.udone.sael.util.Devices;

public class SecretSettingActivity extends BaseActivity {

    private static String TAG = SecretSettingActivity.class.getSimpleName();

    @BindView(R.id.container)
    LinearLayout layout;

    ActionBar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_secret);
        ButterKnife.bind(this);
        try {
            if (Devices.isLollipop()) setTransition(getIntent());
        } catch (NullPointerException e) {
//            ReportError.send(e);
        }

//        setSupportActionBar(toolbar);
//        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        bar = getSupportActionBar();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(android.R.id.content, new SecretSettingsFragment()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public View getLayout() {
        return layout;
    }

}