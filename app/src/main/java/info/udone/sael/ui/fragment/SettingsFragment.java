package info.udone.sael.ui.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.ObjectGraph;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.di.FragmentModule;
import info.udone.sael.domain.source.DeleteContract;
import info.udone.sael.ui.activity.SettingsActivity;

/**
 * Created by AndrewX on 22/11/2016.
 */

public class SettingsFragment extends PreferenceFragment {

    protected static String TAG = SettingsFragment.class.getSimpleName();
    private ObjectGraph activityGraph;

    private Preference prefVersion;
    private Preference prefDevelop;
    private Preference prefDevice;

    private Preference prefCompile;
    private int click = 0;
    private EditText secret;

    @Inject
    DeleteContract.Presenter presenter;

    private MaterialDialog.Builder builder = null;
    private MaterialDialog dialog = null;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //Disable scrollbar
        ListView listView = (ListView) view.findViewById(android.R.id.list);
        if (listView != null) listView.setVerticalScrollBarEnabled(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.setting);
        injectDependencies();
        prefDevelop = findPreference("key_develop");
        prefDevice = findPreference("key_device");
        prefVersion = findPreference("key_version");
//        prefCompile = findPreference("key_compile");
        init();
    }

    private void init() {
        String appVersion;
        String deviceModel = "";

        try {
            deviceModel = Build.MANUFACTURER + " - " + Build.MODEL;
            PackageInfo pinfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            appVersion = pinfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
//            ReportError.send(e);
            appVersion = "App version unknown";
        }
        if (appVersion.trim().length() == 0) {
            appVersion = "App version unknown";
        }
        if (deviceModel.trim().length() == 0) {
            deviceModel = "Android unknown";
        }

//        String compile;
//        try {
//            compile = BuildConfig.BUILD_TYPE;
//        } catch (Exception e) {
//            compile = "Compile unknown";
//        }
//
//        if (compile.trim().length() == 0) {
//            compile = "Compile unknown";
//        }

//        prefCompile.setSummary(compile);

        prefVersion.setSummary(appVersion);
        prefDevelop.setSummary("UDONE");
        prefDevice.setSummary(deviceModel);
//        findPreference("key_lic").setOnPreferenceClickListener(preference -> {
//
//            return false;
//        });
        findPreference("key_close").setOnPreferenceClickListener(preference -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
            builder.setTitle(R.string.log_out_title);
            builder.setIcon(ContextCompat.getDrawable(getActivity(), R.mipmap.ic_launcher));
            builder.setMessage(R.string.log_out_content2);
            builder.setNegativeButton("Cancelar", (dialogInterface, i) -> {
                dialogInterface.dismiss();
            }).setPositiveButton("Aceptar", (dialogInterface, i) -> {
                SettingsActivity appCompatActivity = (SettingsActivity) getActivity();
                appCompatActivity.closeApp();
                Bundle params = new Bundle();
                params.putBoolean("click_close_app", true);

            });
            builder.show();
            return false;
        });

        findPreference("key_clean").setOnPreferenceClickListener(preference -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
            builder.setTitle(R.string.log_out_content_clean_all);
            builder.setIcon(ContextCompat.getDrawable(getActivity(), R.mipmap.ic_launcher));
            builder.setMessage(R.string.log_out_content_clean);
            builder.setNegativeButton("Cancelar", (dialogInterface, i) -> {
                dialogInterface.dismiss();
            }).setPositiveButton("Aceptar", (dialogInterface, i) -> {
                presenter.deleteNotification();
                Bundle params = new Bundle();
                params.putString("click_clean_notification", "click");
            });
            builder.show();
            return false;
        });

//        findPreference("key_clean_all").setOnPreferenceClickListener(preference -> {
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
//            builder.setTitle(R.string.pref_preference_clean_all);
//            builder.setIcon(ContextCompat.getDrawable(getActivity(), R.mipmap.ic_launcher));
//            builder.setMessage("Esta seguro que quiero borrar los datos del dispositivo?");
//            builder.setNegativeButton("Cancelar", (dialogInterface, i) -> {
//                dialogInterface.dismiss();
//            }).setPositiveButton("Aceptar", (dialogInterface, i) -> {
//                MaterialDialog.Builder builder2 = new MaterialDialog.Builder(getActivity()).title("Sael").content("Borrando los datos del dispositivo, espere por favor...").progress(true, 0).progressIndeterminateStyle(false);
//                MaterialDialog dialog2 = builder2.build();
//                dialog2.show();
//                getActivity().runOnUiThread(() -> presenter.deleteAll());
//                dialog2.dismiss();
//                Bundle params = new Bundle();
//                params.putString("click_clean_all", "click");
//                mFirebaseAnalytics.logEvent("click_clean_all", params);
//            });
//            builder.show();
//            return false;
//        });

        findPreference("key_roger").setOnPreferenceClickListener(preference -> {
            sendEmail("rogerp.009@gmail.com");
            return false;
        });

        findPreference("key_mirelys").setOnPreferenceClickListener(preference -> {
            sendEmail("mirelyssadg.93@gmail.com");
            return false;
        });

//        findPreference("key_elisaul").setOnPreferenceClickListener(preference -> {
//            sendEmail("elisauldavid12@gmail.com");
//            return false;
//        });
//        prefCompile.setOnPreferenceClickListener(preference -> {
//            click++;
//            if (click == 9) {
//                Toast.makeText(getActivity(), "Little to be developer", Toast.LENGTH_SHORT).show();
//            }
//            if (click == 17) {
//                prefCompile.setSelectable(false);
//                prefCompile.setEnabled(false);
//                builder = new MaterialDialog.Builder(getActivity())
//                        .title("Developer mode")
//                        .customView(R.layout.dialog_secret, true)
//                        .negativeText("Cancel")
//                        .positiveText("Done").onPositive((dialog1, which) -> {
//                            if (secret.getText().toString().equals("soportesael2016") || secret.getText().toString().equals("saelmobile2016")) {
//                                Bundle params = new Bundle();
//                                params.putString("click_secret_setting", "click");
//                                mFirebaseAnalytics.logEvent("click_secret_setting", params);
//                                startActivity(new Intent(getActivity(), SecretSettingActivity.class));
//                            } else {
//                                Bundle params = new Bundle();
//                                params.putString("secret_setting_error_password", "click");
//                                mFirebaseAnalytics.logEvent("secret_setting_error_password", params);
//                                Toast.makeText(getActivity(), "Incorrect password", Toast.LENGTH_SHORT).show();
//                                dialog.dismiss();
//                            }
//                        }).onNegative((dialog12, which) -> {
//                            dialog.dismiss();
//                        });
//
//                dialog = builder.build();
//                View view = dialog.getCustomView();
//                secret = (EditText) view.findViewById(R.id.secret);
//                dialog.show();
//            }
//            return false;
//        });

    }

    private void injectDependencies() {
        Sael app = (Sael) getActivity().getApplication();
        List<Object> activityScopeModules = new ArrayList<>();
        activityScopeModules.add(new FragmentModule(getActivity()));
        activityGraph = app.buildGraphWithAditionalModules(activityScopeModules);
        inject(this);
    }

    private void inject(Object entityToGetInjected) {
        activityGraph.inject(entityToGetInjected);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activityGraph = null;
        click = 0;
    }

    @Override
    public void onPause() {
        super.onPause();
        click = 0;
    }

    private void sendEmail(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto: " + email));
        startActivity(Intent.createChooser(emailIntent, "Send feedback"));
    }
}