package info.udone.sael.ui.adapte;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Electives;
import info.udone.sael.domain.entity.Materias;
import info.udone.sael.domain.entity.Requisito;
import info.udone.sael.util.ColorGenerator;
import info.udone.sael.util.ElectiveSemesterFilter;
import info.udone.sael.util.SemesterFilter;
import info.udone.sael.util.TextDrawable;
import info.udone.sael.util.UseView;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class ElectiveAdapte extends AbstractExpandableItemAdapter<ElectiveAdapte.BaseViewHolder, ElectiveAdapte.ChildViewHolder> {

    private List<Electives> electivesList;
    private int lastPosition = -1;
    private ElectiveSemesterFilter filter;

    public ElectiveAdapte(List<Electives> electivesList) {
        this.electivesList = electivesList;
        filter = new ElectiveSemesterFilter(electivesList, this);
        setHasStableIds(true); // this is required for expandable feature.
    }

    @Override
    public int getGroupCount() {
        return electivesList.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return electivesList.get(groupPosition).getREQUISITOS().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return electivesList.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public BaseViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_group_materia, parent, false);
        return new MyGroupViewHolder(v);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_child_requisito, parent, false);
        return new MyChildViewHolder(v);
    }

    @Override
    public void onBindGroupViewHolder(BaseViewHolder holder, int groupPosition, int viewType) {
        Electives elective = electivesList.get(groupPosition);
        holder.mCode.setText(elective.getCODIGO());
        holder.mCredito.setText(elective.getCREDITOS());
        holder.mName.setText(elective.getNOMBRE());
        holder.mSemestre.setText(elective.getSEMESTRE());
        holder.mTipo.setText(elective.getTIPO_MATERIA_NOMBRE());
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
        Requisito requisito = electivesList.get(groupPosition).getREQUISITOS().get(childPosition);
        holder.mCodeRequisito.setText(String.format(Sael.getContext().getString(R.string.codigo), requisito.getCODIGO()));
        holder.mNameRequisito.setText(requisito.getNOMBRE());
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(BaseViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return true;
    }

    static abstract class BaseViewHolder extends AbstractExpandableItemViewHolder {

        @BindView(R.id.name)
        TextView mName;
        @BindView(R.id.code)
        TextView mCode;
        @BindView(R.id.tipo)
        TextView mTipo;
        @BindView(R.id.semestre)
        TextView mSemestre;
        @BindView(R.id.credito)
        TextView mCredito;

        BaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static abstract class ChildViewHolder extends AbstractExpandableItemViewHolder {

        @BindView(R.id.name_requisito)
        TextView mNameRequisito;

        @BindView(R.id.code_requisito)
        TextView mCodeRequisito;

        ChildViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private static class MyGroupViewHolder extends BaseViewHolder {
        MyGroupViewHolder(View itemView) {
            super(itemView);
        }
    }

    private static class MyChildViewHolder extends ChildViewHolder {
        MyChildViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void animateTo(List<Electives> newElectives) {
        applyAndAnimateRemovals(newElectives);
        applyAndAnimateAdditions(newElectives);
        applyAndAnimateMovedItems(newElectives);
    }

    private void applyAndAnimateRemovals(List<Electives> newElectives) {
        for (int i = newElectives.size() - 1; i >= 0; i--) {
            final Electives model = newElectives.get(i);
            if (!newElectives.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Electives> newElectives) {
        for (int i = 0, count = newElectives.size(); i < count; i++) {
            final Electives model = newElectives.get(i);
            if (!electivesList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Electives> newElectives) {
        for (int toPosition = newElectives.size() - 1; toPosition >= 0; toPosition--) {
            final Electives model = newElectives.get(toPosition);
            final int fromPosition = electivesList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    private Electives removeItem(int position) {
        final Electives model = electivesList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    private void addItem(int position, Electives model) {
        electivesList.add(position, model);
        notifyItemInserted(position);
    }

    private void moveItem(int fromPosition, int toPosition) {
        final Electives model = electivesList.remove(fromPosition);
        electivesList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void setList(List<Electives> list) {
        this.electivesList = list;
    }

    //call when you want to filter
    public void filterList(String text) {
        filter.filter(text);
    }

}