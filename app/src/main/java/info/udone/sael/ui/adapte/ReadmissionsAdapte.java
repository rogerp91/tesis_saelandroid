package info.udone.sael.ui.adapte;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Readmissions;
import info.udone.sael.util.ColorGenerator;
import info.udone.sael.util.UseView;

/**
 * Created by Roger Patiño on 14/08/2016.
 */

public class ReadmissionsAdapte extends RecyclerView.Adapter<ReadmissionsAdapte.ViewHolder> {

    List<Readmissions> readmissionsList;

    public ReadmissionsAdapte(List<Readmissions> readmissionsList) {
        this.readmissionsList = readmissionsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.readmissions_itam_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Readmissions readmissions = readmissionsList.get(position);
        ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
        holder.mLineColor.setBackgroundColor(mColorGenerator.getRandomColor());
        holder.mPer.setText(Sael.getContext().getString(R.string.periodo3) + readmissions.getANO() + " - " + readmissions.getPER());
        holder.mCreate.setText(String.format("Creado: %s", readmissions.getCREADO().split(" ")[0]));
        holder.mAprob.setText(String.format("Aprobado: %s", readmissions.getAPROBADO()));
        UseView.setFadeAnimation(holder.mLineColor);
    }

    @Override
    public int getItemCount() {
        return readmissionsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.per)
        TextView mPer;
        @BindView(R.id.create)
        TextView mCreate;
        @BindView(R.id.aprob)
        TextView mAprob;
        @BindView(R.id.line_color)
        LinearLayout mLineColor;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}