package info.udone.sael.ui.adapte;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.document.DocumentFragment;
import info.udone.sael.domain.entity.Document;


/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class DocumentAdapte extends RecyclerView.Adapter<DocumentAdapte.ViewHolder> {

    private static String TAG = DocumentAdapte.class.getSimpleName();

    private List<Document> documents;
    DocumentFragment.ClickTypeDoc clickTypeDoc;

    public DocumentAdapte(List<Document> documents, DocumentFragment.ClickTypeDoc clickTypeDoc) {
        this.documents = documents;
        this.clickTypeDoc = clickTypeDoc;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_document, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Document document = documents.get(position);
        holder.mIcon.setImageDrawable(document.getIcon());
        holder.mTitle.setText(document.getTitle());
        holder.mSescription.setText(document.getSub_title());
        holder.mCard.setOnClickListener(view -> clickTypeDoc.onClickDoc(document.getId()));
    }

    @Override
    public int getItemCount() {
        return documents.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon)
        ImageView mIcon;
        @BindView(R.id.title)
        TextView mTitle;
        @BindView(R.id.card)
        CardView mCard;
        @BindView(R.id.short_description)
        TextView mSescription;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}