package info.udone.sael.ui.adapte;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Note;
import info.udone.sael.util.ColorGenerator;


/**
 * Created by Roger Patiño on 02/08/2016.
 */
public class NoteAdapte extends RecyclerView.Adapter<NoteAdapte.ViewHolder> {
    private static String TAG = NoteAdapte.class.getSimpleName();

    private List<Note> notes;

    public NoteAdapte(List<Note> notes) {
        this.notes = notes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_itam_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Note note = notes.get(position);
        holder.mNote.setText(note.getULT_NOTA());
        holder.mName.setText(note.getNOMBRE());
        holder.mAno.setText(Sael.getContext().getString(R.string.periodo2) + note.getULT_ANO() + " - " + note.getULT_PER());
        holder.mCode.setText(String.format("Codigo: %s", note.getCODIGO()));
        holder.mInscrita.setText(String.format("Inscrita: %s", note.getINSCRITA()));
        holder.mReprobada.setText(String.format("Reprobada: %s", note.getREPROBADA()));
        holder.mRetirada.setText(String.format("Retirada: %s", note.getRETIRADA()));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    @Override
    public long getItemId(int position) {
        return notes.get(position).getId();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView mName;
        @BindView(R.id.ano)
        TextView mAno;
        @BindView(R.id.code)
        TextView mCode;
        @BindView(R.id.inscrita)
        TextView mInscrita;
        @BindView(R.id.reprobada)
        TextView mReprobada;
        @BindView(R.id.retirada)
        TextView mRetirada;
        @BindView(R.id.note)
        TextView mNote;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void animateTo(List<Note> newNote) {
        applyAndAnimateRemovals(newNote);
        applyAndAnimateAdditions(newNote);
        applyAndAnimateMovedItems(newNote);
    }

    private void applyAndAnimateRemovals(List<Note> newNote) {
        for (int i = newNote.size() - 1; i >= 0; i--) {
            final Note model = newNote.get(i);
            if (!newNote.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Note> newNote) {
        for (int i = 0, count = newNote.size(); i < count; i++) {
            final Note model = newNote.get(i);
            if (!notes.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Note> newNote) {
        for (int toPosition = newNote.size() - 1; toPosition >= 0; toPosition--) {
            final Note model = newNote.get(toPosition);
            final int fromPosition = notes.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    private Note removeItem(int position) {
        final Note model = notes.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    private void addItem(int position, Note model) {
        notes.add(position, model);
        notifyItemInserted(position);
    }

    private void moveItem(int fromPosition, int toPosition) {
        final Note model = notes.remove(fromPosition);
        notes.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
}