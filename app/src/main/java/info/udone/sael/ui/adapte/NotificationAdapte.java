package info.udone.sael.ui.adapte;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Notification;

/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class NotificationAdapte extends RecyclerView.Adapter<NotificationAdapte.ViewHolder> {

    private static String TAG = NotificationAdapte.class.getSimpleName();

    private List<Notification> notifications;

    public NotificationAdapte(List<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Notification notification = notifications.get(position);
        holder.mMame.setText(notification.getTitle());
        holder.mMessage.setText(notification.getBody());
        holder.mFromAndTimestamp.setText(notification.getFroms() + ": " + notification.getTime());
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    @Override
    public long getItemId(int position) {
        return notifications.get(position).getId();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView mMame;
        @BindView(R.id.message)
        TextView mMessage;
        @BindView(R.id.from_timestamp)
        TextView mFromAndTimestamp;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void deleteAll() {
        try {
            notifications.clear();
            notifyDataSetChanged();
        } catch (NullPointerException e) {
            Log.d(TAG, "deleteAll: " + e.getMessage());
        }
    }

}