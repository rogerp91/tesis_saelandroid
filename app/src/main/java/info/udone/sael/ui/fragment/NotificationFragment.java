package info.udone.sael.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.roger91.mlprogress.MlProgress;
import com.github.rogerp91.pref.SP;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.notification.NotificationContract;
import info.udone.sael.ui.activity.ContainerActivity;
import info.udone.sael.ui.adapte.NotificationAdapte;
import info.udone.sael.util.ActivityWithOptions;

import static info.udone.sael.util.Constants.ADMIN;
import static info.udone.sael.util.Constants.EXTRA_TRANSITION;
import static info.udone.sael.util.Constants.ID_FRAGMENT;
import static info.udone.sael.util.Constants.TRANSITION_FADE_SLOW;

public class NotificationFragment extends BaseFragment implements NotificationContract.View {

    private static String TAG = NotificationFragment.class.getSimpleName();

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @BindView(R.id.container_fragment)
    FrameLayout mFragment;

    @BindView(R.id.progress)
    MlProgress mProgressView;
    @BindView(R.id.layout_message)
    LinearLayout layout_message;
    @BindView(R.id.text_message)
    TextView text_message;
    @BindView(R.id.recycler_list)
    RecyclerView recycler;
//    @BindView(R.id.swipe_refresh)
//    SwipeRefreshLayout swipeRefreshLayout;

    NotificationAdapte adapte;

    @BindView(R.id.fab)
    FloatingActionButton buttonSend;

    @Inject
    NotificationContract.Presenter presenter;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_notification;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        presenter.setView(this);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
        animation.setDuration(300);
        mFragment.setAnimation(animation);
        mFragment.animate();
        animation.start();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler.setHasFixedSize(true);
        buttonSend.setVisibility(View.GONE);
        if (SP.getBoolean(ADMIN)) {
            buttonSend.setVisibility(View.VISIBLE);
        } else {
            buttonSend.setVisibility(View.GONE);
        }
        LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);
//        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary),
//                ContextCompat.getColor(getActivity(), R.color.colorAccent),
//                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
//        );
//        swipeRefreshLayout.setOnRefreshListener(() -> presenter.loadModels(false));
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("UPDATE");
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
        presenter.start();
        try {
            adapte.notifyDataSetChanged();
        } catch (NullPointerException e) {
//            ReportError.send(e);
        }
    }

    @Override
    public void updateView() {
        try {
            List<Notification> notifications = new ArrayList<>();
            adapte = new NotificationAdapte(notifications);
            adapte.setHasStableIds(true);
            recycler.setAdapter(adapte);
            adapte.notifyDataSetChanged();
        } catch (NullPointerException e) {
//            ReportError.send(e);
        }
    }

    @Override
    public void showProgress(final boolean active) {
        showOrNotView(active, mProgressView);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
//        if (getView() == null) {
//            return;
//        }
//        final SwipeRefreshLayout srl = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
//        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showModels(List<Notification> objects) {
        adapte = new NotificationAdapte(objects);
        adapte.setHasStableIds(true);
        recycler.setAdapter(adapte);
        adapte.notifyDataSetChanged();
    }

    @Override
    public void showNoModels(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_data_available_notification));
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @OnClick(R.id.text_try_again)
    void onClickTryAgain() {
        presenter.setView(this);
        presenter.start();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("UPDATE")) {
                layout_message.setVisibility(View.GONE);
                presenter.loadModels(false);
            } else {
                if (intent.getAction().equals("UPDATE_DISPLAY")) {
                    presenter.start();
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @OnClick(R.id.fab)
    void clickSendNotification() {
        goToContainer(new Intent(getActivity(), ContainerActivity.class).putExtra(ID_FRAGMENT, 8));
    }

    private void goToContainer(Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(EXTRA_TRANSITION, TRANSITION_FADE_SLOW);
            ActivityWithOptions.startActivityWithOptions(intent, getActivity());
        } else {
            startActivity(intent);
            getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
}