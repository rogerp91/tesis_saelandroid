package info.udone.sael.ui.adapte;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Specialtys;
import info.udone.sael.specialty.SpecialtysFragment;
import info.udone.sael.util.ColorGenerator;
import info.udone.sael.util.UseView;


/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class SpecialtyAdapte extends RecyclerView.Adapter<SpecialtyAdapte.ViewHolder> {

    private List<Specialtys> specialtyses;
    private SpecialtysFragment.SpecialityItemListener specialityItemListener;


    public SpecialtyAdapte(List<Specialtys> specialtyses, SpecialtysFragment.SpecialityItemListener specialityItemListener) {
        this.specialtyses = specialtyses;
        this.specialityItemListener = specialityItemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.specialtys_itam_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
        int color = mColorGenerator.getRandomColor();
        holder.mLineColor.setBackgroundColor(color);
        holder.btnPensum.setTextColor(color);
        holder.btnElective.setTextColor(color);
        final Specialtys specialtys = specialtyses.get(position);

        holder.mPensum.setText(String.format("Pensum: %s", specialtys.getPENSUM()));
//        holder.mCode.setText(String.format(Sael.getContext().getString(R.string.codigo4), specialtys.getCODIGO()));

        holder.mCode.setText("Código de especialiad: " + specialtys.getCODIGO());

        holder.mTitle.setText(specialtys.getTITULO());

        holder.btnPensum.setOnClickListener(view -> specialityItemListener.onPensum(specialtys, position));
        holder.btnElective.setOnClickListener(view -> specialityItemListener.onElective(specialtys, position));
//        UseView.setFadeAnimation(holder.mLineColor);
    }

    @Override
    public int getItemCount() {
        return specialtyses.size();
    }

    @Override
    public long getItemId(int position) {
        return specialtyses.get(position).getId();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pensum)
        TextView mPensum;
        @BindView(R.id.title)
        TextView mTitle;
        @BindView(R.id.code)
        TextView mCode;
        @BindView(R.id.btnPensum)
        AppCompatButton btnPensum;
        @BindView(R.id.btnelective)
        AppCompatButton btnElective;
        @BindView(R.id.layout_header)
        LinearLayout mLineColor;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void animateTo(List<Specialtys> newSpecialtyses) {
        applyAndAnimateRemovals(newSpecialtyses);
        applyAndAnimateAdditions(newSpecialtyses);
        applyAndAnimateMovedItems(newSpecialtyses);
    }

    private void applyAndAnimateRemovals(List<Specialtys> newSpecialtyses) {
        for (int i = newSpecialtyses.size() - 1; i >= 0; i--) {
            final Specialtys model = newSpecialtyses.get(i);
            if (!newSpecialtyses.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Specialtys> newSpecialtyses) {
        for (int i = 0, count = newSpecialtyses.size(); i < count; i++) {
            final Specialtys model = newSpecialtyses.get(i);
            if (!specialtyses.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Specialtys> newSpecialtyses) {
        for (int toPosition = newSpecialtyses.size() - 1; toPosition >= 0; toPosition--) {
            final Specialtys model = newSpecialtyses.get(toPosition);
            final int fromPosition = specialtyses.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    private Specialtys removeItem(int position) {
        final Specialtys model = specialtyses.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    private void addItem(int position, Specialtys model) {
        specialtyses.add(position, model);
        notifyItemInserted(position);
    }

    private void moveItem(int fromPosition, int toPosition) {
        final Specialtys model = specialtyses.remove(fromPosition);
        specialtyses.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
}