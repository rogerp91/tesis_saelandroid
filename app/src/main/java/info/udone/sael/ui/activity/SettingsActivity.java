package info.udone.sael.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.domain.source.DeleteContract;
import info.udone.sael.ui.fragment.SettingsFragment;
import info.udone.sael.util.Devices;
import info.udone.sael.util.ReportError;

public class SettingsActivity extends BaseActivity {

    private static String TAG = SettingsActivity.class.getSimpleName();

    @BindView(R.id.container)
    LinearLayout layout;

    ActionBar bar;

    @Inject
    DeleteContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        try {
            if (Devices.isLollipop()) setTransition(getIntent());
        } catch (NullPointerException e) {
//            ReportError.send(e);
        }

//        setSupportActionBar(toolbar);
//        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        bar = getSupportActionBar();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(android.R.id.content, new SettingsFragment()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public View getLayout() {
        return layout;
    }

    public void closeApp() {
        setResult(999);
        finish();

    }

}
