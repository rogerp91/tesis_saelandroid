package info.udone.sael.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.transition.Transition;
import android.transition.TransitionInflater;

import com.github.rogerp91.pref.SP;

import java.util.ArrayList;
import java.util.List;

import dagger.ObjectGraph;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.di.ActivityModule;
import info.udone.sael.login.LoginActivity;
import info.udone.sael.util.Constants;

import static info.udone.sael.util.Constants.EXTRA_TRANSITION;
import static info.udone.sael.util.Constants.TRANSITION_FADE_SLOW;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ObjectGraph activityGraph;

    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
    }

    private void injectDependencies() {
        Sael sael = (Sael) getApplication();
        List<Object> activityScopeModules = new ArrayList<>();
        activityScopeModules.add(new ActivityModule(this));
        activityGraph = sael.buildGraphWithAditionalModules(activityScopeModules);
        inject(this);
    }

    private void inject(Object entityToGetInjected) {
        activityGraph.inject(entityToGetInjected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_CHANGE_PASSWORD);
        registerReceiver(broadcastReceiver, intentFilter);
        if (SP.getBoolean(Constants.ACTION_CHANGE_PASSWORD, false)) {
            showChangePassword();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showChangePassword();
        }
    };


    private void showChangePassword() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Cambio de contraseña");
        builder.setMessage("Se ha detectado un cambio de contraseña desde la web");
        builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
            SP.clear();
            SP.putBoolean(Constants.INTRODUTION, true);
            finish();
            startActivity(new Intent(BaseActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        });
        builder.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void setTransition(Intent intent) {
        String transition = intent.getStringExtra(EXTRA_TRANSITION);
        switch (transition) {
            case TRANSITION_FADE_SLOW:
                Transition transitionFadeSlow = TransitionInflater.from(this).inflateTransition(R.transition.fade_slow);
                getWindow().setEnterTransition(transitionFadeSlow);
                break;
        }
    }

}
