package info.udone.sael.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;

import info.udone.sael.R;
import info.udone.sael.appeals.AppealsFragment;
import info.udone.sael.document.DocumentFragment;
import info.udone.sael.files.FilesFragment;
import info.udone.sael.measure.MeasuresFragment;
import info.udone.sael.note.NotesFragment;
import info.udone.sael.readmissions.ReadmissionsFragment;
import info.udone.sael.school.SchoolsFragment;
import info.udone.sael.specialty.SpecialtysFragment;
import info.udone.sael.notification.CreateNotificationFragment;
import info.udone.sael.util.Devices;
import info.udone.sael.util.ReportError;

import static info.udone.sael.util.Constants.ID_FRAGMENT;

public class ContainerActivity extends BaseActivity {

    private static String TAG = ContainerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        try {
            if (Devices.isLollipop()) setTransition(getIntent());
        } catch (NullPointerException e) {
//            ReportError.send(e);
        }
        int id = getIntent().getExtras().getInt(ID_FRAGMENT);
        if (savedInstanceState == null) {
            switch (id) {
                case 1:
                    setFragment(AppealsFragment.newInstance());
                    break;
                case 2:
                    setFragment(SchoolsFragment.newInstance());
                    break;
                case 3:
                    setFragment(SpecialtysFragment.newInstance());
                    break;
                case 4:
                    setFragment(MeasuresFragment.newInstance());
                    break;
                case 5:
                    setFragment(NotesFragment.newInstance());
                    break;
                case 6:
                    setFragment(DocumentFragment.newInstance());
                    break;
                case 7:
                    setFragment(ReadmissionsFragment.newInstance());
                    break;
                case 8:
                    setFragment(CreateNotificationFragment.newInstance());
                    break;
                case 9:
                    setFragment(FilesFragment.newInstance());
                    break;
            }
        }
    }

    private void setFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }
}