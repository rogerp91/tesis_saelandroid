package info.udone.sael.ui.adapte;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Measures;
import info.udone.sael.util.ColorGenerator;
import info.udone.sael.util.TextDrawable;
import info.udone.sael.util.UseView;


/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class MeasuresAdapte extends RecyclerView.Adapter<MeasuresAdapte.ViewHolder> {
    private static String TAG = MeasuresAdapte.class.getSimpleName();

    private List<Measures> measuresList;

    public MeasuresAdapte(List<Measures> measuresList) {
        this.measuresList = measuresList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.measures_itam_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Measures measures = measuresList.get(position);
        String measure = measures.getMEDIDA();
        ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
        TextDrawable.IBuilder mDrawableBuilder = TextDrawable.builder().rect();
        TextDrawable drawable = mDrawableBuilder.build(measure, mColorGenerator.getColor(measure));
        holder.mImgColor.setImageDrawable(drawable);
        holder.mCode.setText(String.format("Tipo de medida: %s", measures.getMEDIDA()));
        holder.mAno.setText(Sael.getContext().getString(R.string.periodo) + measures.getANO() + " - " + measures.getPER());
        UseView.setFadeAnimation(holder.mImgColor);
    }

    @Override
    public int getItemCount() {
        return measuresList.size();
    }

    /**
     * TypedArray colors = getResources().obtainTypedArray(R.array.loading_colors);
     * int index = (int) (Math.random() * colors.length());
     * int color = colors.getColor(index, Color.BLACK);
     * textView.setTextColor(color);
     */

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.code)
        TextView mCode;
        @BindView(R.id.ano)
        TextView mAno;
        @BindView(R.id.imgColor)
        ImageView mImgColor;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}