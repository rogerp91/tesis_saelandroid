package info.udone.sael.ui.adapte;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Appeals;
import info.udone.sael.util.ColorGenerator;
import info.udone.sael.util.Functions;
import info.udone.sael.util.ReportError;
import info.udone.sael.util.UseView;


/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class AppealsAdapte extends RecyclerView.Adapter<AppealsAdapte.ViewHolder> {
    private static String TAG = AppealsAdapte.class.getSimpleName();

    private List<Appeals> appealses;

    public AppealsAdapte(List<Appeals> appealses) {
        this.appealses = appealses;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appeals_itam_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
        holder.mLayoutheaderColor.setBackgroundColor(mColorGenerator.getRandomColor());

        Appeals appeals = appealses.get(position);
        holder.mType.setText(String.format("Tipo: %s", appeals.getTIPO()));
        holder.mCreate.setText(String.format("Creado: %s", appeals.getCREADO().split(" ")[0]));
        holder.mAno.setText("Periodo: " + appeals.getANO() + " - " + appeals.getPER());
        UseView.setFadeAnimation(holder.mLayoutheaderColor);
        try {
            if (appeals.getCOMENTARIO().length() == 0) {
                holder.mComment.setText(R.string.no_available);
            } else {
                String tx = appeals.getCOMENTARIO().toLowerCase();
                holder.mComment.setText(Functions.toTitleCase2(tx));
            }
        } catch (NullPointerException e) {
//            ReportError.send(e);
            holder.mComment.setText(R.string.no_available);
        }


    }

    @Override
    public int getItemCount() {
        return appealses.size();
    }

    /**
     * TypedArray colors = getResources().obtainTypedArray(R.array.loading_colors);
     * int index = (int) (Math.random() * colors.length());
     * int color = colors.getColor(index, Color.BLACK);
     * textView.setTextColor(color);
     */

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.create)
        TextView mCreate;
        @BindView(R.id.ano)
        TextView mAno;
        @BindView(R.id.type)
        TextView mType;
        @BindView(R.id.comment)
        TextView mComment;
        @BindView(R.id.layout_header)
        LinearLayout mLayoutheaderColor;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}