package info.udone.sael.ui.adapte;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.domain.entity.ProfileItem;

/**
 * Created by AndrewX on 20/11/2016.
 */

public class ProfileAdapte extends RecyclerView.Adapter<ProfileAdapte.ViewHolder> {

    private List<ProfileItem> profileItems;
    private Context context;

    public ProfileAdapte(List<ProfileItem> profileItems, Context context) {
        this.profileItems = profileItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_profile_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProfileItem profileItem = profileItems.get(position);
        holder.mTitle.setText(profileItem.getTitle());
        holder.mSubName.setText(profileItem.getSubname());
        holder.mType.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary));
        holder.mType.setBackground(profileItem.getDrawable());
    }

    @Override
    public int getItemCount() {
        return profileItems.size();
    }

    @Override
    public long getItemId(int position) {
        return profileItems.get(position).getId();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView mTitle;
        @BindView(R.id.subname)
        TextView mSubName;
        @BindView(R.id.avatar)
        ImageView mType;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}