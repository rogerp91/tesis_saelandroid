package info.udone.sael.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import info.udone.sael.R;
import info.udone.sael.presenter.ProfileContract;
import info.udone.sael.util.ImageTool;

import static info.udone.sael.R.id.ced;


public class ProfileFragment extends BaseFragment implements ProfileContract.View {

    private String TAG = ProfileFragment.class.getSimpleName();

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @BindView(R.id.container)
    ScrollView mScroll;

    @BindView(R.id.avatar)
    CircleImageView mAvatar;

    @BindView(R.id.logo)
    ImageView mLogo;

    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.last)
    TextView mLast;
    @BindView(ced)
    TextView mCed;

    @BindView(R.id.email)
    TextView mEmail;

    @BindView(R.id.titulo)
    TextView mTitulo;
    @BindView(R.id.promedio)
    TextView mPromedio;
    @BindView(R.id.ingreso)
    TextView mIngreso;

    @Inject
    ProfileContract.Presenter presenter;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_personal;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        presenter.setView(this);
        setHasOptionsMenu(true);
        presenter.onResumen();

        //Animation
        Animation animation = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
        animation.setDuration(300);
        mScroll.setAnimation(animation);
        mScroll.animate();
        animation.start();

    }

    @Override
    public void setAvatar(String avatar) {
        Picasso.with(getActivity()).setIndicatorsEnabled(true);
        Picasso.with(getActivity()).load(avatar).priority(Picasso.Priority.HIGH).noFade().error(R.drawable.cloud_sad)
                .placeholder(R.mipmap.ic_launcher).into(mAvatar, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAvatar.setOnClickListener(view1 -> {
            ImageTool.showFullSizePhoto(mAvatar, getActivity());
        });
    }

    @Override
    public void setName(String name) {
        mName.setText(name);
    }

    @Override
    public void setLast(String last) {
        mLast.setText(last);
    }

    @Override
    public void setCed(String ced) {
        mCed.setText(ced);
    }

    @Override
    public void setEmail(String email) {
        mEmail.setText(email);

    }

    @Override
    public void setTitulo(String titulo) {
        mTitulo.setText(titulo);
    }

    @Override
    public void setPromedio(String promedio) {
        mPromedio.setText(promedio);
    }

    @Override
    public void setIng(String ing) {
        mIngreso.setText(ing);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

}