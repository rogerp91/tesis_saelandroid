package info.udone.sael.ui.activity;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.rogerp91.pref.SP;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.domain.source.DeleteContract;
import info.udone.sael.domain.source.NotificationDataSource;
import info.udone.sael.domain.source.local.NotificationLocalDataSource;
import info.udone.sael.login.LoginActivity;
import info.udone.sael.service.DocumentService;
import info.udone.sael.service.NotificationService;
import info.udone.sael.ui.fragment.NotificationFragment;
import info.udone.sael.ui.fragment.ProfileFragment;
import info.udone.sael.ui.fragment.UniversityFragment;
import info.udone.sael.util.ActivityWithOptions;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Devices;

import static info.udone.sael.Sael.getContext;
import static info.udone.sael.util.Constants.EXTRA_TRANSITION;
import static info.udone.sael.util.Constants.FROM_NOTIFICATION;
import static info.udone.sael.util.Constants.ID_FRAGMENT;
import static info.udone.sael.util.Constants.TRANSITION_FADE_SLOW;

public class MainActivity extends BaseActivity {

    private static String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    private boolean from_notification = Boolean.FALSE;

    @Inject
    DeleteContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        try {
            if (Devices.isLollipop()) setTransition(getIntent());
        } catch (NullPointerException e) {
//            ReportError.send(e);
//            ReportLog.send(TAG + " onCreate: " + e.getMessage());
        }


        try {
            Bundle b = getIntent().getExtras();
            if (getIntent().getAction().equals(FROM_NOTIFICATION)) {
                from_notification = Boolean.TRUE;
                saveNotitication(b);
            } else {
                if (getIntent().getAction().equals(NotificationService.ACTION_EMAIL)) {
                    sendEmailError(getIntent());
                } else {
                    if (getIntent().getAction().equals(DocumentService.ACTION_DOWNLOAD_COMPLETE)) {
                        if (Devices.isMarshmallow()) {
                            if (Devices.hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                goToContainer(new Intent(this, ContainerActivity.class).putExtra(ID_FRAGMENT, 9));
                            } else {
                                Snackbar snackbar;
                                snackbar = Snackbar.make(findViewById(R.id.container), "Se require permiso de almacenamiento", Snackbar.LENGTH_SHORT);
                                View snackBarView = snackbar.getView();
                                snackBarView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.info));
                                snackbar.show();
                            }
                        } else {
                            goToContainer(new Intent(this, ContainerActivity.class).putExtra(ID_FRAGMENT, 9));
                        }
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        MenuItem item;
        CharSequence title;
        Fragment fragment;
        if (!from_notification) {
            Log.d(TAG, "onCreate: " + from_notification);
            bottomNavigationView.getMenu().getItem(0).setChecked(true);
            item = bottomNavigationView.getMenu().getItem(0);
            title = item.getTitle();
            fragment = UniversityFragment.newInstance();
        } else {
            Log.d(TAG, "onCreate: item = bottomNavigationView.getMenu().getItem(1);");
            item = bottomNavigationView.getMenu().getItem(1);
            bottomNavigationView.getMenu().getItem(1).setChecked(true);
            title = item.getTitle();
            fragment = NotificationFragment.newInstance();
        }
        setTitle(title);
        setFragment(fragment);

        bottomNavigationView.setOnNavigationItemSelectedListener(item_ -> {
            switch (item_.getItemId()) {
                case R.id.universityItem:
                    setTitle(item_.getTitle());
                    item_.setChecked(true);
                    bottomNavigationView.getMenu().getItem(1).setChecked(false);
                    setFragment(UniversityFragment.newInstance());
                    break;
                case R.id.notificationItem:
                    setTitle(item_.getTitle());
                    item_.setChecked(true);
                    setFragment(NotificationFragment.newInstance());
                    break;
                case R.id.profileItem:
                    bottomNavigationView.getMenu().getItem(1).setChecked(false);
                    item_.setChecked(true);
                    setTitle(item_.getTitle());
                    setFragment(ProfileFragment.newInstance());
                    break;
            }
            return true;
        });
    }

    private void setFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, fragment).setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setting) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(intent, 999);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 999) {
            if (resultCode == 999) {
                MaterialDialog.Builder builder = new MaterialDialog.Builder(this).title("Sael").content("Cerrando sesión, espere por favor...").progress(true, 0).progressIndeterminateStyle(false);
                MaterialDialog dialog = builder.build();
                dialog.show();
                runOnUiThread(() -> presenter.deleteAll());
                Thread thread = new Thread(() -> {
                    SP.clear();
                    SP.putBoolean(Constants.INTRODUTION, true);
                    try {
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                    } catch (IOException e) {
//                        ReportError.send(e);
                    }
                    finish();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    dialog.dismiss();
                    startActivity(intent);
                });
                thread.start();
            }
        }
    }


    private void sendEmailError(Intent intent_) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);
        String error = intent_.getExtras().getString(NotificationService.EXTRA_ERROR_CODE);
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Soporte Sael");
        builder.setIcon(ContextCompat.getDrawable(this, R.mipmap.ic_launcher));
        builder.setMessage("Notificar error producido");
        builder.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        }).setPositiveButton("Aceptar", (dialogInterface, i) -> {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setType("message/rfc822");
            intent.putExtra(Intent.EXTRA_EMAIL, "rogerp.009@gmail.com");
            intent.setData(Uri.parse("mailto:" + "rogerp.009@gmail.com"));
            intent.putExtra(Intent.EXTRA_SUBJECT, "sael.mobile@gmail.com");
            intent.putExtra(Intent.EXTRA_TEXT, error);
            try {
                startActivity(intent);
            } catch (android.content.ActivityNotFoundException e) {
                e.printStackTrace();
                Log.d("Email error:", e.toString());
            }
        });
        builder.show();


    }

    private void saveNotitication(Bundle bundle) {
        Notification notification = new Notification();
        notification.setFroms(bundle.getString("from_to"));
        notification.setTitle(bundle.getString("title"));
        notification.setBody(bundle.getString("body"));
        notification.setTime(bundle.getString("time"));
        NotificationDataSource localDataSource = new NotificationLocalDataSource();
        localDataSource.saveNotification(notification);
    }

    private void goToContainer(Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(EXTRA_TRANSITION, TRANSITION_FADE_SLOW);
            ActivityWithOptions.startActivityWithOptions(intent, this);
        } else {
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
}
