package info.udone.sael.util;

public class Constants {

    public static final String FCM = "fcm";

    public static final String INTRODUTION = "introdution";

    public static final String SESSION = "session";

    public static final String TOKEN = "token";

    public static final String ACTION_CHANGE_PASSWORD = "action_change_password";
    public static final String ACTION_DEVICE_DISABLE = "action_device_disable";

    public static final String PHOTO = "photo";
    public static final String ADMIN = "admin";
    public static final String ID = "id";
    public static final String CEDULA = "cedula";
    public static final String NOMBRE1 = "nombre1";
    public static final String NOMBRE2 = "nombre2";
    public static final String APELLIDO1 = "apelldio1";
    public static final String APELLIDO2 = "apellido2";
    public static final String CORREO = "correo";
    public static final String ANO_ING = "ano_ing";
    public static final String PER_ING = "per_ing";
    public static final String ESTATUS = "estatus";
    public static final String TITULO = "titulo";
    public static final String CODIGO = "codigo";
    public static final String PROMEDIO = "promedio";

    public static final String CODE = "codigo";
    public static final String PEMSUN = "pensum";

    public static final String PATH_BASE = "Sael";

    /**
     * Table SQL
     */
    public final static String SQL_DIR = "sql";
    public final static String CREATEFILE = "sael.sql";
    public final static String UPGRADEFILE_PREFIX = "upgrade-";
    public final static String UPGRADEFILE_SUFFIX = ".sql";
    public final static String DATABASE_NAME = "Sael";
    public final static int DATABASE_VERSION = 1;
    public static final String TABLE_APPEALS = "Appeals";
    public static final String TABLE_NOTE = "Note";
    public static final String TABLE_SPECIALTYS = "Specialtys";
    public static final String TABLE_SCHOOL = "School";
    public static final String TABLE_MATERIA = "Materia";
    public static final String TABLE_REQUISITO = "Requisito";
    public static final String TABLE_ELECTIVE = "Elective";

    public static final String PREF_USER_FIRST_TIME = "user_first_time";

    public static final String EXTRA_TRANSITION = "EXTRA_TRANSITION";
    public static final String TRANSITION_FADE_FAST = "FADE_FAST";
    public static final String TRANSITION_FADE_SLOW = "FADE_SLOW";
    public static final String TRANSITION_SLIDE_RIGHT = "SLIDE_RIGHT";
    public static final String TRANSITION_SLIDE_BOTTOM = "SLIDE_BOTTOM";
    public static final String TRANSITION_EXPLODE = "EXPLODE";
    public static final String TRANSITION_EXPLODE_BOUNCE = "EXPLODE_BOUNCE";

    public static final String ID_FRAGMENT = "id_fragment";

    public static final String FROM_NOTIFICATION = "info.udone.sael.intent.FROM_NOTIFICATION";


}