package info.udone.sael.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

/**
 * Created by Roger Patiño on 07/07/2016.
 */

public class Typefaces {

    private static final Hashtable<String, Typeface> typefaceCache = new Hashtable<>();

    public static Typeface getTypeface(String assetPath, Context context) {
        synchronized (typefaceCache) {
            if (!typefaceCache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(context.getAssets(), "fonts/" + assetPath + ".ttf");
                    typefaceCache.put(assetPath, t);
                } catch (Exception e) {
                    Log.e("Typefaces", "Error en typeface '" + assetPath + "' Por " + e.getMessage());
                    return null;
                }
            }
            return typefaceCache.get(assetPath);
        }
    }
}