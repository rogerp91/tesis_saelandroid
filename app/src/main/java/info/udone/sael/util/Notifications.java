package info.udone.sael.util;

import android.support.annotation.IntegerRes;

import info.udone.sael.R;

/**
 * Created by AF on 28/12/2016.
 */

public class Notifications {


    public static int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_notifications_white_24dp : R.mipmap.ic_launcher;
    }

    public static int getNotificationError() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_error_white_24dp : R.mipmap.ic_launcher;
    }
}