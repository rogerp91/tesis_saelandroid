package info.udone.sael.util;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import info.udone.sael.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by AF on 02/01/2017.
 */

public class ImageTool {

    public static void showFullSizePhoto(ImageView imageView, Context context) {
        AlertDialog.Builder imageDialog = new AlertDialog.Builder(context, R.style.CustomDialogImg);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_image, (ViewGroup) ((Activity) context).findViewById(R.id.layout_root));
        ImageView image = (ImageView) layout.findViewById(R.id.image_detail);
        image.setImageDrawable(imageView.getDrawable());
        imageDialog.setView(layout);
        AlertDialog dialog = imageDialog.create();
        dialog.show();
    }

}
