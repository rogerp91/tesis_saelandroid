package info.udone.sael.util;

/**
 * Created by AndrewX on 21/11/2016.
 */

public class FormatDBString {

    public static String getFormatDBString(String StringToFormat) {
        if (StringToFormat == null || StringToFormat.equals("")) {
            return "'" + "" + "'";
        } else
            return "'" + StringToFormat.replace("'", "''") + "'";
    }

}