package info.udone.sael.util;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import info.udone.sael.domain.entity.Electives;
import info.udone.sael.ui.adapte.ElectiveAdapte;

/**
 * Created by avendano on 24/02/17.
 */

public class ElectiveSemesterFilter extends Filter {

    private List<Electives> electives;
    private List<Electives> filteredList;
    private ElectiveAdapte adapter;

    public ElectiveSemesterFilter(List<Electives> electives, ElectiveAdapte adapter) {
        this.electives = electives;
        this.adapter = adapter;
        this.filteredList = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {
        filteredList.clear();
        final FilterResults results = new FilterResults();
        if (charSequence.equals("all")) {
            for (final Electives item : electives) {
                filteredList.add(item);
            }
        } else {
            for (final Electives item : electives) {
                if (item.getSEMESTRE().contains(charSequence)) {
                    filteredList.add(item);
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        adapter.setList(filteredList);
        adapter.notifyDataSetChanged();
    }
}
