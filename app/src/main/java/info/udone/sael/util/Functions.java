package info.udone.sael.util;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.text.DecimalFormat;

import info.udone.sael.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Roger Patiño on 09/08/2016.
 */

public class Functions {

    public static String toTitleCase(String s) {
        if (s.isEmpty()) {
            return s;
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public static String toTitleCase2(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String anArr : arr) {
            sb.append(Character.toUpperCase(anArr.charAt(0))).append(anArr.substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public static String bytesToHuman(long size) {
        long Kb = 1024;
        long Mb = Kb * 1024;
        long Gb = Mb * 1024;
        long Tb = Gb * 1024;
        long Pb = Tb * 1024;
        long Eb = Pb * 1024;
        if (size < Kb) return floatForm(size) + " Byte";
        if (size >= Kb && size < Mb) return floatForm((double) size / Kb) + " KB";
        if (size >= Mb && size < Gb) return floatForm((double) size / Mb) + " MB";
        if (size >= Gb && size < Tb) return floatForm((double) size / Gb) + " GB";
        if (size >= Tb && size < Pb) return floatForm((double) size / Tb) + " TB";
        if (size >= Pb && size < Eb) return floatForm((double) size / Pb) + " PB";
        if (size >= Eb) return floatForm((double) size / Eb) + " Eb";
        return "???";
    }

    private static String floatForm(double d) {
        return new DecimalFormat("#.##").format(d);
    }


}