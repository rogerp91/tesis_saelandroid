package info.udone.sael.util;

import info.udone.sael.domain.entity.ProvideClient;
import info.udone.sael.domain.source.remote.HttpRestClient;
import info.udone.sael.domain.source.remote.HttpRestClientExternal;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by AF on 29/12/2016.
 */

public class ClientUtils {

    public static HttpRestClient getRestClient(ProvideClient client, String url) {
        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(client.getGson()))
                .client(client.getOkHttpClient())
                .build().create(HttpRestClient.class);

    }

    public static HttpRestClientExternal getRestClientExtenal(ProvideClient client, String url) {
        return new Retrofit.Builder()
                .baseUrl(url+ "")
                .addConverterFactory(GsonConverterFactory.create(client.getGson()))
                .client(client.getOkHttpClient())
                .build().create(HttpRestClientExternal.class);

    }

}