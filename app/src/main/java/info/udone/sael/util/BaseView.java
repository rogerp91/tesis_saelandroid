package info.udone.sael.util;

import java.util.List;

/**
 * Created by Roger Patiño on 02/08/2016.
 */
public interface BaseView<T> {

    void setTitle();

    void showProgress(boolean active);// Progreso

    void setLoadingIndicator(boolean active);// Indicador

    void showModels(List<T> objects);// Listado de proyect

    void showNoModels(final boolean active);// No hay

    void showNetworkError(final boolean active);// No red

    void showErrorOcurred(final boolean active);

    void showErrorNotSolve(final boolean active);

    void showMessage(String message);

    boolean isActive();

}