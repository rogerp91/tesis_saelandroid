package info.udone.sael.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import info.udone.sael.Sael;

/**
 * Created by Roger Pati&ntilde;o on 20/07/2016.
 */

public class RobotoLightTextView extends TextView {

    public RobotoLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RobotoLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public RobotoLightTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setTypeface(Typefaces.getTypeface("Roboto-Light", Sael.getContext()));
    }
}
