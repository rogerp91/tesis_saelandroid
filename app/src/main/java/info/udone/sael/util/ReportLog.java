package info.udone.sael.util;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by AF on 27/12/2016.
 */

public class ReportLog {

    public static void send(String s) {
        FirebaseCrash.log(s);
    }

}
