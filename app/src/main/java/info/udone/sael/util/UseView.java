package info.udone.sael.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.AlphaAnimation;

/**
 * Created by Roger Patiño on 23/12/2016.
 */

public class UseView {

    /**
     * Mostrar o no mostrar un elemento
     *
     * @param active  {@link Boolean}
     * @param view    {@link View}
     * @param context {@link Context}
     * @param isAdded {@link Fragment#isAdded()}
     */
    public static void showOrHideView(final boolean active, final View view, Context context, boolean isAdded) {
        if (!isAdded) {// onPause o onDestroy
            return;
        }
        int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);
        view.setVisibility(active ? View.VISIBLE : View.GONE);
        view.animate().setDuration(shortAnimTime).alpha(active ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(active ? View.VISIBLE : View.GONE);
            }
        });
    }

    public static void showOrHideViewMultiple(final boolean active, final View view1, final View view2, Context context, boolean isAdded) {
        if (!isAdded) {// onPause o onDestroy
            return;
        }
        int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);
        view1.setVisibility(active ? View.GONE : View.VISIBLE);
        view1.animate().setDuration(shortAnimTime).alpha(active ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view1.setVisibility(active ? View.GONE : View.VISIBLE);
            }
        });
        view2.setVisibility(active ? View.VISIBLE : View.GONE);
        view2.animate().setDuration(shortAnimTime).alpha(active ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view2.setVisibility(active ? View.VISIBLE : View.GONE);
            }
        });
    }

    public static void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }


}