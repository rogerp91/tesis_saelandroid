package info.udone.sael.util;

import android.app.ActivityManager;
import android.content.Context;

import java.util.List;

import info.udone.sael.Sael;

/**
 * Created by AF on 30/12/2016.
 */

public class ServiceTools {

    public static boolean isServiceRunning(String serviceClassName) {
        final ActivityManager activityManager = (ActivityManager) Sael.getContext().getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            if (runningServiceInfo.service.getClassName().equals(serviceClassName)) {
                return true;
            }
        }
        return false;
    }

}