package info.udone.sael.util;

import android.util.Base64;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import info.udone.sael.domain.entity.Electives;
import info.udone.sael.domain.entity.Materias;
import info.udone.sael.domain.entity.Requisito;

/**
 * Created by AndrewX on 21/11/2016.
 */

public class Convertion {

    public static String TAG = Convertion.class.getSimpleName();

    public static List<Materias> getPensumArray(JsonArray jsonArray) {

        List<Materias> materiases = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); ++i) {
            JsonObject object = (JsonObject) jsonArray.get(i);
            Materias materias = new Materias();
            if (!object.isJsonNull()) {
                materias.setCARRERA(object.get("CARRERA").getAsString());
                materias.setPENSUM(object.get("PENSUM").getAsString());
                materias.setCODIGO(object.get("CODIGO").getAsString());
                materias.setNOMBRE(object.get("NOMBRE").getAsString());
                materias.setTIPO_MATERIA(object.get("TIPO_MATERIA").getAsString());
                materias.setTIPO_MATERIA_NOMBRE(object.get("TIPO_MATERIA_NOMBRE").getAsString());
                materias.setSEMESTRE(object.get("SEMESTRE").getAsString());
                materias.setCREDITOS(object.get("CREDITOS").getAsString());
                materias.setESPECIAL(object.get("ESPECIAL").getAsString());
                JsonArray jsonRequisitos = object.getAsJsonArray("REQUISITOS");
                if (!jsonRequisitos.isJsonNull() && jsonRequisitos != null) {
                    List<Requisito> requisitos = new ArrayList<>();
                    for (int j = 0; j < jsonRequisitos.size(); j++) {
                        try {
                            JsonObject jObject = (JsonObject) jsonRequisitos.get(j);
                            if (!jObject.isJsonNull()) {
                                Requisito requisito = new Requisito();
                                requisito.setCODIGO(jObject.get("CODIGO").getAsString());
                                requisito.setNOMBRE(jObject.get("NOMBRE").getAsString());
                                requisitos.add(requisito);
                            }
                        } catch (Exception e) {
//                            ReportError.send(e);
                            Log.e(TAG, "getPensumArray: " + e.getMessage());
                        }
                    }
                    materias.setREQUISITOS(requisitos);
                } else {
                    materias.setREQUISITOS(null);
                }
            }
            materiases.add(materias);
        }
        return materiases;
    }

    public static List<Electives> getElectiveArray(JsonArray jsonArray) {

        List<Electives> electives = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); ++i) {
            JsonObject object = (JsonObject) jsonArray.get(i);
            Electives elective = new Electives();
            if (!object.isJsonNull()) {
                elective.setCARRERA(object.get("CARRERA").getAsString());
                elective.setPENSUM(object.get("PENSUM").getAsString());
                elective.setCODIGO(object.get("CODIGO").getAsString());
                elective.setNOMBRE(object.get("NOMBRE").getAsString());
                elective.setTIPO_MATERIA(object.get("TIPO_MATERIA").getAsString());
                elective.setTIPO_MATERIA_NOMBRE(object.get("TIPO_MATERIA_NOMBRE").getAsString());
                elective.setSEMESTRE(object.get("SEMESTRE").getAsString());
                elective.setCREDITOS(object.get("CREDITOS").getAsString());
                elective.setESPECIAL(object.get("ESPECIAL").getAsString());
                JsonArray jsonRequisitos = object.getAsJsonArray("REQUISITOS");
                if (!jsonRequisitos.isJsonNull()) {
                    List<Requisito> requisitos = new ArrayList<>();
                    for (int j = 0; j < jsonRequisitos.size(); j++) {
                        try {
                            JsonObject jObject = (JsonObject) jsonRequisitos.get(j);
                            if (!jObject.isJsonNull()) {
                                Requisito requisito = new Requisito();
                                requisito.setCODIGO(jObject.get("CODIGO").getAsString());
                                requisito.setNOMBRE(jObject.get("NOMBRE").getAsString());
                                requisitos.add(requisito);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "getElectiveArray: " + e.getMessage());
                        }

                    }
                    elective.setREQUISITOS(requisitos);
                } else {
                    elective.setREQUISITOS(null);
                }
            }
            electives.add(elective);
        }
        return electives;
    }

    public static String getBase64(String text) {
        String base64 = "";
        try {
            byte[] data = text.getBytes("UTF-8");
            base64 = Base64.encodeToString(data, Base64.DEFAULT);
//            byte[] data = Base64.decode(base64, Base64.DEFAULT);
//            tex = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return base64;
    }


}