package info.udone.sael.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import info.udone.sael.R;
import info.udone.sael.domain.entity.Document;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.domain.entity.PushNotification;

/**
 * Created by AF on 26/12/2016.
 */

public class Mapper {

    public static List<Document> addDocuemntList(Context context, Integer... allowed) {
        List<Document> documents = new ArrayList<>();
        if (allowed[0] == 1) {
            documents.add(new Document(1, context.getResources().getString(R.string.interno), ContextCompat.getDrawable(context, R.drawable.pdf_interno), "Constancias interna no requiere Pago y Firma del Jefe"));
        }
        if (allowed[1] == 2) {
            documents.add(new Document(2, context.getResources().getString(R.string.externo), ContextCompat.getDrawable(context, R.drawable.pdf_externo), "Constancias externa requiere Pago y Firma del Jefe"));
        }
        if (allowed[2] == 3) {
            documents.add(new Document(3, context.getResources().getString(R.string.beca_funjemasu_solicitud_o_renovacion), ContextCompat.getDrawable(context, R.drawable.pdf_fontur), "Constancias para Beca Funjemasu (solicitud o renovacion)"));
        }
        if (allowed[3] == 4) {
            documents.add(new Document(4, context.getResources().getString(R.string.fontur_ne_solicitud_de_tarjeta_o_recarga), ContextCompat.getDrawable(context, R.drawable.pdf_beca), "Constancias Fontur-NE (solicitud de tarjeta o recarga)"));
        }
        return documents;
    }

    public static PushNotification getNotification(Notification notification) {
        PushNotification pushNotification = new PushNotification();
        pushNotification.setTitle(notification.getTitle());
        pushNotification.setBody(notification.getBody());
        pushNotification.setTime(notification.getTime());
        pushNotification.setFrom_to(notification.getFroms());
        pushNotification.setCarrera(notification.getCarrera());
        pushNotification.setCedula(notification.getCedula());
        pushNotification.setProridad(notification.getProridad());
        return pushNotification;
    }

}