package info.udone.sael.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.TimeUnit;

import info.udone.sael.Sael;
import okhttp3.OkHttpClient;


/**
 * Created by Roger Patiño on 15/06/2016.
 */
public class Networks {

    public static boolean isOnline(Context context) {
        Context ctx = context == null ? Sael.getContext() : context;
        ConnectivityManager connMgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static OkHttpClient getComponentOkHttp() {
        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
    }
}