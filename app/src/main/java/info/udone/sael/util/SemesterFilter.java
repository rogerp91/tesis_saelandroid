package info.udone.sael.util;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import info.udone.sael.domain.entity.Materias;
import info.udone.sael.ui.adapte.PensumAdapte;

/**
 * Created by AF on 03/01/2017.
 */

public class SemesterFilter extends Filter {

    private List<Materias> materiases;
    private List<Materias> filteredList;
    private PensumAdapte adapter;

    public SemesterFilter(List<Materias> materiases, PensumAdapte adapter) {
        this.materiases = materiases;
        this.adapter = adapter;
        this.filteredList = new ArrayList<>();
    }

    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {
        filteredList.clear();
        final FilterResults results = new FilterResults();
        if (charSequence.equals("all")) {
            for (final Materias item : materiases) {
                filteredList.add(item);
            }
        } else {
            for (final Materias item : materiases) {
                if (item.getSEMESTRE().contains(charSequence)) {
                    filteredList.add(item);
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        adapter.setList(filteredList);
        adapter.notifyDataSetChanged();
    }
}
