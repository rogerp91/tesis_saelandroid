package info.udone.sael.util;

import android.support.annotation.NonNull;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public interface BasePresenter<T> {

    void setView(@NonNull T view);

    void start();

    void loadModels(boolean showLoadingUI);

    void deleteAllModels();

}