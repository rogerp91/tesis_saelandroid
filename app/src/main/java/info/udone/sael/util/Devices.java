package info.udone.sael.util;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;

import java.util.Arrays;
import java.util.UUID;

import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Device;

import static android.os.Build.BOARD;
import static android.os.Build.BRAND;
import static android.os.Build.CPU_ABI;
import static android.os.Build.CPU_ABI2;
import static android.os.Build.DISPLAY;
import static android.os.Build.HARDWARE;
import static android.os.Build.HOST;
import static android.os.Build.MANUFACTURER;
import static android.os.Build.MODEL;
import static android.os.Build.PRODUCT;
import static android.os.Build.SERIAL;
import static android.os.Build.SUPPORTED_ABIS;

/**
 * Created by Roger Patiño on 08/07/2016.
 */

public class Devices {
    /**
     * Version del disposition
     *
     * @return version
     */
    public static String getVersion() {
        PackageInfo pInfo = null;
        String version = "";
        try {
            pInfo = Sael.getContext().getPackageManager().getPackageInfo(Sael.getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "";
        }
        assert pInfo != null;
        version = pInfo.versionName;
        return version;
    }

    /**
     * @return Es android lollipop
     */
    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    /**
     * @return Verificar si es marshmallow
     */
    public static boolean isMarshmallow() {
        return Build.VERSION.SDK_INT > 22;
    }

    /**
     * Verificar si el permiso es garantizado
     *
     * @param permission Permiso
     * @return bool
     */
    public static boolean hasPermission(String permission) {
        int res = Sael.getContext().checkCallingOrSelfPermission(permission);
        return res == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * @return verifica si se puede leer la memoria externa
     */
    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    /**
     * @return verifica si pel almacenamiento esta disponible
     */
    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static Device getDevices(String fcm) {
        Device device = new Device();
        device.setKey_fcm(fcm);
        device.setBoard(BOARD);
        device.setBrand(BRAND);
        device.setCpu_abi(CPU_ABI);
        device.setCpu_abi2(CPU_ABI2);
        device.setKey_device(UUID.randomUUID().toString());
        device.setDisplay_devices(DISPLAY);
        device.setHardware(HARDWARE);
        device.setHardware(HOST);
        device.setManufacturer(MANUFACTURER);
        device.setModel(MODEL);
        device.setProduct(PRODUCT);
        device.setSerial(SERIAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            device.setSupported_abis(Arrays.toString(SUPPORTED_ABIS));
        }
        device.setVersion_cevice(Build.VERSION.RELEASE);
        device.setVersion_codes(Integer.toString(Build.VERSION.SDK_INT));
        return device;

    }

}
