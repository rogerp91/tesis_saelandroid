package info.udone.sael.util;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * Created by AndrewX on 20/11/2016.
 */

public class ActivityWithOptions {


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void startActivityWithOptions(Intent intent, Activity activity) {
        ActivityOptions transitionActivity = null;
        transitionActivity = ActivityOptions.makeSceneTransitionAnimation(activity);
        activity.startActivity(intent, transitionActivity.toBundle());
    }

}
