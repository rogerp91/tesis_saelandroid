package info.udone.sael.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import info.udone.sael.domain.entity.Requisito;

/**
 * Created by AndrewX on 21/11/2016.
 */

public class RequisitoDeserializer implements JsonDeserializer<List<Requisito>> {

    private String TAG = RequisitoDeserializer.class.getSimpleName();

    @Override
    public List<Requisito> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        List<Requisito> requisitos = new ArrayList<>();
        try {
            JsonArray data = json.getAsJsonObject().getAsJsonArray("REQUISITOS");
            if (data.isJsonNull() || data.getAsJsonNull() == null) {
                return null;
            } else {
                for (int i = 1; i < data.size(); i++) {
                    JsonObject jObject = (JsonObject) data.get(i);
                    if (!jObject.isJsonNull()) {
                        Requisito requisito = new Requisito();
                        requisito.setCODIGO(jObject.get("CODIGO").getAsString());
                        requisito.setNOMBRE(jObject.get("NOMBRE").getAsString());
                        requisitos.add(requisito);
                    }
                }
            }
        } catch (IllegalStateException e) {
//            ReportError.send(e);
            return null;
        }

//        if (data.isJsonNull() || data.getAsJsonNull() == null) {
//            return null;
//        } else {
//            for (JsonElement e : data) {
//                requisitos.add(context.deserialize(e, Requisito.class));
//            }
//        }
//        JsonObject jsonObject = json.getAsJsonObject();
//        try {
//            if (jsonObject.isJsonNull()) {
//                requisito = null;
//            } else {
//                String CODIGO = jsonObject.get("CODIGO").getAsString();
//                String NOMBRE = jsonObject.get("NOMBRE").getAsString();
//                requisito.setNOMBRE(NOMBRE);
//                requisito.setCODIGO(CODIGO);
//            }
//        } catch (Exception e) {
//            requisito = null;
//            Log.e(TAG, "deserialize: " + e.getMessage());
//        }

        return requisitos;
    }

}