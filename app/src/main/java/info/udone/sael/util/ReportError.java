package info.udone.sael.util;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by AF on 27/12/2016.
 */

public class ReportError {

    public static void send(Throwable e) {
        FirebaseCrash.report(e);
    }

}
