package info.udone.sael.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import info.udone.sael.Sael;

/**
 * Created by Roger Pati&ntilde;o on 20/07/2016.
 */

public class RobotoBoldTextView extends TextView {

    public RobotoBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public RobotoBoldTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setTypeface(Typefaces.getTypeface("Roboto-Bold", Sael.getContext()));
    }
}
