package info.udone.sael.measure;

import android.provider.BaseColumns;

import info.udone.sael.domain.entity.Measures;
import info.udone.sael.util.BasePresenter;
import info.udone.sael.util.BaseView;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public interface MeasuresContract {

    // TODO: View
    interface View extends BaseView<Measures> {

    }

    // TODO: Presentador
    interface Presenter extends BasePresenter<View> {

    }

    abstract class MeasureEntry implements BaseColumns {
        public static final String TABLE_NAME = "Measures";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_PER = "PER";
        public static final String COLUMN_NAME_ANO = "ANO";
        public static final String COLUMN_NAME_CEDULA = "CEDULA";
        public static final String COLUMN_NAME_MEDIDA = "MEDIDA";
    }

}