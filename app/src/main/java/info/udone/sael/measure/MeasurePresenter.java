package info.udone.sael.measure;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Measures;
import info.udone.sael.domain.source.MeasureDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class MeasurePresenter implements MeasuresContract.Presenter {

    private MeasuresContract.View view;
    private MeasureDataSource repository;

    private Context context;

    @Inject
    MeasurePresenter(@Named("re_measure") MeasureDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }

    @Override
    public void setView(@NonNull MeasuresContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
        view.setTitle();
    }

    @Override
    public void start() {
        loadModels(true);
    }

    @Override
    public void loadModels(boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onPregress();
        } else {
            onIndicator();
        }
    }

    private void onPregress() {
        view.showProgress(true);
        view.showErrorNotSolve(false);
        view.showNoModels(false);
        view.showErrorOcurred(false);
        repository.getMeasure(new MeasureDataSource.LoadMeasureCallback() {
            @Override
            public void onLoaded(List<Measures> measures) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (measures.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(measures);
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNoModels(true);
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorOcurred(true);
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorNotSolve(true);
            }

            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNetworkError(true);
            }
        });
    }

    private void onIndicator() {
        repository.refleshMeasure(new MeasureDataSource.LoadMeasureCallback() {
            @Override
            public void onLoaded(List<Measures> measuresList) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (measuresList.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(measuresList);
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.no_data_available_appeals));
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.error_occurred));
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.error_many));
            }

            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.no_connection));
            }
        });
    }

    private void showMessge(String msg) {
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }

    @Override
    public void deleteAllModels() {
        repository.deleteAllMeasure();
    }
}
