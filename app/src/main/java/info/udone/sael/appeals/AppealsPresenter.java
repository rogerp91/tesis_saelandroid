package info.udone.sael.appeals;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Appeals;
import info.udone.sael.domain.source.AppealsDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class AppealsPresenter implements AppealsContract.Presenter {
    
    private AppealsContract.View view;
    private AppealsDataSource repository;

    private Context context;

    @Inject
    AppealsPresenter(@Named("re_appeals") AppealsDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }
    
    @Override
    public void setView(@NonNull AppealsContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
        view.setTitle();
    }
    
    @Override
    public void start() {
        loadModels(true);
    }
    
    @Override
    public void loadModels(boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onProgress();
        } else {
            onIndicator();
        }
    }
    
    private void onProgress() {
        view.showProgress(true);
        view.showErrorNotSolve(false);
        view.showNoModels(false);
        view.showErrorOcurred(false);
        repository.getAppeals(new AppealsDataSource.LoadAppealCallback() {
            @Override
            public void onLoaded(List<Appeals> appealses) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (appealses.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(appealses);
                }
            }
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNoModels(true);
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorOcurred(true);
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorNotSolve(true);
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNetworkError(true);
            }
        });
    }
    
    private void onIndicator() {
        repository.refleshAppeals(new AppealsDataSource.LoadAppealCallback() {
            @Override
            public void onLoaded(List<Appeals> appealses) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (appealses.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(appealses);
                }
            }
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                view.setLoadingIndicator(false);
                showMessage(context.getString(R.string.no_data_available_appeals));
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                view.setLoadingIndicator(false);
                showMessage(context.getString(R.string.error_occurred));
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                view.setLoadingIndicator(false);
                showMessage(context.getString(R.string.error_many));
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) return;
                view.setLoadingIndicator(false);
                showMessage(context.getString(R.string.no_connection));
            }
        });
    }
    
    private void showMessage(String msg) {
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }
    
    @Override
    public void deleteAllModels() {
        repository.deleteAllAppeals();
    }
}