package info.udone.sael.appeals;

import android.provider.BaseColumns;

import info.udone.sael.domain.entity.Appeals;
import info.udone.sael.util.BasePresenter;
import info.udone.sael.util.BaseView;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public interface AppealsContract {


    // TODO: View
    interface View extends BaseView<Appeals> {

    }

    // TODO: Presentador
    interface Presenter extends BasePresenter<View> {

    }

    abstract class AppealsEntry implements BaseColumns {
        public static final String TABLE_NAME = "Appeals";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_ANO = "ANO";
        public static final String COLUMN_NAME_PER = "PER";
        public static final String COLUMN_NAME_CREADO = "CREADO";
        public static final String COLUMN_NAME_TIPO = "TIPO";
        public static final String COLUMN_NAME_SUBTIPO = "SUBTIPO";
        public static final String COLUMN_NAME_MOTIVO = "MOTIVO";
        public static final String COLUMN_NAME_COMENTARIO = "COMENTARIO";
    }

}