package info.udone.sael.presenter;

import android.support.annotation.NonNull;

/**
 * Created by AndrewX on 20/11/2016.
 */

public interface ProfileContract {

    interface View {

        void setAvatar(String avatar);

        void setName(String name);

        void setLast(String last);

        void setCed(String ced);

        void setEmail(String email);

        void setTitulo(String titulo);

        void setPromedio(String promedio);

        void setIng(String ing);

        boolean isActive();

    }

    /**
     * TODO: Presentador ResponseLogin
     */
    interface Presenter {

        void setView(@NonNull View view);

        void onResumen();

    }

}