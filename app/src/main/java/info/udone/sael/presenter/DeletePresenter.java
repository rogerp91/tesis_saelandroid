package info.udone.sael.presenter;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.domain.source.DeleteContract;
import info.udone.sael.domain.source.DeleteDataSource;


/**
 * Created by Roger Patiño on 15/08/2016.
 */

public class DeletePresenter implements DeleteContract.Presenter {

    private DeleteDataSource repository;

    @Inject
    public DeletePresenter(@Named("re_delete") DeleteDataSource repository) {
        this.repository = repository;
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public void deleteNotification() {
        repository.deleteNotification();
    }
}