package info.udone.sael.presenter;

import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;

import com.github.rogerp91.pref.SP;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;
import static info.udone.sael.util.Constants.ANO_ING;
import static info.udone.sael.util.Constants.APELLIDO1;
import static info.udone.sael.util.Constants.APELLIDO2;
import static info.udone.sael.util.Constants.CEDULA;
import static info.udone.sael.util.Constants.CORREO;
import static info.udone.sael.util.Constants.NOMBRE1;
import static info.udone.sael.util.Constants.NOMBRE2;
import static info.udone.sael.util.Constants.PROMEDIO;
import static info.udone.sael.util.Constants.TITULO;

/**
 * Created by AndrewX on 20/11/2016.
 */

public class ProfilePresenter implements ProfileContract.Presenter {

    private ProfileContract.View view;

    String url = "";
    String name1 = "";
    String last1 = "";
    String name2 = "";
    String last2 = "";
    String titulo = "";
    String cedula = "";
    String email = "";
    String ano_ing = "";
    String promedio = "";

    @Inject
    ProfilePresenter() {
    }

    @Override
    public void setView(@NonNull ProfileContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
    }

    @Override
    public void onResumen() {
        if (!view.isActive()) return;
        name1 = SP.getString(NOMBRE1);
        last1 = SP.getString(APELLIDO1);
        name2 = SP.getString(NOMBRE2);
        last2 = SP.getString(APELLIDO2);
        titulo = SP.getString(TITULO);
        cedula = SP.getString(CEDULA);
        ano_ing = SP.getString(ANO_ING);
        email = SP.getString(CORREO);
        promedio = SP.getString(PROMEDIO);

        if (name1.isEmpty()) {
            name1 = "No disponible";
        }
        if (last1.isEmpty()) {
            last1 = "No disponible";
        }
        if (name2.isEmpty()) {
            name2 = "No disponible";
        }
        if (last2.isEmpty()) {
            last2 = "No disponible";
        }

        if (titulo.isEmpty()) {
            titulo = "No disponible";
        }
        if (!cedula.isEmpty()) {
            url = "https://mypcplanet.com/sael-api-restful/" + cedula + ".jpg";
        }
        if (email.isEmpty()) {
            email = "No disponible";
        }
        if (promedio.isEmpty()) {
            promedio = "No disponible";
        }
        try {
            byte[] bytes;
            String s2;
            bytes = name1.getBytes("UTF-8");
            s2 = new String(bytes, "UTF-8");
            name1 = s2;
        } catch (UnsupportedEncodingException e) {
            name1 = "No disponible";
        }

        try {
            byte[] bytes;
            String s2;
            bytes = last1.getBytes("UTF-8");
            s2 = new String(bytes, "UTF-8");
            last1 = s2;
        } catch (UnsupportedEncodingException e) {
            last1 = "No disponible";
        }

        name2 = SP.getString(NOMBRE2);
        try {
            byte[] bytes;
            String s2;
            bytes = name2.getBytes("UTF-8");
            s2 = new String(bytes, "UTF-8");
            name2 = s2;
        } catch (UnsupportedEncodingException e) {
            name2 = "No disponible";
        }

        last2 = SP.getString(APELLIDO2);
        try {
            byte[] bytes;
            String s2;
            bytes = last2.getBytes("UTF-8");
            s2 = new String(bytes, "UTF-8");
            last2 = s2;
        } catch (UnsupportedEncodingException e) {
            last2 = "No disponible";
        }

        String decodedName1 = Html.fromHtml(name1).toString();
        String decodedLast1 = Html.fromHtml(last1).toString();
        String decodedName2 = Html.fromHtml(name2).toString();
        String decodedLast2 = Html.fromHtml(last2).toString();

        view.setAvatar(url);
        view.setName(decodedName1 + " " + decodedName2);
        view.setLast(decodedLast1 + " " + decodedLast2);
        view.setCed(cedula);
        view.setEmail(email);
        view.setTitulo(titulo);
        view.setPromedio(promedio);
        view.setIng(ano_ing);
    }
}