package info.udone.sael.specialty;

import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import info.udone.sael.domain.entity.Specialtys;
import info.udone.sael.util.BasePresenter;
import info.udone.sael.util.BaseView;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public interface SpecialtyContract {

    // TODO: View
    interface View extends BaseView<Specialtys> {

        void showPensumDetailsUi(String pensum, String code);

        void showElectiveDetailsUi(String pensum, String code);

    }

    // TODO: Presentador
    interface Presenter extends BasePresenter<View> {

        void openPensumDetails(@NonNull Specialtys requestedSpeciality);

        void openElectiveDetails(@NonNull Specialtys requestedSpeciality);
    }

    abstract class SpecialtyEntry implements BaseColumns {
        public static final String TABLE_NAME = "Specialtys";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_CODIGO = "CODIGO";
        public static final String COLUMN_NAME_TITULO = "TITULO";
        public static final String COLUMN_NAME_PENSUM = "PENSUM";
    }

}