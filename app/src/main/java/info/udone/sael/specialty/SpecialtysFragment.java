package info.udone.sael.specialty;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.roger91.mlprogress.MlProgress;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Specialtys;
import info.udone.sael.elective.ElectiveActivity;
import info.udone.sael.pensum.PensumActivity;
import info.udone.sael.ui.adapte.SpecialtyAdapte;
import info.udone.sael.ui.fragment.BaseFragment;
import info.udone.sael.util.ActivityWithOptions;
import info.udone.sael.util.Constants;
import info.udone.sael.util.CustomRecyclerView;

import static info.udone.sael.util.Constants.EXTRA_TRANSITION;
import static info.udone.sael.util.Constants.TRANSITION_FADE_SLOW;


public class SpecialtysFragment extends BaseFragment implements SpecialtyContract.View {

    public static SpecialtysFragment newInstance() {
        return new SpecialtysFragment();
    }

    @BindView(R.id.progress)
    MlProgress mProgressView;
    @BindView(R.id.layout_message)
    LinearLayout layout_message;
    @BindView(R.id.text_message)
    TextView text_message;
    @BindView(R.id.recycler_list)
    RecyclerView recycler;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private SpecialtyAdapte adapte;

    @Inject
    SpecialtyContract.Presenter presenter;

    @State
    int scrollPos = 0;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_generic;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        presenter.setView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler.setHasFixedSize(true);
        LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.loadModels(false));
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void setTitle() {
        ab.setTitle("Especialidad");
    }

    @Override
    public void showProgress(final boolean active) {
        showOrNotView(active, mProgressView);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showModels(List<Specialtys> objects) {
        adapte = new SpecialtyAdapte(objects, mItemListener);
        adapte.setHasStableIds(true);
        recycler.setAdapter(adapte);
        adapte.notifyDataSetChanged();
        recycler.scrollToPosition(scrollPos);
    }

    @Override
    public void showNoModels(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_data_available_note));
    }

    @Override
    public void showNetworkError(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_connection));
    }

    @Override
    public void showErrorOcurred(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_occurred));
    }

    @Override
    public void showErrorNotSolve(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_many));
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @OnClick(R.id.text_try_again)
    void onClickTryAgain() {
        presenter.start();
    }

    /**
     * Click en listado de item.
     */
    private SpecialityItemListener mItemListener = new SpecialityItemListener() {
        @Override
        public void onPensum(Specialtys clickedSpecialtys, int pos) {
            scrollPos = pos;
            presenter.openPensumDetails(clickedSpecialtys);

        }

        @Override
        public void onElective(Specialtys clickedSpecialtys, int pos) {
            scrollPos = pos;
            presenter.openElectiveDetails(clickedSpecialtys);
        }

    };

    @Override
    public void showPensumDetailsUi(String pensum, String code) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PEMSUN, pensum);
        bundle.putString(Constants.CODE, code);
        goToContainer(new Intent(getActivity(), PensumActivity.class).putExtras(bundle));
    }

    @Override
    public void showElectiveDetailsUi(String pensum, String code) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PEMSUN, pensum);
        bundle.putString(Constants.CODE, code);
        goToContainer(new Intent(getActivity(), ElectiveActivity.class).putExtras(bundle));
    }

    /**
     * Click a pensum o elective desde el adapte
     */
    public interface SpecialityItemListener {

        void onPensum(Specialtys clickedSpecialtys, int pos);

        void onElective(Specialtys clickedSpecialtys, int pos);
    }

    private void goToContainer(Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(EXTRA_TRANSITION, TRANSITION_FADE_SLOW);
            ActivityWithOptions.startActivityWithOptions(intent, getActivity());
        } else {
            startActivity(intent);
            getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

}