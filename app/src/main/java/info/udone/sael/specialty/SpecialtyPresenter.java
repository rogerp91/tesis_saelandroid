package info.udone.sael.specialty;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Specialtys;
import info.udone.sael.domain.source.SpecialtyDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class SpecialtyPresenter implements SpecialtyContract.Presenter {
    
    private SpecialtyContract.View view;
    private SpecialtyDataSource repository;
    
    private Context context;
    
    @Inject
    SpecialtyPresenter(@Named("re_specialty") SpecialtyDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }
    
    @Override
    public void setView(@NonNull SpecialtyContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
        view.setTitle();
    }
    
    @Override
    public void start() {
        loadModels(true);
    }
    
    @Override
    public void loadModels(boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onPregress();
        } else {
            onIndicator();
        }
    }
    
    private void onIndicator() {
        repository.refleshSpecialty(new SpecialtyDataSource.LoadSpecialtyCallback() {
            @Override
            public void onLoaded(List<Specialtys> specialtyses) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (specialtyses.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(specialtyses);
                }
            }
            
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.no_data_available_note));
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.error_occurred));
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.error_many));
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.no_connection));
            }
        });
    }
    
    private void showMessge(String msg) {
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }
    
    private void onPregress() {
        view.showProgress(true);
        view.showErrorNotSolve(false);
        view.showNoModels(false);
        view.showErrorOcurred(false);
        repository.getSpecialtys(new SpecialtyDataSource.LoadSpecialtyCallback() {
            @Override
            public void onLoaded(List<Specialtys> specialtyses) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (specialtyses.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(specialtyses);
                }
            }
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNoModels(true);
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorOcurred(true);
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorNotSolve(true);
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNetworkError(true);
            }
        });
    }
    
    @Override
    public void deleteAllModels() {
        
    }
    
    @Override
    public void openPensumDetails(@NonNull Specialtys requestedSpeciality) {
        view.showPensumDetailsUi(requestedSpeciality.getPENSUM(), requestedSpeciality.getCODIGO());
    }
    
    @Override
    public void openElectiveDetails(@NonNull Specialtys requestedSpeciality) {
        view.showElectiveDetailsUi(requestedSpeciality.getPENSUM(), requestedSpeciality.getCODIGO());
    }
}
