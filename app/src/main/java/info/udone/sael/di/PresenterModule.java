package info.udone.sael.di;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import info.udone.sael.appeals.AppealsContract;
import info.udone.sael.appeals.AppealsPresenter;
import info.udone.sael.document.DocumentContract;
import info.udone.sael.document.DocumentPresenter;
import info.udone.sael.domain.source.DeleteContract;
import info.udone.sael.elective.ElectivePresenter;
import info.udone.sael.elective.ElectivesContract;
import info.udone.sael.files.FileContract;
import info.udone.sael.files.FilesPresenter;
import info.udone.sael.login.LoginContract;
import info.udone.sael.login.LoginPresenter;
import info.udone.sael.measure.MeasurePresenter;
import info.udone.sael.measure.MeasuresContract;
import info.udone.sael.note.NoteContract;
import info.udone.sael.note.NotePresenter;
import info.udone.sael.notification.CreateNotificationContract;
import info.udone.sael.notification.CreateNotificationPresenter;
import info.udone.sael.notification.NotificationContract;
import info.udone.sael.notification.NotificationPresenter;
import info.udone.sael.pensum.PensumContract;
import info.udone.sael.pensum.PensumPresenter;
import info.udone.sael.presenter.DeletePresenter;
import info.udone.sael.presenter.ProfileContract;
import info.udone.sael.presenter.ProfilePresenter;
import info.udone.sael.readmissions.ReadmissionsContract;
import info.udone.sael.readmissions.ReadmissionsPresenter;
import info.udone.sael.school.SchoolContract;
import info.udone.sael.school.SchoolPresenter;
import info.udone.sael.specialty.SpecialtyContract;
import info.udone.sael.specialty.SpecialtyPresenter;

/**
 * Created by Roger Patiño on 01/12/2015.
 */
@Module(library = true, complete = false)
public class PresenterModule {

    @Provides
    @Singleton
    LoginContract.Presenter provideLoginPresenter(LoginPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    ProfileContract.Presenter provideProfilePresenter(ProfilePresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    AppealsContract.Presenter provideAppealsPresenter(AppealsPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    SchoolContract.Presenter provideSchoolPresenter(SchoolPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    NoteContract.Presenter provideAppealsPresenter(NotePresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    SpecialtyContract.Presenter provideSpecialtyPresenter(SpecialtyPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    PensumContract.Presenter providePensumPresenter(PensumPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    ElectivesContract.Presenter provideElectivePresenter(ElectivePresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    MeasuresContract.Presenter provideMeasuresPresenter(MeasurePresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    ReadmissionsContract.Presenter provideProfessorsReadmissions(ReadmissionsPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    NotificationContract.Presenter provideProfessorsReadmissions(NotificationPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    DeleteContract.Presenter provideDeletePresenter(DeletePresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    DocumentContract.Presenter provideDocumentPresenter(DocumentPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    CreateNotificationContract.Presenter provideCreateNotification(CreateNotificationPresenter presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    FileContract.Presenter provideFilePresenter(FilesPresenter presenter) {
        return presenter;
    }


}