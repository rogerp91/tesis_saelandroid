package info.udone.sael.di;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import info.udone.sael.domain.source.AppealsDataSource;
import info.udone.sael.domain.source.DeleteDataSource;
import info.udone.sael.domain.source.ElectiveDataSource;
import info.udone.sael.domain.source.MeasureDataSource;
import info.udone.sael.domain.source.NoteDataSource;
import info.udone.sael.domain.source.NotificationDataSource;
import info.udone.sael.domain.source.PensumDataSource;
import info.udone.sael.domain.source.ProfessorsDataSource;
import info.udone.sael.domain.source.ReadmissionsDataSource;
import info.udone.sael.domain.source.SchoolDataSource;
import info.udone.sael.domain.source.SpecialtyDataSource;
import info.udone.sael.domain.source.repository.AppealsRepository;
import info.udone.sael.domain.source.repository.DeleteRepository;
import info.udone.sael.domain.source.repository.ElectiveRepository;
import info.udone.sael.domain.source.repository.MeasureRepository;
import info.udone.sael.domain.source.repository.NoteRepository;
import info.udone.sael.domain.source.repository.NotificationRepository;
import info.udone.sael.domain.source.repository.PensumRepository;
import info.udone.sael.domain.source.repository.ProfessorsRepository;
import info.udone.sael.domain.source.repository.ReadmissionsRepository;
import info.udone.sael.domain.source.repository.SchoolRepository;
import info.udone.sael.domain.source.repository.SpecialtyRepository;


/**
 * Created by Roger Patiño on 11/07/2016.
 */
@Module(complete = false, library = true)
public class RepositoryModule {

    @Provides
    @Singleton
    @Named("re_appeals")
    AppealsDataSource provideRepositoryAppeals(AppealsRepository repository) {
        return repository;
    }


    @Provides
    @Singleton
    @Named("re_elective")
    ElectiveDataSource provideRepositoryEleticve(ElectiveRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_delete")
    DeleteDataSource provideRepositoryDelete(DeleteRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_measure")
    MeasureDataSource provideRepositorySchool(MeasureRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_note")
    NoteDataSource provideRepositoryAppeals(NoteRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_notification")
    NotificationDataSource provideRepositoryNotification(NotificationRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_pensum")
    PensumDataSource provideRepositoryPensum(PensumRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_professors")
    ProfessorsDataSource provideRepositoryProfessors(ProfessorsRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_readmission")
    ReadmissionsDataSource provideRepositoryReadmissions(ReadmissionsRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_school")
    SchoolDataSource provideRepositorySchool(SchoolRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    @Named("re_specialty")
    SpecialtyDataSource provideRepositorySpeciality(SpecialtyRepository repository) {
        return repository;
    }

}