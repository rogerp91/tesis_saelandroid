package info.udone.sael.di;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import info.udone.sael.domain.source.AppealsDataSource;
import info.udone.sael.domain.source.DeleteDataSource;
import info.udone.sael.domain.source.ElectiveDataSource;
import info.udone.sael.domain.source.MeasureDataSource;
import info.udone.sael.domain.source.NoteDataSource;
import info.udone.sael.domain.source.NotificationDataSource;
import info.udone.sael.domain.source.PensumDataSource;
import info.udone.sael.domain.source.ProfessorsDataSource;
import info.udone.sael.domain.source.ReadmissionsDataSource;
import info.udone.sael.domain.source.SchoolDataSource;
import info.udone.sael.domain.source.SpecialtyDataSource;
import info.udone.sael.domain.source.local.AppealsLocalDataSource;
import info.udone.sael.domain.source.local.DeleteLocalDataSource;
import info.udone.sael.domain.source.local.ElectiveLocalDataSource;
import info.udone.sael.domain.source.local.MeasureLocalDataSource;
import info.udone.sael.domain.source.local.NotesLocalDataSource;
import info.udone.sael.domain.source.local.NotificationLocalDataSource;
import info.udone.sael.domain.source.local.PensumLocalDataSource;
import info.udone.sael.domain.source.local.ProfessorsLocalDataSource;
import info.udone.sael.domain.source.local.ReadmissionsLocalDataSource;
import info.udone.sael.domain.source.local.SchoolLocalDataSource;
import info.udone.sael.domain.source.local.SpecialtyLocalDataSource;
import info.udone.sael.domain.source.remote.AppealsRemoteDataSource;
import info.udone.sael.domain.source.remote.ElectiveRemoteDataSource;
import info.udone.sael.domain.source.remote.MeasureRemoteDataSource;
import info.udone.sael.domain.source.remote.NotesRemoteDataSource;
import info.udone.sael.domain.source.remote.PensumRemoteDataSource;
import info.udone.sael.domain.source.remote.ProfessorsRemoteDataSource;
import info.udone.sael.domain.source.remote.ReadmissionsRemoteDataSource;
import info.udone.sael.domain.source.remote.SchoolRemoteDataSource;
import info.udone.sael.domain.source.remote.SpecialtyRemoteDataSource;

/**
 * Created by Roger Patiño on 11/07/2016.
 */
@Module(complete = false, library = true)
public class DataSourceModule {

    @Provides
    @Singleton
    @Named("re_appeals_l")
    AppealsDataSource provideLocalDataSourceAppeals(AppealsLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_appeals_r")
    AppealsDataSource provideRemoteDataSourceAppeals(AppealsRemoteDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_elective_l")
    ElectiveDataSource provideLocalDataSourceElective(ElectiveLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_elective_r")
    ElectiveDataSource provideRemoteDataSourceElective(ElectiveRemoteDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_delete_l")
    DeleteDataSource provideLocalDataSourceDelete(DeleteLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_measures_l")
    MeasureDataSource provideLocalDataSourceMeasures(MeasureLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_measures_r")
    MeasureDataSource provideRemoteDataSourceMeasures(MeasureRemoteDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_note_l")
    NoteDataSource provideLocalDataSourceNotes(NotesLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_note_r")
    NoteDataSource provideRemoteDataSourceNotes(NotesRemoteDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_notification_l")
    NotificationDataSource provideLocalDataSourceNotification(NotificationLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_pensum_l")
    PensumDataSource provideLocalDataSourcePensum(PensumLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_pensum_r")
    PensumDataSource provideRemoteDataSourcePensum(PensumRemoteDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_profesor_l")
    ProfessorsDataSource provideLocalDataSourcProfessors(ProfessorsLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_profesor_r")
    ProfessorsDataSource provideRemoteDataSourceProfessors(ProfessorsRemoteDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_readmission_l")
    ReadmissionsDataSource provideLocalDataSourceReadmissions(ReadmissionsLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_readmission_r")
    ReadmissionsDataSource provideRemoteDataSourceReadmissions(ReadmissionsRemoteDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_school_l")
    SchoolDataSource provideLocalDataSourceSchool(SchoolLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_school_r")
    SchoolDataSource provideRemoteDataSourceSchool(SchoolRemoteDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_specialty_l")
    SpecialtyDataSource provideLocalDataSourceSpecialty(SpecialtyLocalDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Singleton
    @Named("re_specialty_r")
    SpecialtyDataSource provideRemoteDataSourceSpecialty(SpecialtyRemoteDataSource dataSource) {
        return dataSource;
    }

}