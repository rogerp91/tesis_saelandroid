package info.udone.sael.di;

import android.app.Activity;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import info.udone.sael.ForApplication;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
@Module(includes = {
        FragmentGraphInjectModule.class,
        PresenterModule.class,
        InteractorModule.class,
        RepositoryModule.class,
        DataSourceModule.class,
        ClientModule.class,
},
        library = true,
        complete = false)
public class FragmentModule {

    private final Activity activityContext;

    public FragmentModule(Activity activityContext) {
        this.activityContext = activityContext;
    }

    @Singleton
    @Provides
    @ForApplication
    Context provideActivityContext() {
        return activityContext;
    }

    @Provides
    Activity provideActivityActivity() {//para el fragment
        return activityContext;
    }

}