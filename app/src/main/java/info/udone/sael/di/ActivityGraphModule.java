package info.udone.sael.di;


import dagger.Module;
import info.udone.sael.elective.ElectiveActivity;
import info.udone.sael.login.LoginActivity;
import info.udone.sael.pensum.PensumActivity;
import info.udone.sael.ui.activity.ContainerActivity;
import info.udone.sael.ui.activity.MainActivity;
import info.udone.sael.ui.activity.SecretSettingActivity;
import info.udone.sael.ui.activity.SettingsActivity;


@Module(
        injects = {
                MainActivity.class,
                LoginActivity.class,
                ContainerActivity.class,
                PensumActivity.class,
                ElectiveActivity.class,
                SettingsActivity.class,
                SecretSettingActivity.class

        },
        complete = false
)
class ActivityGraphModule {

}
