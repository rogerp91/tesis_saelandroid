package info.udone.sael.di;

import dagger.Module;
import dagger.Provides;
import info.udone.sael.ForApplication;
import info.udone.sael.Sael;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
@Module(
        injects = Sael.class,
        library = true,
        includes = {
                ExecutorModule.class,
                //ServiceModule.class
        }
)
public class AppModules {

    public Sael app;

    public AppModules(Sael app) {
        this.app = app;
    }

    @ForApplication
    @Provides
    Sael provideApplication() {
        return this.app;
    }
}
