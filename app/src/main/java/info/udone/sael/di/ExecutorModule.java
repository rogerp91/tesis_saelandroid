package info.udone.sael.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import info.udone.sael.executor.Executor;
import info.udone.sael.executor.MainThread;
import info.udone.sael.executor.MainThreadImpl;
import info.udone.sael.executor.ThreadExecutor;

/**
 * Created by Roger Patiño on 30/11/2015.
 */
@Module(library = true)
public class ExecutorModule {

    @Provides
    @Singleton
    Executor provideExecutor(ThreadExecutor threadExecutor) {
        return threadExecutor;
    }

    @Provides
    @Singleton
    MainThread provideMainThread(MainThreadImpl impl) {
        return impl;
    }
}
