package info.udone.sael.di;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import info.udone.sael.document.DocumentContract;
import info.udone.sael.files.FileContract;
import info.udone.sael.interactor.DocumentInteractor;
import info.udone.sael.interactor.FilesInteractor;
import info.udone.sael.interactor.LoginInteractor;
import info.udone.sael.interactor.NotificationInteractor;
import info.udone.sael.login.LoginContract;
import info.udone.sael.notification.CreateNotificationContract;

/**
 * Created by Roger Patiño on 01/12/2015.
 */
@Module(library = true, complete = false)
public class InteractorModule {

    @Provides
    @Singleton
    LoginContract.LoginInteractor provideLoginInteractor(LoginInteractor interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    @Named("generate")
    DocumentContract.GenerateDocumentInteractor provideGenerateInteractor(DocumentInteractor interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    @Named("check")
    DocumentContract.CheckDocumentInteractor provideCheckInteractor(DocumentInteractor interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    FileContract.FileInteractor provideFileInteractor(FilesInteractor interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    CreateNotificationContract.SendNotificationInteractor provideNotificationInteractor(NotificationInteractor interactor) {
        return interactor;
    }

}