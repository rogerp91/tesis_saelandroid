package info.udone.sael.di;

import java.io.File;

import dagger.Module;
import info.udone.sael.appeals.AppealsFragment;
import info.udone.sael.document.DocumentFragment;
import info.udone.sael.elective.ElectiveFragment;
import info.udone.sael.files.FilesFragment;
import info.udone.sael.login.LoginFragment;
import info.udone.sael.measure.MeasuresFragment;
import info.udone.sael.note.NotesFragment;
import info.udone.sael.notification.CreateNotificationFragment;
import info.udone.sael.pensum.PensumFragment;
import info.udone.sael.readmissions.ReadmissionsFragment;
import info.udone.sael.school.SchoolsFragment;
import info.udone.sael.specialty.SpecialtysFragment;
import info.udone.sael.ui.fragment.NotificationFragment;
import info.udone.sael.ui.fragment.ProfileFragment;
import info.udone.sael.ui.fragment.SecretSettingsFragment;
import info.udone.sael.ui.fragment.SettingsFragment;
import info.udone.sael.ui.fragment.UniversityFragment;


/**
 * Created by Roger Patiño on 22/12/2015.
 */
@Module(injects = {
        SettingsFragment.class,
        LoginFragment.class,
        ProfileFragment.class,
        UniversityFragment.class,
        NotificationFragment.class,
        AppealsFragment.class,
        MeasuresFragment.class,
        NotesFragment.class,
//        ProfessorsFragment.class,
        ReadmissionsFragment.class,
        SpecialtysFragment.class,
        SchoolsFragment.class,
//        SemestersFragment.class,
        PensumFragment.class,
        ElectiveFragment.class,
        DocumentFragment.class,
        CreateNotificationFragment.class,
        SecretSettingsFragment.class,
        FilesFragment.class


}, complete = false, library = true)
public class FragmentGraphInjectModule {
}
