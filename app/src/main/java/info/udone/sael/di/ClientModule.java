package info.udone.sael.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import info.udone.sael.BuildConfig;
import info.udone.sael.domain.entity.ProvideClient;
import info.udone.sael.domain.source.remote.HttpRestClient;
import info.udone.sael.domain.source.remote.HttpRestClientExternal;
import info.udone.sael.domain.source.remote.HttpRestClientFirebase;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Roger Patiño on 08/07/2016.
 */

@Module(complete = false, library = true)
public class ClientModule {

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder().serializeNulls().create();
    }

    @Provides
    @Singleton
    public HttpLoggingInterceptor provideInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.LEVEL.equals("1") ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(45, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    @Named("host_firebase")
    public OkHttpClient provideOkHttpClientFirebae(HttpLoggingInterceptor interceptor) {//Host de firebase data base
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(HttpRestClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public ProvideClient provideClient(Gson gson, OkHttpClient okHttpClient) {
        return new ProvideClient(okHttpClient, gson);
    }

    @Provides
    @Singleton
    public HttpRestClient provideHttpRestClient(Retrofit retrofit) {
        return retrofit.create(HttpRestClient.class);
    }


    @Provides
    @Singleton
    public HttpRestClientExternal provideHttpRestClientExternal(Retrofit retrofit) {
        return retrofit.create(HttpRestClientExternal.class);
    }

    @Provides
    @Singleton
    @Named("host_firebase")
    public Retrofit provideRetrofitFirebase(Gson gson, @Named("host_firebase") OkHttpClient okHttpClient) {//Get Host firebase
        return new Retrofit.Builder()
                .baseUrl(HttpRestClientFirebase.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public HttpRestClientFirebase provideHttpRestClientFirebase(@Named("host_firebase") Retrofit retrofit) {//Get Host firebase
        return retrofit.create(HttpRestClientFirebase.class);
    }

}