package info.udone.sael.document;

import android.support.annotation.NonNull;

import java.util.List;

import info.udone.sael.domain.entity.Doc;
import info.udone.sael.domain.entity.Document;
import info.udone.sael.domain.entity.DocumentGenerate;
import info.udone.sael.domain.entity.FileDoc;

/**
 * Created by AndrewX on 22/11/2016.
 */

public interface DocumentContract {

    /**
     * TODO: Vista ResponseLogin
     */
    interface View {

        void setTitle();

        void showModels(List<Document> documents, boolean ci, boolean ce, boolean re);

        void showNoModels(final boolean active);// No hay

        void setLoadingIndicator(boolean active);// Indicador

        void showMessage(String message);

        void onSuccessCheck(Doc doc);

        void onSuccessGenerate(FileDoc doc);

        void showProgress(boolean active);

        void showUnauthorized(boolean active);

        void showUnprocessableEntity(boolean active);

        void showForbidden(boolean active);

        void showNetworkError(boolean active);

        void showErrorOcurred(boolean active);

        void showErrorNotSolve(boolean active);

        void showUnauthorizedGenerate();

        void showUnprocessableEntityGenerate();

        void showForbiddenGenerate();

        void showNetworkErrorGenerate();

        void showErrorOcurredGenerate();

        void showErrorNotSolveGenerate();

        boolean isActive();

        void dialogProgress(boolean active);

    }

    interface Presenter {

        void setView(@NonNull View view);

        void start(@NonNull String cedula);

        void loadModels(boolean showLoadingUI, @NonNull String cedula);

        void docGenerate(@NonNull DocumentGenerate generate);

    }

    /**
     * TODO: Interactor Check
     */
    interface CheckDocumentInteractor {

        interface Callback {

            void onSuccess(Doc doc);

            void onUnauthorized(); // 401

            void onForbidden(); // 403

            void onUnprocessableEntity(); // 422

            void onErrorOcurred(); // Error

            void onErrorNotSolve();// 500

            void onNetworkError();
        }

        void execute(@NonNull String cedula, @NonNull Callback callBackAnt);

    }

    /**
     * TODO: Interactor Generate
     */
    interface GenerateDocumentInteractor {

        interface Callback {

            void onSuccess(FileDoc doc);

            void onUnauthorized(); // 401

            void onForbidden(); // 403

            void onUnprocessableEntity(); // 422

            void onErrorOcurred(); // Error

            void onErrorNotSolve();// 500

            void onNetworkError();


        }

        void execute(@NonNull DocumentGenerate generate, @NonNull Callback callBackAnt);

    }
}