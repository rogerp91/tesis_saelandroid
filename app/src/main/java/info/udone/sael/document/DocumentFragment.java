package info.udone.sael.document;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.roger91.mlprogress.MlProgress;
import com.github.rogerp91.pref.SP;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Doc;
import info.udone.sael.domain.entity.Document;
import info.udone.sael.domain.entity.DocumentGenerate;
import info.udone.sael.domain.entity.FileDoc;
import info.udone.sael.service.DocumentService;
import info.udone.sael.ui.adapte.DocumentAdapte;
import info.udone.sael.ui.fragment.BaseFragment;
import info.udone.sael.util.Devices;
import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;

import static info.udone.sael.util.Constants.CEDULA;


public class DocumentFragment extends BaseFragment implements
        DocumentContract.View {

    private static String TAG = DocumentFragment.class.getSimpleName();

    public static DocumentFragment newInstance() {
        return new DocumentFragment();
    }

    private MaterialDialog.Builder builder = null;
    private MaterialDialog dialog = null;

    @BindView(R.id.progress)
    MlProgress mProgressView;
    @BindView(R.id.layout_message)
    LinearLayout layout_message;
    @BindView(R.id.text_message)
    TextView text_message;
    @BindView(R.id.recycler_list)
    RecyclerView recycler;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private DocumentAdapte adapte;

    @Inject
    DocumentContract.Presenter presenter;

    private String mod_doc = "";

    boolean mCI, mCE, mRE;

    private String cantConstantes = "";
    private String cantEstudio = "";
    private String cantRecord = "";

    //Constancias
    private LinearLayout containerConstancias;

    //Estudio
    private LinearLayout containerEstudio;

    //Record
    private LinearLayout containerRecord;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_generic;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        PermissionGen.with(getActivity()).addRequestCode(100).permissions(Manifest.permission.WRITE_EXTERNAL_STORAGE).request();
        presenter.setView(this);
        presenter.start(SP.getString(CEDULA));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        builder = new MaterialDialog.Builder(getActivity()).title("Generando constancia").content("Espere, esto puede tardar varios minutos").progress(true, 0).progressIndeterminateStyle(false);
        dialog = builder.build();
        recycler.setHasFixedSize(true);
        LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.loadModels(false, SP.getString(CEDULA)));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mod_doc = "";
    }

    @Override
    public void setTitle() {
        ab.setTitle("Constancias");
    }

    @Override
    public void showModels(List<Document> documents, boolean ci, boolean ce, boolean re) {
        mCI = ci;
        mCE = ce;
        mRE = re;
        Log.d("DocumentFragment", "onSuccess: ");
        adapte = new DocumentAdapte(documents, clickTypeDoc);
        adapte.setHasStableIds(true);
        recycler.setAdapter(adapte);
    }

    @Override
    public void showNoModels(boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_data_available_document));
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showMessage(String message) {
        shoeMessageError(message);
    }

    @Override
    public void onSuccessCheck(Doc doc) {

    }

    @Override
    public void onSuccessGenerate(FileDoc doc) {
        shoeMessage("Preparando descarga de archivo...", R.color.success);
        if (!DocumentService.IS_ACTIVE) {
            Intent i = new Intent(getActivity(), DocumentService.class).putExtra(DocumentService.ACTION_DOWNLOAD_URL, doc.getLink()).putExtra(DocumentService.ACTION_DOWNLOAD_ID, doc.getDocId());
            getActivity().startService(i);
        } else {
            shoeMessage("Se esta descargando una constancia", R.color.info);
        }

    }

    @Override
    public void showProgress(boolean active) {
        showOrNotView(active, mProgressView);
    }

    @Override
    public void showUnauthorized(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_autho));
    }

    @Override
    public void showUnprocessableEntity(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_active_body));
    }

    @Override
    public void showForbidden(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_active_body));
    }

    @Override
    public void showNetworkError(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_connection));
    }

    @Override
    public void showErrorOcurred(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_occurred));
    }

    @Override
    public void showErrorNotSolve(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_many));
    }

    @Override
    public void showUnauthorizedGenerate() {
        shoeMessageError(getString(R.string.error_occurred));
    }

    @Override
    public void showUnprocessableEntityGenerate() {
        shoeMessageError(getString(R.string.error_active_body));
    }

    @Override
    public void showForbiddenGenerate() {
        shoeMessageError(getString(R.string.error_active_body));
    }

    @Override
    public void showNetworkErrorGenerate() {
        shoeMessageError(getString(R.string.no_connection));
    }

    @Override
    public void showErrorOcurredGenerate() {
        shoeMessageError(getString(R.string.error_occurred));
    }

    @Override
    public void showErrorNotSolveGenerate() {
        shoeMessageError(getString(R.string.error_many));
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void dialogProgress(boolean active) {
        if (active) {
            dialog.show();
        } else {
            dialog.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @PermissionFail(requestCode = 100)
    public void failPermission() {
        Snackbar snack = Snackbar.make(getView(), "Se necesitan permisos especiales!", Snackbar.LENGTH_INDEFINITE);
        snack.setAction("Configuración", v -> {
            final Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + getActivity().getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(i);
        });
        snack.show();
    }

    public interface ClickTypeDoc {

        void onClickDoc(int type);

    }

    public ClickTypeDoc clickTypeDoc = type -> {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
        builder.setTitle("Tipo de Constancia");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.doc_dialog2, null);
        builder.setView(dialogView);

        containerConstancias = (LinearLayout) dialogView.findViewById(R.id.container_constancias);
        containerEstudio = (LinearLayout) dialogView.findViewById(R.id.container_estudio);
        containerRecord = (LinearLayout) dialogView.findViewById(R.id.container_record);
        Spinner spinner1 = (Spinner) dialogView.findViewById(R.id.segmented_constancia);
        Spinner spinner2 = (Spinner) dialogView.findViewById(R.id.segmented_estudio);
        Spinner spinner3 = (Spinner) dialogView.findViewById(R.id.segmented_record);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.const_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);
        spinner3.setAdapter(adapter);

        if (type == 1) {
            mod_doc = "i";
        } else {
            if (type == 2) {
                mod_doc = "e";
            } else {
                if (type == 3) {
                    mod_doc = "b";
                } else {
                    if (type == 4) {
                        mod_doc = "f";
                    }
                }
            }
        }
        if (mCI) {
            showOrNotView(true, containerConstancias);
        }
        if (mCE) {
            showOrNotView(true, containerEstudio);
        }
        if (mRE) {
            showOrNotView(true, containerRecord);
        }


        builder.setPositiveButton("OK", (dialogInterface, j) -> {

            cantConstantes = spinner1.getSelectedItem().toString();
            cantEstudio = spinner2.getSelectedItem().toString();
            cantRecord = spinner3.getSelectedItem().toString();


            if (cantConstantes.isEmpty() && cantEstudio.isEmpty() && cantRecord.isEmpty()) {
                shoeMessageError("Debe escoger la cantidad de constancia");
                return;
            }
            if (cantConstantes.equals("0") && cantEstudio.equals("0") && cantRecord.equals("0")) {
                shoeMessageError("Debe escoger la cantidad de constancia");
                return;
            }

            if (Devices.isMarshmallow()) {
                if (Devices.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    getGenerate(Integer.parseInt(cantConstantes), Integer.parseInt(cantEstudio), Integer.parseInt(cantRecord));
                } else {
                    Snackbar snack = Snackbar.make(getView(), "Se requiere permiso de almacenamiento", Snackbar.LENGTH_LONG);
                    snack.setAction("Configuración", v -> {
                        final Intent i = new Intent();
                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:" + getActivity().getPackageName()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(i);
                    });
                    snack.show();
                }
            } else {
                getGenerate(Integer.parseInt(cantConstantes), Integer.parseInt(cantEstudio), Integer.parseInt(cantRecord));
            }


        });
        builder.setNegativeButton("Cancelar", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });

        builder.show();

    };

    @OnClick({R.id.text_try_again,})
    void onClickTryAgain() {
        presenter.start(SP.getString(CEDULA));
    }

    private void getGenerate(int cantConstantes, int cantEstudio, int cantRecord) {
        DocumentGenerate documentGenerate = new DocumentGenerate();
        documentGenerate.setCedula(SP.getString(CEDULA));
        documentGenerate.setDoc_modo(mod_doc);
        documentGenerate.setCi(cantConstantes);
        documentGenerate.setCe(cantEstudio);
        documentGenerate.setRe(cantRecord);
        presenter.docGenerate(documentGenerate);
    }
}