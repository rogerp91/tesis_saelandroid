package info.udone.sael.document;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Doc;
import info.udone.sael.domain.entity.Document;
import info.udone.sael.domain.entity.DocumentGenerate;
import info.udone.sael.domain.entity.FileDoc;
import info.udone.sael.util.Mapper;
import info.udone.sael.util.Networks;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by AndrewX on 22/11/2016.
 */

public class DocumentPresenter implements DocumentContract.Presenter {

    private DocumentContract.View view;
    private DocumentContract.GenerateDocumentInteractor genDocInt;
    private DocumentContract.CheckDocumentInteractor checkDocInt;

    private int i = 0;
    private int e = 0;
    private int f = 0;
    private int b = 0;

    private Context context;

    @Inject
    public DocumentPresenter(@Named("generate") DocumentContract.GenerateDocumentInteractor genDocInt, @Named("check") DocumentContract.CheckDocumentInteractor checkDocInt) {
        this.genDocInt = genDocInt;
        this.checkDocInt = checkDocInt;
        context = Sael.getContext();
    }

    @Override
    public void setView(@NonNull DocumentContract.View view) {
        checkNotNull(view, "View not null o empty!");
        this.view = view;
        view.setTitle();
    }

    @Override
    public void start(@NonNull String cedula) {
        loadModels(true, cedula);
    }

    @Override
    public void loadModels(boolean showLoadingUI, @NonNull String cedula) {
        if (showLoadingUI) { // Colocar el cargando
            onProgress(cedula);
        } else {
            onIndicator(cedula);
        }
    }

    private void onProgress(String cedula) {
        view.showProgress(true);
        view.showNetworkError(false);
        view.showErrorNotSolve(false);
        view.showErrorOcurred(false);
        view.showUnauthorized(false);
        view.showForbidden(false);
        view.showUnauthorized(false);
        checkDocInt.execute(cedula, new DocumentContract.CheckDocumentInteractor.Callback() {
            @Override
            public void onSuccess(Doc doc) {
                if (!view.isActive()) {
                    return;
                }

                try {
                    if (!doc.getDocModo().getI().isEmpty()) {//Interno
                        i = 1;
                    }
                } catch (NullPointerException ex) {
                    i = 0;
                }

                try {
                    if (!doc.getDocModo().getE().isEmpty()) {//Externa
                        e = 2;
                    }
                } catch (NullPointerException ex) {
                    e = 0;
                }

                try {
                    if (!doc.getDocModo().getF().isEmpty()) {// Fontur
                        f = 3;
                    }
                } catch (NullPointerException ex) {
                    f = 0;
                }

                try {
                    if (!doc.getDocModo().getB().isEmpty()) {// Fontur
                        b = 4;
                    }
                } catch (NullPointerException ex) {
                    b = 0;
                }

                List<Document> documents = Mapper.addDocuemntList(Sael.getContext(), i, e, f, b);

                if (i == 0 && e == 0 && f == 0 && b == 0) {
                    Log.d("TAG_", "onSuccess: ");
                    view.showNoModels(true);
                    view.showProgress(false);//Progress
                    return;
                }

                //Doc Tipo
                boolean ci = Boolean.FALSE;
                boolean ce = Boolean.FALSE;
                boolean re = Boolean.FALSE;
                try {
                    if (!doc.getDocTipo().getCi().isEmpty()) {//Inscripción
                        ci = Boolean.TRUE;
                    }
                } catch (NullPointerException ex) {
                    ci = Boolean.FALSE;
                }

                try {
                    if (!doc.getDocTipo().getCe().isEmpty()) {//Externa
                        ce = Boolean.TRUE;
                    }
                } catch (NullPointerException ex) {
                    ce = Boolean.FALSE;
                }

                try {
                    if (!doc.getDocTipo().getRe().isEmpty()) {//Record
                        re = Boolean.TRUE;
                    }
                } catch (NullPointerException ex) {
                    re = Boolean.FALSE;
                }

                view.showModels(documents, ci, ce, re);
                view.showProgress(false);//Progress

            }

            @Override
            public void onUnauthorized() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);//Progress
                view.showUnauthorized(true);
            }

            @Override
            public void onForbidden() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);//Progress
                view.showForbidden(true);
            }

            @Override
            public void onUnprocessableEntity() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);//Progress
                view.showUnauthorized(true);
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);//Progress
                view.showErrorOcurred(true);
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);//Progress
                view.showErrorNotSolve(true);
            }

            @Override
            public void onNetworkError() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);//Progress
                view.showNetworkError(true);
            }
        });
    }

    public void onIndicator(String cedula) {
        checkDocInt.execute(cedula, new DocumentContract.CheckDocumentInteractor.Callback() {
            @Override
            public void onSuccess(Doc doc) {
                if (!view.isActive()) {
                    return;
                }

                try {
                    Log.d("TAG_", "onSuccess: " + doc.getDocModo().getI());
                    if (!doc.getDocModo().getI().isEmpty()) {//Interno
                        i = 1;
                    }
                } catch (NullPointerException ex) {
                    i = 0;
                }

                try {
                    Log.d("TAG_", "onSuccess: " + doc.getDocModo().getE());
                    if (!doc.getDocModo().getE().isEmpty()) {//Externa
                        e = 2;
                    }
                } catch (NullPointerException ex) {
                    e = 0;
                }

                try {
                    Log.d("TAG_", "onSuccess: " + doc.getDocModo().getF());
                    if (!doc.getDocModo().getF().isEmpty()) {// Fontur
                        f = 3;
                    }
                } catch (NullPointerException ex) {
                    f = 0;
                }

                try {
                    Log.d("TAG_", "onSuccess: " + doc.getDocModo().getB());
                    if (!doc.getDocModo().getB().isEmpty()) {// Fontur
                        b = 4;
                    }
                } catch (NullPointerException ex) {
                    b = 0;
                }

                List<Document> documents = Mapper.addDocuemntList(Sael.getContext(), i, e, f, b);

                if (i == 0 && e == 0 && f == 0 && b == 0) {
                    Log.d("TAG_", "onSuccess: ");
                    view.showNoModels(true);
                    view.showProgress(false);//Progress
                    return;
                }

                //Doc Tipo
                boolean ci = Boolean.FALSE;
                boolean ce = Boolean.FALSE;
                boolean re = Boolean.FALSE;
                try {
                    if (!doc.getDocTipo().getCi().isEmpty()) {//Inscripción
                        ci = Boolean.TRUE;
                    }
                } catch (NullPointerException ex) {
                    ci = Boolean.FALSE;
                }

                try {
                    if (!doc.getDocTipo().getCe().isEmpty()) {//Externa
                        ce = Boolean.TRUE;
                    }
                } catch (NullPointerException ex) {
                    ce = Boolean.FALSE;
                }

                try {
                    if (!doc.getDocTipo().getRe().isEmpty()) {//Record
                        re = Boolean.TRUE;
                    }
                } catch (NullPointerException ex) {
                    re = Boolean.FALSE;
                }

                view.showModels(documents, ci, ce, re);
                view.setLoadingIndicator(false);

            }

            @Override
            public void onUnauthorized() {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                view.showMessage(context.getString(R.string.error_autho));
                view.showUnauthorized(true);
            }

            @Override
            public void onForbidden() {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                view.showMessage(context.getString(R.string.error_active_body));
            }

            @Override
            public void onUnprocessableEntity() {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                view.showMessage(context.getString(R.string.error_active_body));
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                view.showMessage(context.getString(R.string.error_occurred));
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                view.showMessage(context.getString(R.string.error_many));
            }

            @Override
            public void onNetworkError() {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                view.showMessage(context.getString(R.string.no_connection));
            }
        });
    }

    @Override
    public void docGenerate(@NonNull DocumentGenerate generate) {
        Log.d("Document Presnerter", "docGenerate: ");
        if (!Networks.isOnline(null)) {
            view.showNetworkErrorGenerate();
            return;
        }
        view.dialogProgress(true);
        genDocInt.execute(generate, new DocumentContract.GenerateDocumentInteractor.Callback() {
            @Override
            public void onSuccess(FileDoc doc) {
                if (!view.isActive()) {
                    return;
                }
                view.onSuccessGenerate(doc);
                view.dialogProgress(false);
            }

            @Override
            public void onUnauthorized() {
                if (!view.isActive()) {
                    return;
                }
                view.dialogProgress(false);
                view.showUnauthorizedGenerate();
            }

            @Override
            public void onForbidden() {
                if (!view.isActive()) {
                    return;
                }
                view.dialogProgress(false);
                view.showForbiddenGenerate();
            }

            @Override
            public void onUnprocessableEntity() {
                if (!view.isActive()) {
                    return;
                }
                view.dialogProgress(false);
                view.showUnprocessableEntityGenerate();
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.dialogProgress(false);
                view.showErrorOcurredGenerate();
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.dialogProgress(false);
                view.showErrorNotSolveGenerate();
            }

            @Override
            public void onNetworkError() {
                if (!view.isActive()) {
                    return;
                }
                view.dialogProgress(false);
                view.showNetworkErrorGenerate();
            }
        });
    }
}