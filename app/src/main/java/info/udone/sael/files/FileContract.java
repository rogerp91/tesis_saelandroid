package info.udone.sael.files;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.List;

/**
 * Created by AndrewX on 22/11/2016.
 */

public interface FileContract {

    interface View {

        void setTitle();

        void showModels(List<File> files);

        void showNoFiles(final boolean active);// No hay

        void setLoadingIndicator(boolean active);// Indicador

        void showMessage(String message);

        void showProgress(boolean active);

        void showErrorOcurred(boolean active);

        boolean isActive();

        void noPermission(boolean active);
    }

    interface Presenter {

        void setView(@NonNull FileContract.View view);

        void start();

        void loadModels(boolean showLoadingUI);

        void showNoPermission(boolean active);

    }

    /**
     * TODO: Interactor
     */
    interface FileInteractor {

        interface Callback {

            void onLoaded(List<File> files);

            void onDataNotAvailable();

            void onNotPermission();
        }

        void execute(@NonNull Callback callBackAnt);

    }

}