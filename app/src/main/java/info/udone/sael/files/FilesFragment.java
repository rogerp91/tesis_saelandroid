package info.udone.sael.files;

import android.Manifest;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.roger91.mlprogress.MlProgress;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import info.udone.sael.R;
import info.udone.sael.ui.adapte.FilesAdapte;
import info.udone.sael.ui.fragment.BaseFragment;
import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;

public class FilesFragment extends BaseFragment implements FileContract.View {

    private static String TAG = FilesFragment.class.getSimpleName();

    public static FilesFragment newInstance() {
        return new FilesFragment();
    }

    @BindView(R.id.progress)
    MlProgress mProgressView;
    @BindView(R.id.layout_message)
    LinearLayout layout_message;
    @BindView(R.id.text_message)
    TextView text_message;
    @BindView(R.id.recycler_list)
    RecyclerView recycler;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.layout_message_permission)
    LinearLayout layout_message_permission;

    @Inject
    FileContract.Presenter presenter;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_generic;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        presenter.setView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PermissionGen.with(getActivity()).addRequestCode(99).permissions(Manifest.permission.READ_EXTERNAL_STORAGE).request();


        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        recycler.setLayoutManager(mLayoutManager);
        recycler.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recycler.setItemAnimator(new DefaultItemAnimator());


        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.loadModels(false));
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void setTitle() {
        ab.setTitle("Descargas");
    }

    @Override
    public void showModels(List<File> files) {
        FilesAdapte adapte = new FilesAdapte(getActivity().getApplicationContext(), files, selectFile);
        adapte.setHasStableIds(true);
        recycler.setAdapter(adapte);

    }

    @Override
    public void showNoFiles(boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_files));
    }


    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showProgress(boolean active) {
        showOrNotView(active, mProgressView);
    }

    @Override
    public void showErrorOcurred(boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_occurred));
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void noPermission(boolean active) {
        showOrNotView(active, layout_message_permission);
    }

    public interface SelectFileListener {

        void selectFile(File file);

        void shareFile(Uri uri);

    }

    SelectFileListener selectFile = new SelectFileListener() {
        @Override
        public void selectFile(File file) {
            getActivity().runOnUiThread(() -> {
                Uri filepath = Uri.fromFile(file);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(filepath, "application/pdf");
                try {
                    startActivity(intent);
                } catch (Exception e) {
                    showMessage("Asegúrese de tener instalado un lector de PDF");
                    Log.e("error", "" + e);
                }
            });
        }

        @Override
        public void shareFile(Uri uri) {
            getActivity().runOnUiThread(() -> {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("application/pdf");
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                try {
                    startActivity(Intent.createChooser(intent, "Share PDF file"));
                } catch (Exception e) {
                    showMessage("No se puede abrir o compartir el documento PDF.");
                }
            });
        }
    };

    @OnClick(R.id.text_try_again)
    void onClickTryAgain() {
        presenter.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @PermissionFail(requestCode = 99)
    public void failPermission() {
        Log.d(TAG, "failPermission: ");
    }

    @OnClick(R.id.open_file)
    void clickNoPermission() {
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + getActivity().getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(i);
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}