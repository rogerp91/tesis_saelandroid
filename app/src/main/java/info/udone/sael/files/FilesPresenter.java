package info.udone.sael.files;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 09/08/2016.
 */

public class FilesPresenter implements FileContract.Presenter {

    private FileContract.View view;
    private FileContract.FileInteractor interactor;

    @Inject
    public FilesPresenter(FileContract.FileInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void setView(@NonNull FileContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
        view.setTitle();
    }

    @Override
    public void start() {
        loadModels(true);
    }

    @Override
    public void loadModels(boolean showLoadingUI) {
        if (showLoadingUI) {
            onProgress();
        } else {
            onIndicator();
        }
    }

    @Override
    public void showNoPermission(boolean active) {
        view.showProgress(false);
        view.showErrorOcurred(false);
        view.showNoFiles(false);
        view.noPermission(active);
    }

    private void onProgress() {
        view.noPermission(false);
        view.showProgress(true);
        view.showErrorOcurred(false);
        view.showNoFiles(false);
        interactor.execute(new FileContract.FileInteractor.Callback() {
            @Override
            public void onLoaded(List<File> files) {
                if (!view.isActive()) {
                    return;
                }

                view.showProgress(false);
                view.showModels(files);
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }

                view.showProgress(false);
                view.showNoFiles(true);
            }

            @Override
            public void onNotPermission() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.noPermission(true);
            }
        });
    }

    private void onIndicator() {
        interactor.execute(new FileContract.FileInteractor.Callback() {
            @Override
            public void onLoaded(List<File> files) {
                if (!view.isActive()) {
                    return;
                }

                view.setLoadingIndicator(false);
                view.showModels(files);
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }

                view.setLoadingIndicator(false);
                view.showMessage("No hay archivos disponibles");
            }

            @Override
            public void onNotPermission() {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                view.noPermission(true);
            }
        });
    }

}