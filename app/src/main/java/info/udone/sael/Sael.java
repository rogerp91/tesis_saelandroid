package info.udone.sael;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.github.rogerp91.pref.SP;
import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;

import java.util.List;

import dagger.ObjectGraph;
import info.udone.sael.di.AppModules;
import info.udone.sael.util.Constants;


/**
 * Created by roger on 16/11/16.
 */

public class Sael extends Application {

    private static Sael instance;
    private ObjectGraph objectGraph;
    private static Storage storage;

    public synchronized static Sael getInstance() {
        return instance;
    }

    public synchronized static Context getContext() {
        return instance.getApplicationContext();
    }

    public static Storage getInstanceStorage() {
        if (storage == null) {
            if (SimpleStorage.isExternalStorageWritable()) {
                storage = SimpleStorage.getExternalStorage();
            } else {
                storage = SimpleStorage.getInternalStorage(getContext());
            }
        }
        return storage;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        getInstanceStorage().createDirectory(Constants.PATH_BASE);
        initDependencyInjection();
        new SP.Builder().setContext(this).setMode(ContextWrapper.MODE_PRIVATE).setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true).build();

    }

    public ObjectGraph buildGraphWithAditionalModules(List<Object> modules) {
        if (modules == null) {
            throw new IllegalArgumentException("You can't plus a null module, review your getModules() implementation");
        }
        return objectGraph.plus(modules.toArray());
    }

    public void initDependencyInjection() {
        objectGraph = ObjectGraph.create(new AppModules(this));
        objectGraph.inject(this);
        objectGraph.injectStatics();
    }

    public static String getVersion() {
        String appVersion;
        try {
            PackageInfo pinfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            appVersion = pinfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            appVersion = "App version unknown";
        }
        if (appVersion.trim().length() == 0) {
            appVersion = "App version unknown";
        }
        return appVersion;
    }

}
