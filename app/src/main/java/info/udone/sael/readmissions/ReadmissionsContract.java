package info.udone.sael.readmissions;

import android.provider.BaseColumns;

import info.udone.sael.domain.entity.Readmissions;
import info.udone.sael.util.BasePresenter;
import info.udone.sael.util.BaseView;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public interface ReadmissionsContract {

    // TODO: View
    interface View extends BaseView<Readmissions> {

    }

    // TODO: Presentador
    interface Presenter extends BasePresenter<View> {

    }

    abstract class ReadmissionsEntry implements BaseColumns {
        public static final String TABLE_NAME = "Readmissions";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_ANO = "ANO";
        public static final String COLUMN_NAME_PER = "PER";
        public static final String COLUMN_NAME_CEDULA = "CEDULA";
        public static final String COLUMN_NAME_CREADO = "CREADO";
        public static final String COLUMN_NAME_OFICIO = "OFICIO";
        public static final String COLUMN_NAME_FECHA_OFICIO = "FECHA_OFICIO";
        public static final String COLUMN_NAME_CARRERA_NUEVA = "CARRERA_NUEVA";
        public static final String COLUMN_NAME_COMENTARIO = "COMENTARIO";
        public static final String COLUMN_NAME_EGRESADO = "EGRESADO";
        public static final String COLUMN_NAME_APROBADO = "APROBADO";
    }

}