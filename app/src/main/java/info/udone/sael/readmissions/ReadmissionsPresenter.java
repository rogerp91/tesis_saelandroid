package info.udone.sael.readmissions;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Readmissions;
import info.udone.sael.domain.source.ReadmissionsDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public class ReadmissionsPresenter implements ReadmissionsContract.Presenter {
    
    private ReadmissionsDataSource repository;
    private ReadmissionsContract.View view;

    private Context context;

    @Inject
    ReadmissionsPresenter(@Named("re_readmission") ReadmissionsDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }
    
    @Override
    public void setView(@NonNull ReadmissionsContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
        view.setTitle();
    }
    
    @Override
    public void start() {
        loadModels(true);
    }
    
    @Override
    public void loadModels(boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onProgress();
        } else {
            onIndicator();
        }
    }
    
    private void onProgress() {
        view.showProgress(true);
        view.showErrorNotSolve(false);
        view.showNoModels(false);
        view.showErrorOcurred(false);
        repository.getLoadReadmissions(new ReadmissionsDataSource.LoadReadmissionsCallback() {
            @Override
            public void onLoaded(List<Readmissions> readmissionses) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (readmissionses.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(readmissionses);
                }
            }
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNoModels(true);
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorOcurred(true);
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorNotSolve(true);
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNetworkError(true);
            }
        });
    }
    
    private void onIndicator() {
        repository.refleshLoadReadmissions(new ReadmissionsDataSource.LoadReadmissionsCallback() {
            @Override
            public void onLoaded(List<Readmissions> readmissionses) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (readmissionses.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(readmissionses);
                }
            }
            
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.no_data_available_appeals));
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.error_occurred));
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.error_many));
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.no_connection));
            }
        });
    }
    
    private void showMessge(String msg) {
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }
    
    @Override
    public void deleteAllModels() {
        repository.deleteAllLoadReadmissions();
    }
}
