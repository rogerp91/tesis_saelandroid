package info.udone.sael.professors;

import android.provider.BaseColumns;

import info.udone.sael.domain.entity.Professors;
import info.udone.sael.util.BasePresenter;
import info.udone.sael.util.BaseView;

/**
 * Created by Roger Patiño on 11/08/2016.
 */

public interface ProfessorsContract {


    // TODO: View
    interface View extends BaseView<Professors> {

    }

    // TODO: Presentador
    interface Presenter extends BasePresenter<View> {

    }

    abstract class ProfessorsEntry implements BaseColumns {
        public static final String TABLE_NAME = "Professors";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_CEDULA = "CEDULA";
        public static final String COLUMN_NAME_TIT = "TIT";
        public static final String COLUMN_NAME_NOMBRES = "NOMBRES";
    }


}