package info.udone.sael.pensum;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.udone.sael.R;
import info.udone.sael.ui.activity.BaseActivity;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Devices;


public class PensumActivity extends BaseActivity {

    private static String TAG = PensumActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public ActionBar ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pensum);
        ButterKnife.bind(this);
        try {
            if (Devices.isLollipop()) setTransition(getIntent());
        } catch (NullPointerException e) {
            Log.e(TAG, "onCreate: " + e.getMessage());
        }
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        ab = getSupportActionBar();
        ab.setTitle(getString(R.string.materias));
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            finish();
            return;
        }
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_content, PensumFragment
                            .newInstance(bundle.getString(Constants.PEMSUN), bundle.getString(Constants.CODE)))
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).commit();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }
}