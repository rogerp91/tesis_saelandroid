package info.udone.sael.pensum;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Materias;
import info.udone.sael.domain.source.PensumDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class PensumPresenter implements PensumContract.Presenter {

    private PensumDataSource repository;
    private PensumContract.View view;

    private Context context;
    
    @Inject
    PensumPresenter(@Named("re_pensum") PensumDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }

    @Override
    public void setView(@NonNull PensumContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
    }

    @Override
    public void start(@NonNull String pensum, @NonNull String code) {
        loadModels(pensum, code, true);
    }

    @Override
    public void loadModels(@NonNull String pensum, @NonNull String code, boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onProgress(pensum, code);
        } else {
            onIndicator(pensum, code);
        }
    }

    private void onProgress(@NonNull String pensum, @NonNull String code) {
        view.showProgress(true);
        view.showErrorNotSolve(false);
        view.showNoModels(false);
        view.showErrorOcurred(false);
        repository.getPensums(pensum, code, new PensumDataSource.LoadPensumCallback() {
            @Override
            public void onLoaded(List<Materias> materiases) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (materiases.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(materiases);
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNoModels(true);
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorOcurred(true);
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorNotSolve(true);
            }

            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNetworkError(true);
            }
        });
    }

    private void onIndicator(@NonNull String pensum, @NonNull String code) {
        repository.refleshPensum(pensum, code, new PensumDataSource.LoadPensumCallback() {
            @Override
            public void onLoaded(List<Materias> materiases) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (materiases.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(materiases);
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.no_data_available_note));
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.error_occurred));
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.error_many));
            }

            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.no_connection));
            }
        });
    }

    private void showMessage(String msg){
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }
}