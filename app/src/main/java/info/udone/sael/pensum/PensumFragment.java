package info.udone.sael.pensum;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.roger91.mlprogress.MlProgress;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Materias;
import info.udone.sael.ui.adapte.PensumAdapte;
import info.udone.sael.ui.fragment.BaseFragment;


public class PensumFragment extends BaseFragment implements PensumContract.View, SearchView.OnQueryTextListener {

    private String TAG = PensumFragment.class.getSimpleName();

    private static final String ARG_CODE = "code";
    private static final String ARG_PENSUM = "pensum";

    private String mParamCode;
    private String mParamPensum;

    public static PensumFragment newInstance(String pensum, String code) {
        PensumFragment fragment = new PensumFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PENSUM, pensum);
        args.putString(ARG_CODE, code);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.progress)
    MlProgress mProgressView;
    @BindView(R.id.layout_message)
    LinearLayout layout_message;
    @BindView(R.id.text_message)
    TextView text_message;
    @BindView(R.id.recycler_list)
    RecyclerView recycler;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private PensumAdapte adapte;
//    private NewPensumAdapte adapte2;

    private List<Materias> materiases;

    @Inject
    PensumContract.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamPensum = getArguments().getString(ARG_PENSUM);
            mParamCode = getArguments().getString(ARG_CODE);
        }
    }

    @Override
    public int getLayoutView() {
        return R.layout.fragment_generic;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        presenter.setView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler.setHasFixedSize(true);
        LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.loadModels(mParamPensum, mParamCode, false));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start(mParamPensum, mParamCode);
    }

    @Override
    public void setTitle() {

    }

    @Override
    public void showProgress(final boolean active) {
        showOrNotView(active, mProgressView);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showModels(List<Materias> objects) {
        materiases = objects;
        RecyclerViewExpandableItemManager expMgr = new RecyclerViewExpandableItemManager(null);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapte = new PensumAdapte(objects);
//        recycler.setAdapter(adapte2);
        adapte.notifyDataSetChanged();
        recycler.setAdapter(expMgr.createWrappedAdapter(adapte));
        ((SimpleItemAnimator) recycler.getItemAnimator()).setSupportsChangeAnimations(false);
        expMgr.attachRecyclerView(recycler);

    }

    @Override
    public void showNoModels(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_data_available_pensum));
    }

    @Override
    public void showNetworkError(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_connection));
    }

    @Override
    public void showErrorOcurred(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_occurred));
    }

    @Override
    public void showErrorNotSolve(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_many));
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @OnClick(R.id.text_try_again)
    void onClickTryAgain() {
        presenter.start(mParamPensum, mParamCode);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_pensum, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("TAG", "onOptionsItemSelected: " + Integer.toString(item.getItemId()));
        adapte.setList(materiases);
        switch (item.getItemId()) {
            case R.id.action_semestre_all:
                selectSemester("all");
                break;
            case R.id.action_semestre_1:
                selectSemester("01");
                break;
            case R.id.action_semestre_2:
                selectSemester("02");
                break;
            case R.id.action_semestre_3:
                selectSemester("03");
                break;
            case R.id.action_semestre_4:
                selectSemester("04");
                break;
            case R.id.action_semestre_5:
                selectSemester("05");
                break;
            case R.id.action_semestre_6:
                selectSemester("06");
                break;
            case R.id.action_semestre_7:
                selectSemester("07");
                break;
            case R.id.action_semestre_8:
                selectSemester("08");
                break;
            case R.id.action_semestre_9:
                selectSemester("09");
                break;
            case R.id.action_semestre_10:
                selectSemester("10");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (materiases != null) {
            final List<Materias> filteredModelList = filter(materiases, newText);
            adapte.animateTo(filteredModelList);
            recycler.scrollToPosition(0);
            return false;
        } else {
            showMessage("No hay materias disponibles");
        }
        return true;
    }

    private List<Materias> filter(List<Materias> models, String query) {
        query = query.toLowerCase();
        final List<Materias> filteredModelList = new ArrayList<>();
        for (Materias model : models) {
            final String text = model.getNOMBRE().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private void selectSemester(String semester) {
        if (materiases != null) {
            adapte.filterList(semester);
        } else {
            showMessage("No hay materias disponibles");
        }
    }

}