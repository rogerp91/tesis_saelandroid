package info.udone.sael.note;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Note;
import info.udone.sael.domain.source.NoteDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public class NotePresenter implements NoteContract.Presenter {
    
    private NoteContract.View view;
    private NoteDataSource repository;

    private Context context;

    @Inject
    NotePresenter(@Named("re_note") NoteDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }
    
    @Override
    public void setView(@NonNull NoteContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
        view.setTitle();
    }
    
    @Override
    public void start() {
        loadModels(true);
    }
    
    @Override
    public void loadModels(boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onProgress();
        } else {
            onIndicator();
        }
    }
    
    private void onProgress() {
        view.showProgress(true);
        view.showErrorNotSolve(false);
        view.showNoModels(false);
        view.showErrorOcurred(false);
        repository.getNotes(new NoteDataSource.LoadNoteCallback() {
            @Override
            public void onLoaded(List<Note> notes) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (notes.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(notes);
                }
            }
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNoModels(true);
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorOcurred(true);
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorNotSolve(true);
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNetworkError(true);
            }
        });
    }
    
    private void onIndicator() {
        repository.refleshNotes(new NoteDataSource.LoadNoteCallback() {
            @Override
            public void onLoaded(List<Note> notes) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (notes.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(notes);
                }
            }
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.no_data_available_note));
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.error_occurred));
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.error_many));
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) return;
                showMessge(context.getString(R.string.no_connection));
            }
        });
    }
    
    private void showMessge(String msg) {
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }
    
    @Override
    public void deleteAllModels() {
        
    }
    
    
}