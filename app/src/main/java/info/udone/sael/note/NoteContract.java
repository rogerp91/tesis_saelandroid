package info.udone.sael.note;

import android.provider.BaseColumns;

import info.udone.sael.domain.entity.Note;
import info.udone.sael.util.BasePresenter;
import info.udone.sael.util.BaseView;

/**
 * Created by Roger Patiño on 03/08/2016.
 */

public interface NoteContract {

    // TODO: View
    interface View extends BaseView<Note> {

    }

    // TODO: Presentador
    interface Presenter extends BasePresenter<View> {

    }

    abstract class NotesEntry implements BaseColumns {
        public static final String TABLE_NAME = "Note";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NOMBRE = "NOMBRE";
        public static final String COLUMN_NAME_CODIGO = "CODIGO";
        public static final String COLUMN_NAME_ULT_ANO = "ULT_ANO";
        public static final String COLUMN_NAME_ULT_PER = "ULT_PER";
        public static final String COLUMN_NAME_ULT_NOTA = "ULT_NOTA";
        public static final String COLUMN_NAME_INSCRITA = "INSCRITA";
        public static final String COLUMN_NAME_RETIRADA = "RETIRADA";
        public static final String COLUMN_NAME_REPROBADA = "REPROBADA";
        public static final String COLUMN_NAME_NOREPORTADA = "NOREPORTADA";
        public static final String COLUMN_NAME_APROBADA = "APROBADA";
        public static final String COLUMN_NAME_EJECUCION = "EJECUCION";
        public static final String COLUMN_NAME_OTRA = "OTRA";
    }

}