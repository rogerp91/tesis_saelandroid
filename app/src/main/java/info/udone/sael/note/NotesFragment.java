package info.udone.sael.note;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.roger91.mlprogress.MlProgress;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import info.udone.sael.R;
import info.udone.sael.domain.entity.Note;
import info.udone.sael.ui.adapte.NoteAdapte;
import info.udone.sael.ui.fragment.BaseFragment;


public class NotesFragment extends BaseFragment implements NoteContract.View, SearchView.OnQueryTextListener {

    public static NotesFragment newInstance() {
        return new NotesFragment();
    }

    @BindView(R.id.progress)
    MlProgress mProgressView;
    @BindView(R.id.layout_message)
    LinearLayout layout_message;
    @BindView(R.id.text_message)
    TextView text_message;
    @BindView(R.id.recycler_list)
    RecyclerView recycler;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private NoteAdapte adapte;
    private List<Note> notes;

    @Inject
    NoteContract.Presenter presenter;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_generic;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        presenter.setView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler.setHasFixedSize(true);
        LinearLayoutManager linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.loadModels(false));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void setTitle() {
        ab.setTitle("Notas");
    }

    @Override
    public void showProgress(final boolean active) {
        showOrNotView(active, mProgressView);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
        srl.post(() -> srl.setRefreshing(active));
    }

    @Override
    public void showModels(List<Note> objects) {
        notes = objects;
        adapte = new NoteAdapte(objects);
        adapte.setHasStableIds(true);
        recycler.setAdapter(adapte);
    }

    @Override
    public void showNoModels(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_data_available_note));
    }

    @Override
    public void showNetworkError(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.no_connection));
    }

    @Override
    public void showErrorOcurred(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_occurred));
    }

    @Override
    public void showErrorNotSolve(final boolean active) {
        showOrNotView(active, layout_message);
        text_message.setText(getString(R.string.error_many));
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @OnClick(R.id.text_try_again)
    void onClickTryAgain() {
        presenter.start();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_pensum, menu);
        final MenuItem item_sort = menu.findItem(R.id.action_sort);
        item_sort.setVisible(false);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (notes != null) {
            final List<Note> filteredModelList = filter(notes, newText);
            adapte.animateTo(filteredModelList);
            recycler.scrollToPosition(0);
            return false;
        } else {
            showMessage("No hay materias disponibles");
        }
        return true;
    }

    private List<Note> filter(List<Note> models, String query) {
        query = query.toLowerCase();
        final List<Note> filteredModelList = new ArrayList<>();
        for (Note model : models) {
            final String text = model.getNOMBRE().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

}