package info.udone.sael.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.github.rogerp91.pref.SP;

import info.udone.sael.R;
import info.udone.sael.di.ClientModule;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.domain.entity.ProvideClient;
import info.udone.sael.domain.source.remote.HttpRestClient;
import info.udone.sael.ui.activity.MainActivity;
import info.udone.sael.util.ClientUtils;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Mapper;
import info.udone.sael.util.Networks;
import info.udone.sael.util.Notifications;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationService extends Service {

    private static final String ACTION_SUCCESS = "info.udone.sael.service.action.SUCCESS";
    private static final String ACTION_PUSH = "info.udone.sael.service.action.SUCCESS";
    public static final String ACTION_ERROR = "info.udone.sael.service.action.ERROR";
    public static final String ACTION_NOT_CONNECTION = "info.udone.sael.service.action.NOT_CONNECTION";
    public static final String ACTION_ERROR_OCURRED = "info.udone.sael.service.action.ERROR_OCURRED";
    public static final String ACTION_DISMISS = "info.udone.sael.service.action.dismiss";
    public static final String ACTION_EMAIL = "info.udone.sael.service.action.email";

    private static final String EXTRA_NOTIFICATION = "info.udone.sael.service.extra.NOTIFICATION";//Parametro
    public static final String EXTRA_ERROR_CODE = "info.udone.sael.service.extra.ERROR_CODE";//Parametro

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;

    public static void actionStart(Context context, Notification notification) {
        Intent intent = new Intent(context, NotificationService.class);
        intent.setAction(ACTION_PUSH);
        intent.putExtra(EXTRA_NOTIFICATION, notification);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PUSH.equals(action)) {
                final Notification notification = intent.getExtras().getParcelable(EXTRA_NOTIFICATION);
                handleActionPush(notification);
            }
        }
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void handleActionPush(Notification notification) {
        if (Networks.isOnline(this)) {
            sendNotification(notification);
        } else {
            onPushErrorNetwork();
        }

    }

    private void sendNotification(Notification notification) {
        ClientModule clientModule = new ClientModule();
        HttpRestClient retrofitInterface = ClientUtils.getRestClient(new ProvideClient(clientModule.provideOkHttpClient(clientModule.provideInterceptor()), clientModule.provideGson()), HttpRestClient.BASE_URL);
        Call<Void> call = retrofitInterface.pushNotification((HttpRestClient.PREFIX + SP.getString(Constants.TOKEN, "")), Mapper.getNotification(notification));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    stopSelf();
                } else {
                    if (response.code() == 422) {
                        onPushErrorUnprocessable();
                    } else {
                        if (response.code() == 404) {
                            onPushErrorNotFound();
                        } else {
                            onPushMany(response.code(), response.raw().toString());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                onPushError();
            }
        });
    }

    private void onPushSuccess() {
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Notificaciones Sael")
                .setContentText("Se envió la notificación exitosamente")
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());
        stopSelf();
    }

    // Error del rest
    private void onPushMany(int code, String raw) {

        Intent emailIntent = new Intent(this, MainActivity.class);
        emailIntent.setAction(ACTION_EMAIL);
        emailIntent.putExtra(EXTRA_ERROR_CODE, raw);
        PendingIntent emailSnooze = PendingIntent.getActivity(this, 0, emailIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(Notifications.getNotificationError())
                .setContentTitle("Notificaciones Sael")
                .setStyle(new NotificationCompat.BigTextStyle().bigText("Ocurrió un error inesperado, código " + code + " comuníquese con soporte urgentemente"))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true)
                .addAction(R.drawable.ic_email, getString(R.string.notificar), emailSnooze);

        notificationManager.notify(0, notificationBuilder.build());
        stopSelf();
    }

    //Error
    private void onPushError() {
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(Notifications.getNotificationError())
                .setContentTitle("Notificaciones Sael")
                .setContentText("Lo sentimos. Ocurrió un error inesperado")
                .setSubText("Por favor, envié de nuevo la notificación")
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());
        stopSelf();
    }

    //Error de conexión
    private void onPushErrorNetwork() {
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(Notifications.getNotificationError())
                .setContentTitle("Notificaciones Sael")
                .setContentText("Lo sentimos. No hay conexión a internet")
                .setSubText("Por favor, envié de nuevo la notificación")
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());
        stopSelf();
    }

    //Error de conexión
    private void onPushErrorUnprocessable() {
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(Notifications.getNotificationError())
                .setContentTitle("Notificaciones Sael")
                .setStyle(new NotificationCompat
                        .BigTextStyle()
                        .bigText("Ocurrió un error con los valores de las notificación son invadidos"))
                .setSubText("Por favor, envié de nuevo la notificación")
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());
        stopSelf();
    }

    private void onPushErrorNotFound() {
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(Notifications.getNotificationError())
                .setContentTitle("Notificaciones Sael")
                .setStyle(new NotificationCompat
                        .BigTextStyle()
                        .bigText("Ocurrió un error, verifique los numeros de cedula o los códigos de carrera"))
                .setSubText("Por favor, envié de nuevo la notificación")
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());
        stopSelf();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }

//    private void getHostFirebase(Notification notification) {
//        ClientModule clientModule = new ClientModule();
//        Retrofit retrofit = clientModule.provideRetrofitFirebase(clientModule.provideGson(), clientModule.provideOkHttpClient(clientModule.provideInterceptor()));
//        HttpRestClientFirebase firebase = retrofit.create(HttpRestClientFirebase.class);
//        Call<Host> call = firebase.getHost();
//        call.enqueue(new Callback<Host>() {
//            @Override
//            public void onResponse(Call<Host> call, Response<Host> response) {
//                if (!response.isSuccessful()) {
//                    onPushMany(response.code(), response.raw().toString());
//                } else {
//                    init(notification, response.body().getHost());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Host> call, Throwable t) {
//                ReportLog.send(t.getMessage());
//                onPushError();
//            }
//        });
//
//    }


}
