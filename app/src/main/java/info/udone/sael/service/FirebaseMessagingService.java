package info.udone.sael.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.github.rogerp91.pref.SP;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import info.udone.sael.R;
import info.udone.sael.domain.entity.Notification;
import info.udone.sael.domain.source.NotificationDataSource;
import info.udone.sael.domain.source.local.NotificationLocalDataSource;
import info.udone.sael.ui.activity.MainActivity;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Devices;
import info.udone.sael.util.Notifications;

import static info.udone.sael.util.Constants.FROM_NOTIFICATION;
import static info.udone.sael.util.Constants.SESSION;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private static final String TAG = FirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            try {
                Map<String, String> data = remoteMessage.getData();
                String title = remoteMessage.getNotification().getTitle();
                String body = remoteMessage.getNotification().getBody();
                String time = data.get("time");
                String from = data.get("from_to");
                if (Devices.isLollipop()) {
                    sendData2(title, body, time, from);
                } else {
                    sendData(title, body, time, from);
                }
            } catch (NullPointerException e) {
                Log.d(TAG, "onMessageReceived: " + e.getMessage());
            }
        }

    }

    private void sendData(String title, String body, String time, String from) {
        if (SP.getBoolean(SESSION)) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setAction(FROM_NOTIFICATION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Notification notification = new Notification();
            notification.setFroms(from);
            notification.setTitle(title);
            notification.setBody(body);
            notification.setTime(time);
            NotificationDataSource localDataSource = new NotificationLocalDataSource();
            localDataSource.saveNotification(notification);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(Notifications.getNotificationIcon())
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                    .setContentInfo(from)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            sendBroadcast(new Intent("UPDATE"));
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }

    private void sendData2(String title, String body, String time, String from) {
        if (SP.getBoolean(SESSION)) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setAction(FROM_NOTIFICATION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Notification notification = new Notification();
            notification.setFroms(from);
            notification.setTitle(title);
            notification.setBody(body);
            notification.setTime(time);
            NotificationDataSource localDataSource = new NotificationLocalDataSource();
            localDataSource.saveNotification(notification);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(Notifications.getNotificationIcon())
                    .setContentTitle(title)
                    .setContentText(body)
                    .setContentInfo(from)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            sendBroadcast(new Intent("UPDATE"));
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }


    private void changePassword() {
        if (SP.getBoolean(SESSION)) {
            //Intent intent = new Intent(this, ExampleActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
            sendBroadcast(new Intent(Constants.ACTION_CHANGE_PASSWORD));
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Cambio de contraseña")
                    .setContentText("Se ha detectado un cambio de contraseña desde el sitio Web de SAEL")
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);
            //.setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
            SP.putBoolean(Constants.ACTION_CHANGE_PASSWORD, true);
        }
    }
}