package info.udone.sael.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = FirebaseInstanceIDService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param fcm The new token.
     */
    private void sendRegistrationToServer(String fcm) {
        /**
        // TODO: Implement this method to send token to your app server.
        Prefs.getString(Constants.FCM, fcm);
        String token = Prefs.getString(Constants.TOKEN, "");
        Device device = new Device();
        device.setBoard(BOARD);
        device.setBrand(BRAND);
        device.setCpu_abi(CPU_ABI);
        device.setCpu_abi2(CPU_ABI2);
        device.setDevice(UUID.randomUUID().toString());
        device.setDisplay_devices(DISPLAY);
        device.setHardware(HARDWARE);
        device.setHardware(HOST);
        device.setManufacturer(MANUFACTURER);
        device.setModel(MODEL);
        device.setProduct(PRODUCT);
        device.setSerial(SERIAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            device.setSupported_abis(Arrays.toString(SUPPORTED_ABIS));
        }
        device.setVersion_cevice(Build.VERSION.RELEASE);
        device.setVersion_codes(Integer.toString(Build.VERSION.SDK_INT));

        Retrofit retrofit = Networks.getComponentRetrofit();
        HttpRestClient client = retrofit.create(HttpRestClient.class);
        Call<Void> call = client.postDevice(HttpRestClient.PREFIX + token, device);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, response.raw().toString());
                if (!response.isSuccessful()) {
                    if (response.code() == 401) {

                    } else {
                        if (response.code() == 403) {

                        } else {
                            if (response.code() == 400) {

                            } else {


                                if (response.code() == 409) {

                                }
                            }
                        }
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
         */
    }
}
