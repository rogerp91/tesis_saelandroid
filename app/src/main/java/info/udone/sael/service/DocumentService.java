package info.udone.sael.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.github.rogerp91.pref.SP;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.io.File;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Download;
import info.udone.sael.ui.activity.MainActivity;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Functions;
import info.udone.sael.util.Networks;
import info.udone.sael.util.ReportLog;

import static com.github.rogerp91.pref.SP.putBoolean;
import static info.udone.sael.util.Constants.FROM_NOTIFICATION;

public class DocumentService extends IntentService {

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;

    public static String ACTION_DOWNLOAD_ID = "info.udone.sael.action.id";
    public static String ACTION_DOWNLOAD_URL = "info.udone.sael.action.download";
    public static String ACTION_DOWNLOAD_ERROR = "info.udone.sael.action.download.error";
    public static String ACTION_DOWNLOAD_NETWORK = "info.udone.sael.action.download.network";
    public static String ACTION_DOWNLOAD_COMPLETE = "info.udone.sael.action.download.complete";

    private String url;
    private int id;

    public static String LOCK = "lock_resource";
    public static boolean CHECK_LOCK = false;

    public static boolean IS_ACTIVE = false;
    private String name;
    Download download;

    public DocumentService() {
        super("DocumentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        IS_ACTIVE = true;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent.getExtras() == null) {
            stopSelf();
        } else {
            url = intent.getExtras().getString(ACTION_DOWNLOAD_URL);
            id = intent.getExtras().getInt(ACTION_DOWNLOAD_ID);
        }
        if (!Sael.getInstanceStorage().isDirectoryExists(Constants.PATH_BASE)) {
            Sael.getInstanceStorage().createDirectory(Constants.PATH_BASE);
        }

        if (url.trim().length() == 0) {
            stopSelf();
            return;
        }
        createNotification(id);
        if (Networks.isOnline(this)) {
            putBoolean(LOCK, true);
            downloadFile();
        } else {
            onDownloadErrorNetwork();
            new Thread(() -> {
                Toast.makeText(getApplicationContext(), "Lo sentimos, no hay conexión a internet", Toast.LENGTH_SHORT).show();
            });
        }
    }

    private void createNotification(int name) {
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_file_download_white_24dp)
                .setContentTitle("Constancia " + name)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentText("Preparando para descargar...")
                .setAutoCancel(true)
                .setOngoing(true);
        notificationManager.notify(0, notificationBuilder.build());
    }

    private void downloadFile() {
        download = new Download();
        name = "CONS_" + id + ".pdf";
        Sael.getInstanceStorage().createFile(Constants.PATH_BASE, name, name);
        File outputFile = Sael.getInstanceStorage().getFile(Constants.PATH_BASE, name);
        Ion.with(getApplicationContext()).load(url)
                .progress((downloaded, total) -> sendNotification(total))
                .write(new File(outputFile.getPath()))
                .setCallback((e, file) -> {
                    if (e != null) {
                        ReportLog.send(e.getMessage());
                        CHECK_LOCK = false;
                        onDownloadError();
                    } else {
                        onDownloadComplete();
                    }
                });
    }


    private void sendNotification(long total) {
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setContentText("Descarga en curso");
        notificationBuilder.setContentInfo(Functions.bytesToHuman(total));
        notificationManager.notify(0, notificationBuilder.build());
    }


    private void onDownloadComplete() {
        SP.putBoolean(LOCK, CHECK_LOCK);

//        Intent intent = new Intent();
//        File outputFile = Sael.getInstanceStorage().getFile(Constants.PATH_BASE + "/");
//        Uri uri = Uri.parse(outputFile.getPath());
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        intent.setDataAndType(uri, "*/*");

        Intent intent = new Intent(this, MainActivity.class);
        intent.setAction(ACTION_DOWNLOAD_COMPLETE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
        notificationManager.cancel(0);
        notificationBuilder.setContentIntent(contentIntent);
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setContentText("Archivo descargado exitosamente");
        notificationBuilder.setOngoing(false);
        notificationManager.notify(0, notificationBuilder.build());
    }

    private void onDownloadError() {
        SP.putBoolean(LOCK, CHECK_LOCK);
        notificationManager.cancel(0);
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setOngoing(false);
        notificationBuilder.setContentText("Lo sentimos. Ocurrió un error inesperado");
        notificationBuilder.setSubText("Por favor, descargue el archivo de nuevo");
        notificationManager.notify(0, notificationBuilder.build());
    }

    private void onDownloadErrorNetwork() {
        SP.putBoolean(LOCK, CHECK_LOCK);
        notificationManager.notify(0, notificationBuilder.build());
        notificationManager.cancel(0);
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setOngoing(false);
        notificationBuilder.setContentText("Lo sentimos. No hay conexión a internet");
        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IS_ACTIVE = false;
        SP.putBoolean(LOCK, CHECK_LOCK);
    }
}