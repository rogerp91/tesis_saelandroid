package info.udone.sael.login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.roger91.mlprogress.MlProgress;
import com.google.android.gms.common.GoogleApiAvailability;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import info.udone.sael.R;
import info.udone.sael.ui.activity.MainActivity;
import info.udone.sael.ui.fragment.BaseFragment;
import info.udone.sael.util.ActivityWithOptions;

import static info.udone.sael.ui.activity.BaseActivity.REQUEST_GOOGLE_PLAY_SERVICES;
import static info.udone.sael.util.Constants.EXTRA_TRANSITION;
import static info.udone.sael.util.Constants.TRANSITION_FADE_SLOW;


public class LoginFragment extends BaseFragment implements LoginContract.View, Animation.AnimationListener {

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @BindView(R.id.input_cedula)
    TextInputEditText inputCedula;
    @BindView(R.id.input_password)
    TextInputEditText inputPassword;

    @BindView(R.id.input_layout_cedula)
    TextInputLayout inputLayoutCedula;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;

    @BindView(R.id.scroll_form)
    View mForm;
    @BindView(R.id.progress)
    MlProgress mProgress;

    @BindView(R.id.container_logo)
    View mContainerLogo;

    @BindView(R.id.logo)
    ImageView mLogo;

    @Inject
    LoginContract.Presenter presenter;

    @Override
    public int getLayoutView() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, View view) {
        presenter.setView(this);
        Animation animTranslate = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.translate);
        animTranslate.setFillAfter(true);
        mLogo.setAnimation(animTranslate);
        animTranslate.setAnimationListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inputCedula.addTextChangedListener(textWatcher);
        inputPassword.addTextChangedListener(textWatcher);

        inputPassword.setOnEditorActionListener((textView, i, keyEvent) -> {
            boolean check = false;
            if (i == EditorInfo.IME_ACTION_UNSPECIFIED) {
                presenter.attemptLogin(inputCedula.getText().toString(), inputPassword.getText().toString());
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                check = true;
            }
            return check;
        });
    }

    @Override
    public void onSuccess() {
        Intent intent = new Intent(getActivity(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(EXTRA_TRANSITION, TRANSITION_FADE_SLOW);
            ActivityWithOptions.startActivityWithOptions(intent, getActivity());
        } else {
            startActivity(intent);
            getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        getActivity().finish();
    }

    @Override
    public void showProgress(boolean active) {
        showOrNotViewMultiple(active, mForm, mProgress);
    }

    @Override
    public void showEmailError(String error) {
        shoeMessageError(error);
    }

    @Override
    public void showPasswordError(String error) {
        shoeMessageError(error);
    }

    @Override
    public void showUnauthorized() {
        shoeMessageError(getString(R.string.data_invalid));
    }

    @Override
    public void showForbidden() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.error_active_titile);
        builder.setIcon(ContextCompat.getDrawable(getActivity(), R.mipmap.ic_launcher));
        builder.setMessage(R.string.error_active_body);
        builder.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        }).setPositiveButton("Aceptar", (dialogInterface, i) -> {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://udone.info/sael/")));
        });
        builder.show();
    }

    @Override
    public void showNetworkError() {
        shoeMessageError(getString(R.string.no_connection));
    }

    @Override
    public void showErrorOcurred() {
        shoeMessageError(getString(R.string.error_occurred));
    }

    @Override
    public void showErrorNotSolve() {
        shoeMessageError(getString(R.string.error_many));
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showGooglePlayServicesDialog(int errorCode) {
        Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), errorCode, REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    @Override
    public void onGooglePlayServicesFailed() {
        Toast.makeText(getActivity(), "Se requiere Google Play Services para usar la app", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        showOrNotView(false, mContainerLogo);
        mForm.setAlpha(0f);
        showOrNotView(true, mForm);
        int mediumAnimTime = getActivity().getResources().getInteger(android.R.integer.config_mediumAnimTime);
        mForm.animate().alpha(1f).setDuration(mediumAnimTime).setListener(null);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            inputLayoutCedula.setErrorEnabled(false);
            inputLayoutPassword.setErrorEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResumen();
    }

    @OnClick({R.id.btn_sign_in, R.id.text_forgot_password, R.id.new_account})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in:
                presenter.attemptLogin(inputCedula.getText().toString(), inputPassword.getText().toString());
                break;
            case R.id.text_forgot_password:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://udone.info/sael/recuperar_password.php")));
                break;
            case R.id.new_account:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://udone.info/sael/crear_cuenta.php")));
                break;
        }
    }

}