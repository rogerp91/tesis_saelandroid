package info.udone.sael.login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.github.rogerp91.pref.SP;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import javax.inject.Inject;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.Device;
import info.udone.sael.domain.entity.LoginWithProfile;
import info.udone.sael.util.Constants;
import info.udone.sael.util.Networks;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 02/08/2016.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;
    private LoginContract.LoginInteractor interactor;

    private Context context;

    @Inject
    LoginPresenter(LoginContract.LoginInteractor interactor) {
        this.interactor = interactor;
        context = Sael.getContext();
    }

    @Override
    public void setView(@NonNull LoginContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
    }

    @Override
    public void attemptLogin(@NonNull String cedula, @NonNull String password) {
        boolean isValid = false;
        if (TextUtils.isEmpty(password)) {
            isValid = true;
            if (!view.isActive()) return;
            view.showPasswordError(context.getResources().getString(R.string.not_empty_password));
        } else {
            if (!(password.length() > 0)) {
                isValid = true;
                if (!view.isActive()) return;
                view.showPasswordError(context.getResources().getString(R.string.min_length_password));
            }
        }

        if (TextUtils.isEmpty(cedula)) {
            isValid = true;
            if (!view.isActive()) return;
            view.showEmailError(context.getResources().getString(R.string.min_length_cedula));
        } else {
            if (!(cedula.length() > 6)) {
                isValid = true;
                if (!view.isActive()) return;
                view.showPasswordError(context.getResources().getString(R.string.min_length_cedula));
            }
        }

        if (!isValid) {
            if (Networks.isOnline(null)) {
                view.showProgress(true);// Colocar el cargando
                attemptLoginIntector(cedula, password);
            } else {
                view.showProgress(false);// Quitar el cargando
                view.showNetworkError();
            }
        }

    }

    @Override
    public void onResumen() {
        int statusCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);

        if (GoogleApiAvailability.getInstance().isUserResolvableError(statusCode)) {
            view.showGooglePlayServicesDialog(statusCode);
        } else if (statusCode != ConnectionResult.SUCCESS) {
            view.onGooglePlayServicesFailed();
        }
    }

    private void attemptLoginIntector(String email, String password) {
        interactor.execute(email, password, new LoginContract.LoginInteractor.Callback() {

            @Override
            public void onSuccess(LoginWithProfile loginWithProfile, Device device) {
                if (!view.isActive()) return;
                SP.putBoolean(Constants.SESSION, true);
                SP.putString(Constants.FCM, device.getKey_fcm());
                SP.putString(Constants.TOKEN, loginWithProfile.getToken());
                SP.putString(Constants.ID, loginWithProfile.getProfile().getID());
                SP.putString(Constants.CEDULA, loginWithProfile.getProfile().getCEDULA());
                SP.putString(Constants.NOMBRE1, loginWithProfile.getProfile().getNOMBRE1());
                SP.putString(Constants.NOMBRE2, loginWithProfile.getProfile().getNOMBRE2());
                SP.putString(Constants.APELLIDO1, loginWithProfile.getProfile().getAPELLIDO1());
                SP.putString(Constants.APELLIDO2, loginWithProfile.getProfile().getAPELLIDO2());
                SP.putString(Constants.CORREO, loginWithProfile.getProfile().getCORREO());
                SP.putString(Constants.ANO_ING, loginWithProfile.getProfile().getANOING());
                SP.putString(Constants.ESTATUS, loginWithProfile.getProfile().getESTATUS());
                SP.putString(Constants.TITULO, loginWithProfile.getProfile().getTITULO());
                SP.putString(Constants.CODIGO, loginWithProfile.getProfile().getCODIGO());
                SP.putString(Constants.PROMEDIO, loginWithProfile.getProfile().getPROMEDIO());
                SP.putBoolean(Constants.ADMIN, loginWithProfile.getProfile().getADMIN_MOBILE());
                view.onSuccess();
            }

            @Override
            public void onUnauthorized() {
                if (!view.isActive()) return;
                view.showProgress(false);
                view.showUnauthorized();
            }

            @Override
            public void onForbidden() {
                if (!view.isActive()) return;
                view.showProgress(false);
                view.showForbidden();
            }

            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                view.showProgress(false);
                view.showErrorOcurred();
            }

            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                view.showProgress(false);
                view.showErrorNotSolve();
            }

        });
    }

}