package info.udone.sael.login;

import android.support.annotation.NonNull;

import info.udone.sael.domain.entity.Device;
import info.udone.sael.domain.entity.LoginWithProfile;

/**
 * Created by Roger Patiño on 07/07/2016.
 */

public interface LoginContract {

    /**
     * TODO: Vista ResponseLogin
     */
    interface View {

        void onSuccess();

        void showProgress(boolean active);

        void showEmailError(String error);

        void showPasswordError(String error);

        void showUnauthorized();

        void showForbidden();

        void showNetworkError();

        void showErrorOcurred();

        void showErrorNotSolve();

        boolean isActive();

        void showGooglePlayServicesDialog(int errorCode);

        void onGooglePlayServicesFailed();

    }

    /**
     * TODO: Presentador ResponseLogin
     */
    interface Presenter {

        void setView(@NonNull View view);

        void attemptLogin(@NonNull String email, @NonNull String password);

        void onResumen();

    }

    /**
     * TODO: Interactor ResponseLogin
     */
    interface LoginInteractor {

        interface Callback {

            void onSuccess(LoginWithProfile loginWithProfile, Device device);

            void onUnauthorized(); // 401

            void onForbidden(); // 403

            void onErrorOcurred(); // Error

            void onErrorNotSolve();// 500

        }

        void execute(@NonNull String cedula, @NonNull String password, @NonNull Callback callBackAnt);

    }

}