package info.udone.sael.school;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import info.udone.sael.R;
import info.udone.sael.Sael;
import info.udone.sael.domain.entity.School;
import info.udone.sael.domain.source.SchoolDataSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roger Patiño on 09/08/2016.
 */

public class SchoolPresenter implements SchoolContract.Presenter {
    
    private SchoolDataSource repository;
    
    private SchoolContract.View view;

    private Context context;
    
    @Inject
    SchoolPresenter(@Named("re_school") SchoolDataSource repository) {
        this.repository = repository;
        this.context = Sael.getContext();
    }
    
    @Override
    public void setView(@NonNull SchoolContract.View view) {
        checkNotNull(view, "View not null!");
        this.view = view;
        view.setTitle();
    }
    
    @Override
    public void start() {
        loadModels(true);
    }
    
    @Override
    public void loadModels(boolean showLoadingUI) {
        if (showLoadingUI) { // Colocar el cargando
            onProgress();
        } else {
            onIndicator();
        }
    }
    
    @Override
    public void deleteAllModels() {
        
    }
    
    private void onProgress() {
        view.showProgress(true);
        view.showErrorNotSolve(false);
        view.showNoModels(false);
        view.showErrorOcurred(false);
        repository.getSchool(new SchoolDataSource.LoadSchoolCallback() {
            
            @Override
            public void onLoaded(List<School> schools) {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                if (schools.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(schools);
                }
            }
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNoModels(true);
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorOcurred(true);
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showErrorNotSolve(true);
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) {
                    return;
                }
                view.showProgress(false);
                view.showNetworkError(true);
            }
        });
    }
    
    private void onIndicator() {
        repository.refleshSchool(new SchoolDataSource.LoadSchoolCallback() {
            
            @Override
            public void onLoaded(List<School> schools) {
                if (!view.isActive()) {
                    return;
                }
                view.setLoadingIndicator(false);
                if (schools.isEmpty()) {
                    view.showNoModels(true);
                } else {
                    view.showModels(schools);
                }
            }
            
            @Override
            public void onDataNotAvailable() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.no_data_available_appeals));
            }
            
            @Override
            public void onErrorOcurred() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.error_occurred));
            }
            
            @Override
            public void onErrorNotSolve() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.error_many));
            }
            
            @Override
            public void onErrorNetwork() {
                if (!view.isActive()) return;
                showMessage(context.getString(R.string.no_connection));
            }
        });
    }
    
    private void showMessage(String msg) {
        if (!view.isActive()) {
            return;
        }
        view.setLoadingIndicator(false);
        view.showMessage(msg);
    }
    
}