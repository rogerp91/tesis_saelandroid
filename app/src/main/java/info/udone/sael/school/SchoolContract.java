package info.udone.sael.school;

import android.provider.BaseColumns;

import info.udone.sael.domain.entity.School;
import info.udone.sael.util.BasePresenter;
import info.udone.sael.util.BaseView;

/**
 * Created by Roger Patiño on 09/08/2016.
 */

public interface SchoolContract {

    // TODO: View
    interface View extends BaseView<School> {

    }

    // TODO: Presentador
    interface Presenter extends BasePresenter<View> {

    }

    static abstract class SchoolEntry implements BaseColumns {
        public static final String TABLE_NAME = "School";
        public static final String COLUMN_NAME_CODIGO = "CODIGO";
        public static final String COLUMN_NAME_DESCRIPTION = "DESCRIPCION";
        public static final String COLUMN_NAME_STATUS = "ESTATUS";
    }

}