# Sael #

Android - Code - Screenshot

------

# Descripción

Desarrollo de una API RESTful y aplicación móvil desarrollada en Android e iOS, que permitirá a los estudiantes de la UDO Nueva Esparta que tengan un dispositivo Android de cualquier fabricante (versión 4.1 ó superior) acceder a su cuenta del Sistema Académico En Línea (SAEL), en la cual podrán consultar su notas, materias, carrera, medidas, etc. También permitirá el envió de notificaciones push sobre los eventos que se puedan suscitar durante un determinado período académico.
Tecnologías Utilizadas: ButterKnife, Dagger, Retrofit, GSON, Picasso, MaterialDialogs y Firebase Google.

---

# Descripción

![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503364753/android-app-sael-notificacion_ywv1ux.png  | 100x100 )
![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503364752/android-app-sael-menu_fwtovt.png  | 100x100 )
![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503364752/android-app-sael-pensum_beys8q.png  | 100x100 )

```

# build.gradle app

apply plugin: 'com.android.application'
apply plugin: 'me.tatarka.retrolambda'

def getBuildDate() {
    def date = new Date()
    def formattedDate = date.format('ddMMyyyy')
    def formatInt = formattedDate as int;
    return (formatInt) as int
}

android {
    compileSdkVersion 25
    buildToolsVersion "25.0.2"

    //=========== GENERAL LANZAMIENTO
    def versionMajor = 4
    def versionMinor = 0
    def versionPatch = 0

    //=========== ALPHA
    def alphaVersionMajor = 2
    def alphaVersionMinor = 4
    def alphaVersionPatch = 0
    def versionRCAlpha = "${alphaVersionMajor}.${alphaVersionMinor}.${alphaVersionPatch}"

    //=========== BETA
    def betaVersionMajor = 0
    def betaVersionMinor = 0
    def betaVersionPatch = 0
    def versionRCBeta = "${betaVersionMajor}.${betaVersionMinor}.${betaVersionPatch}"

    def veName = "${versionMajor}.${versionMinor}.${versionPatch}-alpha.${alphaVersionMajor}+RC-${versionRCAlpha}"

    defaultConfig {
        applicationId "info.udone.sael"
        minSdkVersion 16
        targetSdkVersion 25
        versionCode versionMajor
        versionName veName
        renderscriptTargetApi 18
        renderscriptSupportModeEnabled true
    }
    signingConfigs {
        release {
            storeFile file('../keys/sael_key_store_not_lose')
            storePassword 'androidsaelmobile'
            keyAlias 'sael'
            keyPassword 'androidsaelmobile'
        }
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    buildTypes {
        debug {
            versionNameSuffix "-debug"
            buildConfigField 'String', 'HOST', '"http://udone.info"'
            buildConfigField 'String', 'FIREBAE_HOST', '"https://saelmobile-564d6.firebaseio.com/"'
            minifyEnabled false
            useProguard false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
            debuggable false
            buildConfigField 'String', 'LEVEL', '"1"'
        }
        release {
            minifyEnabled true
            buildConfigField 'String', 'HOST', '"http://udone.info"'
            buildConfigField 'String', 'FIREBAE_HOST', '"https://saelmobile-564d6.firebaseio.com/"'
            proguardFiles = buildTypes.debug.proguardFiles
            debuggable false
            minifyEnabled true
            buildConfigField 'String', 'LEVEL', '"2"'
//            signingConfig signingConfigs.release
        }
        localhost {
            minifyEnabled true
            versionNameSuffix "-localhost"
            buildConfigField 'String', 'HOST', '"http://10.0.3.3/sael-api-restful/"'
            proguardFiles = buildTypes.debug.proguardFiles
            debuggable true
            minifyEnabled false
            buildConfigField 'String', 'LEVEL', '"1"'
        }
        applicationVariants.all { variant ->
            variant.outputs.each { output ->
                project.ext { appName = veName }
                def outputDate = new Date().format('yyyyMMddHHmmss')
                def newName = output.outputFile.name
                newName = newName.replace("app-", "$project.ext.appName-")
                if (newName.startsWith("debug")) {
                    newName = newName.replace("-debug", "-debug" + outputDate)
                } else {
                    newName = newName.replace("-release", "-release" + outputDate)
                }
                output.outputFile = new File(output.outputFile.parent, newName)
            }
        }
    }
    packagingOptions {
        exclude 'META-INF/maven/com.squareup.retrofit/retrofit/pom.properties'
        exclude 'META-INF/maven/com.squareup.retrofit/retrofit/pom.xml'
        exclude 'META-INF/maven/com.squareup/javawriter/pom.properties'
        exclude 'META-INF/maven/com.squareup/javawriter/pom.xml'
        exclude 'META-INF/services/javax.annotation.processing.Processor'
        return true
    }
}

dependencies {
    compile fileTree(include: ['*.jar'], dir: 'libs')
    compile('com.h6ah4i.android.widget.advrecyclerview:advrecyclerview:0.10.0@aar') {
        transitive = true
    }
    compile 'com.jakewharton:butterknife:8.4.0'
    compile 'com.android.support:appcompat-v7:25.1.0'
    compile 'com.android.support:design:25.1.0'
    compile 'com.android.support:cardview-v7:25.1.0'
    compile 'com.android.support:recyclerview-v7:25.1.0'
    compile 'com.squareup.dagger:dagger:1.2.5'
    compile 'com.squareup.dagger:dagger-compiler:1.2.5'
    compile 'com.squareup.retrofit2:retrofit:2.1.0'
    compile 'com.squareup.retrofit2:converter-gson:2.0.0-beta4'
    compile 'com.squareup.picasso:picasso:2.5.2'
    compile 'com.squareup.okhttp3:logging-interceptor:3.4.1'
    compile 'com.github.rogerp91:MlProgress:0.0.3'
    compile 'com.github.rogerp91:ManagerSharedPreferences:0.0.1'
    compile 'com.koushikdutta.ion:ion:2.1.6'
    compile 'de.hdodenhof:circleimageview:2.1.0'
    compile 'com.github.traex.rippleeffect:library:1.3'
    compile 'frankiesardo:icepick:3.2.0'
    compile 'com.sromku:simple-storage:1.2.0'
    compile 'de.psdev.licensesdialog:licensesdialog:1.8.1'
    compile 'com.jenzz:materialpreference:1.3'
    compile 'com.afollestad.material-dialogs:core:0.9.1.0'
    compile 'com.android.support:support-v4:25.1.0'
    compile 'info.hoang8f:android-segmented:1.0.6'
    compile 'com.lovedise:permissiongen:0.0.6'
    compile 'com.google.android.gms:play-services-base:10.0.1'
    compile 'com.google.firebase:firebase-messaging:10.0.1'
    compile 'com.github.florent37:arclayout:1.0.1'
    compile 'com.google.firebase:firebase-crash:10.0.1'
    compile 'com.google.firebase:firebase-core:10.0.1'
    compile 'com.google.firebase:firebase-database:10.0.1'
    annotationProcessor 'com.jakewharton:butterknife-compiler:8.4.0'
    provided 'frankiesardo:icepick-processor:3.2.0'
}

apply plugin: 'com.google.gms.google-services'
